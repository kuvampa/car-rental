-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema car_rental
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema car_rental
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `car_rental` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `car_rental`;

-- -----------------------------------------------------
-- Table `car_rental`.`manager`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`manager`
(
    `id_manager`       INT          NOT NULL,
    `login`            VARCHAR(45)  NOT NULL,
    `password`         VARCHAR(64)  NOT NULL,
    `full_name`        VARCHAR(45)  NOT NULL,
    `telephone_number` VARCHAR(45)  NOT NULL,
    `email`            VARCHAR(45)  NOT NULL,
    `documents`        VARCHAR(450) NULL DEFAULT NULL,
    PRIMARY KEY (`id_manager`),
    UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`insurance_package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`insurance_package`
(
    `id_insurance` INT          NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(45)  NOT NULL,
    `description`  VARCHAR(500) NOT NULL,
    `status`       VARCHAR(45)  NOT NULL,
    PRIMARY KEY (`id_insurance`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 9
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`office`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`office`
(
    `id_office`        INT          NOT NULL AUTO_INCREMENT,
    `name`             VARCHAR(45)  NOT NULL,
    `location`         VARCHAR(100) NOT NULL,
    `labor_hours`      VARCHAR(45)  NOT NULL,
    `status`           VARCHAR(45)  NOT NULL,
    `telephone_number` VARCHAR(45)  NOT NULL,
    `map`              VARCHAR(500) NULL DEFAULT NULL,
    PRIMARY KEY (`id_office`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 8
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`terms_of_use_package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`terms_of_use_package`
(
    `id_terms_package` INT         NOT NULL AUTO_INCREMENT,
    `name`             VARCHAR(45) NOT NULL,
    `fuel_conditions`  VARCHAR(45) NOT NULL,
    `mileage_limit`    VARCHAR(45) NOT NULL,
    `status`           VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id_terms_package`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 11
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`car`
(
    `id_car`           INT         NOT NULL AUTO_INCREMENT,
    `id_office`        INT         NOT NULL,
    `id_insurance`     INT         NOT NULL,
    `id_terms_package` INT         NOT NULL,
    `capacity`         INT         NOT NULL,
    `rating`           INT         NOT NULL,
    `price`            FLOAT       NOT NULL,
    `color`            VARCHAR(45) NOT NULL,
    `car_name`         VARCHAR(45) NOT NULL,
    `plate_number`     VARCHAR(45) NOT NULL,
    `type`             VARCHAR(45) NOT NULL,
    `manufacturer`     VARCHAR(45) NOT NULL,
    `fuel_type`        VARCHAR(45) NOT NULL,
    `status`           VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id_car`),
    INDEX `id_terms_package_idx` (`id_terms_package` ASC) VISIBLE,
    INDEX `id_office_idx` (`id_office` ASC) VISIBLE,
    INDEX `id_insurance_idx` (`id_insurance` ASC) VISIBLE,
    CONSTRAINT `id_insurance`
        FOREIGN KEY (`id_insurance`)
            REFERENCES `car_rental`.`insurance_package` (`id_insurance`),
    CONSTRAINT `id_office`
        FOREIGN KEY (`id_office`)
            REFERENCES `car_rental`.`office` (`id_office`),
    CONSTRAINT `id_terms_package`
        FOREIGN KEY (`id_terms_package`)
            REFERENCES `car_rental`.`terms_of_use_package` (`id_terms_package`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 36
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`user`
(
    `id_user`          INT          NOT NULL AUTO_INCREMENT,
    `login`            VARCHAR(45)  NOT NULL,
    `password`         VARCHAR(64)  NOT NULL,
    `full_name`        VARCHAR(45)  NOT NULL,
    `telephone_number` VARCHAR(45)  NOT NULL,
    `email`            VARCHAR(45)  NOT NULL,
    `documents`        VARCHAR(450) NULL DEFAULT NULL,
    `status`           TINYINT      NULL DEFAULT NULL,
    PRIMARY KEY (`id_user`),
    UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 92
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`rental`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`rental`
(
    `id_rental`             INT         NOT NULL AUTO_INCREMENT,
    `id_user`               INT         NOT NULL,
    `id_car`                INT         NOT NULL,
    `date_of_receiving`     DATETIME    NOT NULL,
    `return_date`           DATETIME    NOT NULL,
    `return_place`          VARCHAR(45) NOT NULL,
    `price`                 FLOAT       NOT NULL,
    `right_of_cancellation` TINYINT     NOT NULL,
    PRIMARY KEY (`id_rental`),
    INDEX `id_user_idx` (`id_user` ASC) VISIBLE,
    INDEX `id_service_idx` (`id_car` ASC) VISIBLE,
    CONSTRAINT `id_car`
        FOREIGN KEY (`id_car`)
            REFERENCES `car_rental`.`car` (`id_car`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `id_user`
        FOREIGN KEY (`id_user`)
            REFERENCES `car_rental`.`user` (`id_user`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 63
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`accident`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`accident`
(
    `id_accident` INT          NOT NULL AUTO_INCREMENT,
    `id_rent`     INT          NOT NULL,
    `id_manager`  INT          NULL DEFAULT NULL,
    `price`       FLOAT        NOT NULL,
    `comment`     VARCHAR(200) NOT NULL,
    `status`      VARCHAR(45)  NOT NULL,
    PRIMARY KEY (`id_accident`),
    INDEX `id_manager_idx` (`id_manager` ASC) VISIBLE,
    INDEX `id_rent_idx` (`id_rent` ASC) VISIBLE,
    CONSTRAINT `idmanager`
        FOREIGN KEY (`id_manager`)
            REFERENCES `car_rental`.`manager` (`id_manager`)
            ON DELETE SET NULL
            ON UPDATE RESTRICT,
    CONSTRAINT `idrental`
        FOREIGN KEY (`id_rent`)
            REFERENCES `car_rental`.`rental` (`id_rental`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 15
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`additional_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`additional_service`
(
    `id_service`  INT          NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(45)  NOT NULL,
    `price`       FLOAT        NOT NULL,
    `status`      VARCHAR(45)  NOT NULL,
    `description` VARCHAR(500) NULL DEFAULT NULL,
    PRIMARY KEY (`id_service`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 8
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`admin`
(
    `id_admin`         INT         NOT NULL,
    `login`            VARCHAR(45) NOT NULL,
    `password`         VARCHAR(64) NOT NULL,
    `full_name`        VARCHAR(45) NOT NULL,
    `telephone_number` VARCHAR(45) NOT NULL,
    `email`            VARCHAR(45) NULL DEFAULT NULL,
    `documents`        VARCHAR(45) NULL DEFAULT NULL,
    PRIMARY KEY (`id_admin`),
    UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`service_rent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`service_rent`
(
    `id_rent_service` INT NOT NULL AUTO_INCREMENT,
    `id_rent`         INT NOT NULL,
    `id_service`      INT NULL DEFAULT NULL,
    PRIMARY KEY (`id_rent_service`),
    INDEX `id_service_idx` (`id_service` ASC) VISIBLE,
    INDEX `id_rent_idx` (`id_rent` ASC) VISIBLE,
    CONSTRAINT `id_rent`
        FOREIGN KEY (`id_rent`)
            REFERENCES `car_rental`.`rental` (`id_rental`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `id_serv`
        FOREIGN KEY (`id_service`)
            REFERENCES `car_rental`.`additional_service` (`id_service`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 52
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`user_support`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`user_support`
(
    `id_user_support` INT           NOT NULL AUTO_INCREMENT,
    `id_user`         INT           NOT NULL,
    `id_admin`        INT           NULL DEFAULT NULL,
    `message`         VARCHAR(1000) NOT NULL,
    `status`          VARCHAR(45)   NOT NULL,
    PRIMARY KEY (`id_user_support`),
    INDEX `id_user_idx` (`id_user` ASC) VISIBLE,
    INDEX `id_admin_idx` (`id_admin` ASC) VISIBLE,
    CONSTRAINT `id_admin`
        FOREIGN KEY (`id_admin`)
            REFERENCES `car_rental`.`admin` (`id_admin`)
            ON DELETE SET NULL,
    CONSTRAINT `user_id`
        FOREIGN KEY (`id_user`)
            REFERENCES `car_rental`.`user` (`id_user`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 14
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `car_rental`.`verification_of_documents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `car_rental`.`verification_of_documents`
(
    `id_verification_of_documents` INT          NOT NULL AUTO_INCREMENT,
    `id_rental`                    INT          NOT NULL,
    `id_manager`                   INT          NULL DEFAULT NULL,
    `comment`                      VARCHAR(500) NULL DEFAULT NULL,
    `status`                       VARCHAR(45)  NOT NULL,
    PRIMARY KEY (`id_verification_of_documents`),
    INDEX `id_rental_idx` (`id_rental` ASC) VISIBLE,
    INDEX `id_manager_idx` (`id_manager` ASC) VISIBLE,
    CONSTRAINT `id_manager`
        FOREIGN KEY (`id_manager`)
            REFERENCES `car_rental`.`manager` (`id_manager`)
            ON DELETE SET NULL,
    CONSTRAINT `id_rental`
        FOREIGN KEY (`id_rental`)
            REFERENCES `car_rental`.`rental` (`id_rental`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 28
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
