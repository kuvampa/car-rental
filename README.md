Car rental company

 - The car rental company has several cars to choose from.

 - Each car has its own manufacturer, name, price, dimensions, characteristics and numbers.

 - The client logs into the system, selects a car from the catalog, places an order and sends a   scanned copy of the necessary documents. 

 - Then the manager confirms the order, and the customer is given the option to pay.

 - There is also an administrator for organizing roles and work on the site.

 - Implemented listener that used to automatically check orders every hour.

 - An accident system has been created for issuing compensations and fines.

![alt text](DB-1.png)