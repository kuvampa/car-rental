<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Admin Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/>
                prices.</p>
            <p>
            <form action="adminOffices" method="get">
                <input name="InsertOffice" type="hidden" value="New">
                <button type="submit" class="btn btn-secondary my-2"><fmt:message key="Add new office"/></button>
            </form>
            </p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:if test="${newOffice!=null}">
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">
                                <form method="post" action="adminOffices">
                                    <div class="text-center"><input name="new_office_name"/></div>
                                    <br>
                                    <table>
                                        <tbody>
                                        <tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Location"/>:</p></td>
                                            <td><p class="text-left"><input name="new_office_location"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Labor Hours"/>:</p></td>
                                            <td><p class="text-left"><input name="new_office_labor_hours"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Telephone Number"/>:</p>
                                            </td>
                                            <td><p class="text-left"><input name="new_office_telephone_number"/></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Map"/>:</p></td>
                                            <td><p class="text-left"><input name="new_office_map"/></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <input name="new_accept" type="hidden" value="accept">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Accept"/>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <form action="adminOffices" method="post">
                                            <input name="new_cancel" type="hidden"
                                                   value="cancel">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Cancel"/>
                                            </button>
                                        </form>
                                    </div>
                                    <small class="text-muted"><fmt:message key="created"/></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:forEach var="office" items="${requestScope.offices}">
                    <c:choose>
                        <c:when test="${office.status == 'available'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <iframe title="location"
                                            class="card-img-top"
                                            data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                            alt="Thumbnail [100%x300]"
                                            style="height: 300px; width: 100%; display: block;"
                                            src="<c:out value="${office.map}"/>"
                                            data-holder-rendered="true"></iframe>
                                    <div class="card-body">
                                        <p class="card-text">
                                        <div class="text-center"><c:out value="${office.name}"/></div>
                                        <br>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Location"/>:</p></td>
                                                <td><p class="text-center"><c:out value="${office.location}"/></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Labor Hours"/>:</p>
                                                </td>
                                                <td><p class="text-center"><c:out value="${office.laborHours}"/></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Telephone Number"/>:</p>
                                                </td>
                                                <td><p class="text-center"><c:out
                                                        value="${office.telephoneNumber}"/></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminOffices" method="post">
                                                    <input name="Update-Office" type="hidden"
                                                           value="<c:out value="${office.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Update information"/>
                                                    </button>
                                                </form>
                                                <form action="adminOffices" method="post">
                                                    <input name="Delete-Office" type="hidden"
                                                           value="<c:out value="${office.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Delete this office"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${office.status}"/></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${office.status == 'updated'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <iframe title="location"
                                            class="card-img-top"
                                            data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                            alt="Thumbnail [100%x300]"
                                            style="height: 300px; width: 100%; display: block;"
                                            src="<c:out value="${office.map}"/>"
                                            data-holder-rendered="true"></iframe>
                                    <div class="card-body">
                                        <p class="card-text">
                                        <form method="post" action="adminOffices">
                                            <div class="text-center"><input name="office_name"
                                                                            value="<c:out value="${office.name}"/>"/>
                                            </div>
                                            <br>
                                            <table>
                                                <tbody>
                                                <tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Location"/>:</p></td>
                                                    <td><p class="text-left"><input name="office_location"
                                                                                    value="<c:out value="${office.location}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Labor Hours"/>:</p></td>
                                                    <td><p class="text-left"><input name="office_labor_hours"
                                                                                    value="<c:out value="${office.laborHours}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Telephone Number"/>:</p>
                                                    </td>
                                                    <td><p class="text-left"><input name="office_telephone_number"
                                                                                    value="<c:out value="${office.telephoneNumber}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Map"/>:</p></td>
                                                    <td><p class="text-left"><input name="office_map"
                                                                                    value="<c:out value="${office.map}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <input name="accept" type="hidden"
                                                           value="<c:out value="${office.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Accept"/>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminOffices" method="post">
                                                    <input name="cancel" type="hidden"
                                                           value="<c:out value="${office.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Cancel"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${office.status}"/></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
