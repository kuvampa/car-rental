<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
    <%@include file="css/style.css" %>
    </style>
    <title><fmt:message key="Find Pluto"/></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<form class="form-signin" action="find" method="get">
    <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="Please sign in"/></h1>
    <label for="inputEmail" class="sr-only">Login</label>
    <input name="login" id="inputEmail" class="form-control" placeholder="<fmt:message key="enter your login"/>" required autofocus>
    <label for="inputPassword" class="sr-only"><fmt:message key="Password"/></label>
    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="Password"/>" required>
    <div class="checkbox mb-3">
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="Sign in"/></button>
</form>
<%@include file="footer.jspf" %>
</body>
</html>