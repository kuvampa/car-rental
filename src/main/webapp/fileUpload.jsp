<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title><fmt:message key="Documents Pluto"/></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<form action="start" method="get">
    <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="Your Documents added"/></h1>
</form>
<%@include file="footer.jspf" %>
</body>
</html>