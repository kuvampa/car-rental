<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Admin Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                    <c:forEach var="user" items="${requestScope.users}">
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top"
                                     data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                     alt="Thumbnail [100%x300]" style="height: 300px; width: 100%; display: block;"
                                     src="${pageContext.request.contextPath}/file/users/User.jpg"
                                     data-holder-rendered="true">
                                <div class="card-body">
                                    <p class="card-text">
                                    <div class="text-center"><c:out value="${user.fullName}"/></div>
                                    <br>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td><p class="text-center"><fmt:message key="User ID"/>:</p></td>
                                            <td><p class="text-center"><c:out value="${user.idUser}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-center"><fmt:message key="Login"/>:</p></td>
                                            <td><p class="text-center"><c:out value="${user.login}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-center"><fmt:message key="Telephone Number"/>:</p></td>
                                            <td><p class="text-center"><c:out value="${user.telephoneNumber}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-center"><fmt:message key="Email"/>:</p></td>
                                            <td><p class="text-center"><c:out value="${user.email}"/></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <c:forEach var="admin" items="${requestScope.admins}">
                                            <c:if test="${user.idUser == admin.idAdmin}">
                                                <c:set var="active" scope="session" value="active"/>
                                                <div class="btn-group">
                                                    <form action="adminUsers" method="post">
                                                        <input name="Admin-Remove-Admin" type="hidden"
                                                               value="<c:out value="${user.login}"/>">
                                                        <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                            <fmt:message key="Remove admin rights"/>
                                                        </button>
                                                    </form>
                                                    <form action="adminUsers" method="post">
                                                        <input name="User-to-Admin" type="hidden"
                                                               value="<c:out value="${user.login}"/>">
                                                        <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                            <fmt:message key="You cant block admin"/>
                                                        </button>
                                                    </form>
                                                </div>
                                                <small class="text-muted"><fmt:message key="Admin"/></small>
                                            </c:if>
                                        </c:forEach>
                                        <c:forEach var="manager" items="${requestScope.managers}">
                                            <c:if test="${user.idUser == manager.idManager}">
                                                <c:set var="activeM" scope="session" value="active"/>
                                                <div class="btn-group">
                                                    <form action="adminUsers" method="post">
                                                        <input name="Admin-Remove-Manager" type="hidden"
                                                               value="<c:out value="${user.login}"/>">
                                                        <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                            <fmt:message key="Remove manager rights"/>
                                                        </button>
                                                    </form>
                                                    <form action="adminUsers" method="post">
                                                        <input name="Manager-to-Admin" type="hidden"
                                                               value="<c:out value="${user.login}"/>">
                                                        <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                            <fmt:message key="You cant block manager"/>
                                                        </button>
                                                    </form>
                                                </div>
                                                <small class="text-muted"><fmt:message key="Manager"/></small>
                                            </c:if>
                                        </c:forEach>
                                        <c:if test="${active == 'negative'&& activeM == 'negative'}">
                                            <div class="btn-group">
                                                <form action="adminUsers" method="post">
                                                    <input name="User-to-Admin" type="hidden"
                                                           value="<c:out value="${user.login}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Make user admin"/>
                                                    </button>
                                                </form>
                                                <form action="adminUsers" method="post">
                                                    <input name="User-to-Manager" type="hidden"
                                                           value="<c:out value="${user.login}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Make user manager"/>
                                                    </button>
                                                </form>
                                                <c:choose>
                                                    <c:when test="${user.status == 0}">
                                                        <form action="adminUsers" method="post">
                                                            <input name="User-Block" type="hidden"
                                                                   value="<c:out value="${user.login}"/>">
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-outline-secondary">
                                                                <fmt:message key="Block this user"/>
                                                            </button>
                                                        </form>
                                                    </c:when>
                                                    <c:when test="${user.status == 1}">
                                                        <form action="adminUsers" method="post">
                                                            <input name="User-Unblock" type="hidden"
                                                                   value="<c:out value="${user.login}"/>">
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-outline-secondary">
                                                                <fmt:message key="Unblock this user"/>
                                                            </button>
                                                        </form>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                            <small class="text-muted"><fmt:message key="User"/></small>
                                        </c:if>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <c:set var="activeM" scope="session" value="negative"/>
                        <c:set var="active" scope="session" value="negative"/>
                    </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
