<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Rent Pluto"/></title>
</head
<body class="bg-light">
<%@include file="navbar.jspf" %>
<section class="jumbotron text-center">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4"
             src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
             alt="" width="72" height="72">
        <h2><fmt:message key="Pluto"/></h2>
        <p class="lead"><fmt:message key="Online booking of "/><c:out value="${carRent.carName}"/></p>
    </div>
</section>
<div class="container">
    <form action="verification" method="post">
        <div class="col-md-8 order-md-1">
            <h3 class="mb-3"><fmt:message key="Contact Information"/></h3>
            <div class="row">
                <div class="col-md-6 mb-3"><h4>
                    <label for="fullName"><fmt:message key="Full name"/></label>
                    <input type="text" class="form-control" id="fullName"
                           placeholder="<fmt:message key="Enter your full name"/>" name="name"
                           value="<c:out value="${userRent.fullName}"/>" readonly>
                </h4></div>
                <div class="col-md-6 mb-3"><h4>
                    <label for="phone"><fmt:message key="Phone number"/></label>
                    <input type="tel" name="phone" class="form-control" id="phone"
                           placeholder="<fmt:message key="for example, +380951234567"/>"
                           value="<c:out value="${userRent.telephoneNumber}"/>" readonly>
                </h4></div>
                <legend></legend>
                <div class="mb-3">
                    <h3><fmt:message key="Insuranse and Terms of use"/></h3>
                    <ul>
                        <h4>
                            <li><fmt:message key="Insurance package name"/>: <c:out
                                    value="${insurancePackageRent.name}"/></li>
                            <li><fmt:message key="Description"/>: <c:out
                                    value="${insurancePackageRent.description}"/></li>
                            <li><fmt:message key="Terms of use package"/>: <c:out value="${termsOfUseRent.name}"/></li>
                            <li><fmt:message key="Fuel Conditions"/>: <c:out
                                    value="${termsOfUseRent.fuelConditions}"/></li>
                            <li><fmt:message key="Mileage Limit"/>: <c:out value="${termsOfUseRent.mileageLimit}"/></li>
                        </h4>
                    </ul>
                </div>
                <legend></legend>
                <h3 class="mb-3"><fmt:message key="Rental period"/></h3>
                <div class="col-md-6 mb-3">
                    <h4>
                        <label for="pick1"><fmt:message key="Convenient date for you to pick up"/></label>
                        <input type="datetime-local" name="date_time_pick" id="pick1" min="${min}" max="${max}"
                               required>
                        <hr class="mb-4">
                        <label for="return1"><fmt:message key="Convenient date for you to return"/>:</label>
                        <input type="datetime-local" name="date_time_return" id="return1" min="${min}" max="${max}"
                               required>
                    </h4>
                </div>
                <div class="col-md-6 mb-3">
                    <h4>
                        <label for="phone"><fmt:message key="Busy days"/>:</label>
                        <c:forEach var="rentalsByRec" items="${requestScope.rentalsByRec}">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <div><fmt:message key="From"/></div>
                                    </td>
                                    <td>
                                        <div><fmt:message key="To"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="datetime-local" name="datePick"
                                               value="${rentalsByRec.dateOfReceiving}"
                                               readonly></td>
                                    <td><input type="datetime-local" name="dateRet" value="${rentalsByRec.returnDate}"
                                               readonly>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </c:forEach>
                    </h4>
                </div>
                <legend></legend>
                <div class="col-md-6 mb-3">
                    <h4>
                        <label for="pickup2"><fmt:message key="Pick up place"/>:</label>
                        <select class="custom-select d-block w-100" name="Pick" id="pickup2" required>
                            <option value="Office"><fmt:message key="Office"/></option>
                        </select>
                    </h4>
                </div>
                <div class="col-md-6 mb-3">
                    <h4>
                        <label for="return2"><fmt:message key="Return place"/>:</label>
                        <select class="custom-select d-block w-100" name="Return" id="return2" required>
                            <option value="Office"><fmt:message key="Office"/></option>
                            <option value="In the city"><fmt:message key="In the city"/></option>
                        </select>
                    </h4>
                </div>
                <legend></legend>
                <h3 class="mb-3"><fmt:message key="Additional service"/></h3>
                <div class="custom-control custom-checkbox"><h4>
                    <c:forEach var="service" items="${requestScope.additionalServicesRent}">
                        <label><input type="checkbox" name="<c:out value="${service.name}"/>"><c:out
                                value="${service.name}"/> - <myTags:price price="${service.price}"
                                                                          lang="${lang}"/></label><br>
                    </c:forEach>
                </h4></div>
                <legend></legend>
                <h3 class="mb-3"><fmt:message key="Documents"/></h3>
                <div class="col-md-6 mb-3">
                    <h4>
                        <c:choose>
                            <c:when test="${docs == null}">
                                <fmt:message key="Please add your documents"/>
                                <ul>
                                    <h4>
                                        <li><fmt:message
                                                key="Personal passport (served children aged 23 and over)"/></li>
                                        <li><fmt:message
                                                key="TIN identification code (needed only for citizens of Ukraine)"/></li>
                                        <li><fmt:message
                                                key="Driver's license (driver's experience must not be less than two full years)"/></li>
                                        <li><fmt:message
                                                key="Rights issued according to the international model for non-residents of the country"/></li>
                                    </h4>
                                </ul>
                                <br>
                                <h4 class="mb-3"><fmt:message
                                        key="If you do not know how to add documents, you can see the tutorial"/></h4>
                                <c:choose>
                                    <c:when test="${lang == 'en'}">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/mt_vDebkEjA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </c:when>
                                    <c:otherwise>
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/5LPX5CrK_18" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="Documents are added"/>
                            </c:otherwise>
                        </c:choose>
                    </h4>
                </div>
                <legend></legend>
                <h3 class="mb-3"><fmt:message key="Payment and verification of documents"/></h3>
                <div class="col-md-6 mb-3"><h4>
                    <c:choose>
                        <c:when test="${docs != null}">
                            <input type="submit" name="submit" value="Sent">
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="Add documents first"/>
                        </c:otherwise>
                    </c:choose>
                </h4></div>
            </div>
        </div>
    </form>
</div>
<%@include file="footer.jspf" %>
</body>
</html>