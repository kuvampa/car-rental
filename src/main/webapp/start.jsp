<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="myTagsLib" uri="MyTag" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<form action="start" method="get">
    <div>
        <main role="main">
            <section class="jumbotron text-center">
                <div class="container">
                    <img class="d-block mx-auto mb-4"
                         src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                         alt="" width="72" height="72">
                    <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
                    <p class="lead text-muted"><fmt:message
                            key="For the best people the best cars at the best prices."/></p>
                    <a class="btn btn-primary" data-bs-toggle="offcanvas" href="#offcanvas2" role="button"
                       aria-controls="offcanvas"><fmt:message key="Search and language selection"/></a>
                </div>
            </section>
            <div class="album py-5 bg-light">
                <div class="container">
                    <div class="row">
                        <c:forEach var="car" items="${requestScope.cars}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <img class="card-img-top"
                                         data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                         alt="Thumbnail [100%x300]"
                                         style="height: 300px; width: 100%; display: block;"
                                         src="${pageContext.request.contextPath}/file/cars/<c:out value="${car.plateNumber}"/>.jpg"
                                         data-holder-rendered="true">
                                    <div class="card-body">
                                        <p class="card-text"></p>
                                        <div class="text-center"><c:out value="${car.carName}"/></div>
                                        <br>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Price for 1 day"/>:</p>
                                                </td>
                                                <td><p class="text-center"><myTags:price price="${car.price}"
                                                                                         lang="${lang}"
                                                                                         USD="${USD}"/></p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Type"/>:</p></td>
                                                <td><p class="text-center"><c:out value="${car.type}"/></p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Rating"/>:</p></td>
                                                <td><p class="text-center"><myTagsLib:rating
                                                        rating="${car.rating}"/></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Fuel type"/>:</p></td>
                                                <td><p class="text-center"><c:out value="${car.fuelType}"/></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form>
                                                </form>
                                                <form action="descriptionCar" method="get">
                                                    <input name="CarPlateDesc" type="hidden"
                                                           value="<c:out value="${car.plateNumber}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Description"/>
                                                    </button>
                                                </form>
                                                <c:if test="${blockStatus == 0&&(loginStart!=null||adminStart!=null||managerStart!=null)}">
                                                    <form action="rent" method="get">
                                                        <input name="CarPlateRent" type="hidden"
                                                               value="<c:out value="${car.plateNumber}"/>">
                                                        <button type="submit"
                                                                class="btn btn-sm btn-outline-secondary">
                                                            <fmt:message key="Book this car"/>
                                                        </button>
                                                    </form>
                                                </c:if>
                                            </div>
                                            <small class="text-muted"><c:out value="${car.status}"/></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="submit" class="btn btn-lg btn-outline-primary" name="page" value="left" <c:if
                            test="${blockL=='left'}">
                        disabled
                    </c:if>>&laquo;
                    </button>
                    <button type="submit" class="btn btn-lg btn-outline-primary" name="page" value="right" <c:if
                            test="${blockR=='right'}">
                        disabled
                    </c:if>>&raquo;
                    </button>
                </div>
            </div>
        </main>
        <div class="offcanvas offcanvas-start overflow-scroll" tabindex="-1" id="offcanvas2"
             aria-labelledby="offcanvasLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasExampleLabel"><fmt:message key="Search and language selection"/></h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="container text-left">
                    <div class="collapse navbar-collapse col-md-6 mb-3" id="navbarSupportedContent1">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle btn-lg" id="navbarDropdown6"
                                   role="button"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <fmt:message key="language"/>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown6">
                                    <button name="language" value="UK" class="dropdown-item btn-lg" type="submit">
                                        Українська
                                    </button>
                                    <button name="language" value="EN" class="dropdown-item btn-lg" type="submit">
                                        English
                                    </button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="collapse navbar-collapse col-md-6 mb-3" id="navbarSupportedContent2">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle btn-lg b" id="navbarDropdown2" role="button"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <fmt:message key="Office contacts"/>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                    <c:forEach var="office" items="${requestScope.offices}">
                                        <button name="officeName" value="${office.name}" class="dropdown-item btn-lg" type="submit"><c:out value="${office.name}"/></button>
                                    </c:forEach>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div for="inputSearch" class="sr-only"><fmt:message key="Search"/></div>
                    <input name="name" id="inputSearch" class="form-control"
                           placeholder="<fmt:message key="find your car name"/>" value="${searchedName}">
                    <h4><fmt:message key="Manufacturer"/><br></h4>
                    <c:forEach var="manufacturer" items="${requestScope.carManufacturer}">
                        <c:choose>
                            <c:when test="${checkerM!=1}">
                                <label><input type="checkbox" name="<c:out value="${manufacturer}"/>"
                                <c:forEach var="searchedManufactures" items="${requestScope.searchedManufactures}">
                                <c:if test="${manufacturer==searchedManufactures}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${manufacturer}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox" name="<c:out value="${manufacturer}"/>"><c:out
                                        value="${manufacturer}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <h4><fmt:message key="Color"/><br></h4>
                    <c:forEach var="color" items="${requestScope.carColor}">
                        <c:choose>
                            <c:when test="${checkerCo!=1}">
                                <label><input type="checkbox" name="<c:out value="${color}"/>"
                                <c:forEach var="searchedColors" items="${requestScope.searchedColors}">
                                <c:if test="${color==searchedColors}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${color}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox" name="<c:out value="${color}"/>"><c:out
                                        value="${color}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <h4><fmt:message key="Car Type"/><br></h4>
                    <c:forEach var="type" items="${requestScope.carType}">
                        <c:choose>
                            <c:when test="${checkerT!=1}">
                                <label><input type="checkbox" name="<c:out value="${type}"/>"
                                <c:forEach var="searchedTypes" items="${requestScope.searchedTypes}">
                                <c:if test="${type==searchedTypes}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${type}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox" name="<c:out value="${type}"/>"><c:out
                                        value="${type}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <h4><fmt:message key="Fuel type"/><br></h4>
                    <c:forEach var="fuelType" items="${requestScope.carFuelType}">
                        <c:choose>
                            <c:when test="${checkerF!=1}">
                                <label><input type="checkbox"
                                              name="<c:out value="${fuelType}"/>"
                                <c:forEach var="searchedFuelTypes"
                                           items="${requestScope.searchedFuelTypes}">
                                <c:if test="${fuelType==searchedFuelTypes}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${fuelType}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox" name="<c:out value="${fuelType}"/>"><c:out
                                        value="${fuelType}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <h4><fmt:message key="Capacity"/><br></h4>
                    <c:forEach var="capacity" items="${requestScope.carCapacity}">
                        <c:choose>
                            <c:when test="${checkerCa!=1}">
                                <label><input type="checkbox"
                                              name="<c:out value="${capacity}"/>C"
                                <c:forEach var="searchedCapacity"
                                           items="${requestScope.searchedCapacity}">
                                <c:if test="${capacity==searchedCapacity}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${capacity}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox"
                                              name="<c:out value="${capacity}"/>C"><c:out
                                        value="${capacity}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <h4><fmt:message key="Rating"/><br></h4>
                    <c:forEach var="rating" items="${requestScope.carRating}">
                        <c:choose>
                            <c:when test="${checkerR!=1}">
                                <label><input type="checkbox"
                                              name="<c:out value="${rating}"/>R"
                                <c:forEach var="searchedRatings"
                                           items="${requestScope.searchedRatings}">
                                <c:if test="${rating==searchedRatings}">
                                              checked="checked"
                                </c:if>
                                </c:forEach>
                                ><c:out value="${rating}"/></label><br>
                            </c:when>
                            <c:otherwise>
                                <label><input type="checkbox" name="<c:out value="${rating}"/>R"><c:out
                                        value="${rating}"/></label><br>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <legend></legend>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th scope="col"><fmt:message key="Sorting by name"/>
                                <c:choose>
                                    <c:when test="${sort==1}">
                                        <label><input type="radio" name="sort" value="1"
                                                      checked="checked"><fmt:message
                                                key="A-Z"/></label>
                                    </c:when>
                                    <c:when test="${sort!=1}">
                                        <label><input type="radio" name="sort" value="1"><fmt:message
                                                key="A-Z"/></label>
                                    </c:when>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${sort==2}">
                                        <label><input type="radio" name="sort" value="2"
                                                      checked="checked"><fmt:message
                                                key="Z-A"/></label>
                                    </c:when>
                                    <c:when test="${sort!=2}">
                                        <label><input type="radio" name="sort" value="2"><fmt:message
                                                key="Z-A"/></label>
                                    </c:when>
                                </c:choose>
                            </th>
                        </tr>
                        <tr>
                            <th scope="col"><fmt:message key="Sorting by price"/>
                                <c:choose>
                                    <c:when test="${sort==3}">
                                        <label><input type="radio" name="sort" value="3"
                                                      checked="checked">min-max</label>
                                    </c:when>
                                    <c:when test="${sort!=3}">
                                        <label><input type="radio" name="sort" value="3">min-max</label>
                                    </c:when>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${sort==4}">
                                        <label><input type="radio" name="sort" value="4"
                                                      checked="checked">max-min</label>
                                    </c:when>
                                    <c:when test="${sort!=4}">
                                        <label><input type="radio" name="sort" value="4">max-min</label>
                                    </c:when>
                                </c:choose>
                            </th>
                        </tr>
                        <tr>
                            <th scope="col"><fmt:message key="Without Sorting"/>
                                <c:choose>
                                    <c:when test="${sort==5}">
                                        <label><input type="radio" name="sort" value="5"
                                                      checked="checked"><fmt:message key="Without sort"/></label>
                                    </c:when>
                                    <c:when test="${sort!=5}">
                                        <label><input type="radio" name="sort" value="5"><fmt:message
                                                key="Without sort"/></label>
                                    </c:when>
                                </c:choose>
                            </th>
                        </tr>
                        </tbody>
                    </table>
                    <fmt:message key="Price limits"/>
                    <br>
                    <input name="min" value="${searchedMin}" placeholder="min">
                    <input name="max" value="${searchedMax}" placeholder="max">
                    <div class="checkbox mb-3"></div>
                    <button class="btn btn-lg btn-primary btn-block w-25" type="submit"><fmt:message
                            key="Find"/></button>
                </div>
            </div>
        </div>
    </div>
</form>
<%@include file="footer.jspf" %>
</body>
</html>

