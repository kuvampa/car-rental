<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title<fmt:message key="Delete Pluto"/>></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<form class="form-signin" action="delete2" method="post">
    <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="Please write down your password"/></h1>
    <label for="inputPassword" class="sr-only"><fmt:message key="Password"/></label>
    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="Password"/>" required>
    <label for="repeatPassword" class="sr-only"><fmt:message key="Repeat password"/></label>
    <input name="repeat password" type="password" id="repeatPassword" class="form-control" placeholder="<fmt:message key="Repeat password"/>"
           required>
    <div class="checkbox mb-3">
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="Delete"/></button>
</form>
<%@include file="footer.jspf" %>
</body>
</html>