<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title><fmt:message key="Documents Pluto"/></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4"
                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="100" height="100">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
        </div>
    </section>
    <div class="album py-5 bg-light align-content-center">
        <div class="container">
            <div class="col-md-5">
                <div class="card mb-5 box-shadow">
                    <c:if test="${userDoc != null}">
                        <img class="card-img-top"
                             data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                             alt="Thumbnail [100%x300]"
                             style="height: 300px; width: 100%; display: block;"
                             src="${pageContext.request.contextPath}/file/docs/<c:out value="${userDoc}"/>"
                             data-holder-rendered="true">
                    </c:if>
                    <div class="card-body center-block">
                        <p class="card-text">
                        <div class="text-center"></div>
                        <table>
                            <tbody>
                            </tbody>
                        </table>
                        </p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <c:choose>
                                    <c:when test="${userDoc == null}">
                                            <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="Add documents"/></h1>
                                            <br>
                                            <ul>
                                                <h4>
                                                    <li class="text-left"><fmt:message key="Personal passport (served children aged 23 and over)"/></li>
                                                    <li class="text-left"><fmt:message key="TIN identification code (needed only for citizens of Ukraine)"/></li>
                                                    <li class="text-left"><fmt:message key="Driver's license (driver's experience must not be less than two full years)"/></li>
                                                    <li class="text-left"><fmt:message key="Rights issued according to the international model for non-residents of the country"/></li>
                                                </h4>
                                            </ul>
                                            <br>
                                        <form method="post" action="fileUpload" enctype="multipart/form-data">
                                            <input name="Docs" type="hidden" value="up">
                                            <input  type="file" name="contentAdd">
                                            <button type="submit" class="btn btn-lg btn-outline-primary"><fmt:message key="Upload"/></button>
                                        </form>
                                    </c:when>
                                    <c:otherwise>
                                        <h1 class="h4 mb-3 font-weight-normal"><fmt:message key="Please check that your documents meet all requirements"/></h1>
                                        <ul>
                                            <h5>
                                                <li class="text-left"><fmt:message key="Personal passport (served children aged 23 and over)"/></li>
                                                <li class="text-left"><fmt:message key="TIN identification code (needed only for citizens of Ukraine)"/></li>
                                                <li class="text-left"><fmt:message key="Driver's license (driver's experience must not be less than two full years)"/></li>
                                                <li class="text-left"><fmt:message key="Rights issued according to the international model for non-residents of the country"/></li>
                                            </h5>
                                        </ul>
                                        <br>
                                        <form method="post" action="fileUpload" enctype="multipart/form-data">
                                            <input name="Docs" type="hidden" value="up">
                                            <h1 class="h4 mb-3 font-weight-normal"><fmt:message key="If not, update the documents to match the requirements"/></h1>
                                            <input type="file" name="contentAdd">
                                            <button type="submit" class="btn btn-lg btn-outline-primary"> <fmt:message key="Upload"/></button>
                                        </form>
                                        <br>
                                        <form method="post" action="fileUpload">
                                            <input name="deleteDocuments" type="hidden">
                                            <h1 class="h4 mb-3 font-weight-normal"><fmt:message key="If you do not want your documents to remain on the site, you can delete them"/></h1>
                                            <button type="submit" class="btn btn-lg btn-outline-primary"> <fmt:message key="Delete"/></button>
                                        </form>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>