<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title><fmt:message key="Verification Pluto"/></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<h1 class="h3 mb-3 font-weight-normal"><fmt:message
        key="Your documents are being checked, the status of your order has been changed to waiting after checking the documents, pay the rent"/></h1>
<%@include file="footer.jspf" %>
</body>
</html>
