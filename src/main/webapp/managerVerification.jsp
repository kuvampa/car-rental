<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Manager Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:forEach var="verification" items="${requestScope.verifications}">
                    <c:forEach var="rent" items="${requestScope.rents}">
                        <c:if test="${rent.idRental==verification.idRental&&verification.status=='not verified'}">
                            <c:forEach var="user" items="${requestScope.users}">
                                <c:if test="${user.idUser==rent.idUser}">
                                    <c:forEach var="car" items="${requestScope.cars}">
                                        <c:if test="${car.idCar==rent.idCar}">
                                            <div class="col-md-4">
                                                <form action="managerVerification" method="post">
                                                    <div class="card mb-4 box-shadow">
                                                        <style>
                                                            .card-img-top {
                                                                transition: transform .2s;
                                                            }

                                                            .card-img-top:hover {
                                                                transform: scale(1.3);
                                                            }
                                                        </style>
                                                        <img class="card-img-top"
                                                             data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                                             alt="Thumbnail [100%x300]"
                                                             style="height: 300px; width: 100%; display: block;"
                                                             src="${pageContext.request.contextPath}/file/docs/<c:out value="${user.documents}"/>"
                                                             data-holder-rendered="true">
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                            <div class="text-center"><c:out
                                                                    value="${user.fullName}"/></div>
                                                            <br>
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <td><p class="text-center"><fmt:message
                                                                            key="Car"/>:</p></td>
                                                                    <td><p class="text-center"><c:out
                                                                            value="${car.carName}"/></p></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><p class="text-center"><fmt:message
                                                                            key="Price"/>:</p></td>
                                                                    <td><p class="text-center"><myTags:price price="${rent.price}" lang="${lang}"/></p></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><p class="text-center"><fmt:message
                                                                            key="Date of receiving"/>:</p>
                                                                    </td>
                                                                    <td><input type="datetime-local" name="datePick"
                                                                               value="${rent.dateOfReceiving}" readonly>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><p class="text-center"><fmt:message
                                                                            key="Return date"/>:</p>
                                                                    </td>
                                                                    <td><input type="datetime-local" name="datePick"
                                                                               value="${rent.returnDate}" readonly>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><p class="text-center"><fmt:message
                                                                            key="Comment only for reject"/>:</p>
                                                                    </td>
                                                                    <td><p class="text-center"><input name="comment"/>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            </p>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="btn-group">
                                                                    <input name="reject" type="hidden"
                                                                           value="<c:out value="${verification.idVerificationOfDocuments}"/>">
                                                                    <button name="rejectButton" type="submit"
                                                                            class="btn btn-sm btn-outline-secondary">
                                                                        <fmt:message key="Reject"/>
                                                                    </button>
                                                                    <form action="managerVerification" method="post">
                                                                        <input name="accept" type="hidden" value="<c:out value="${verification.idVerificationOfDocuments}"/>">
                                                                        <button name="acceptButton" type="submit" class="btn btn-sm btn-outline-secondary"><fmt:message key="Accept"/></button>
                                                                    </form>
                                                                </div>
                                                                <small class="text-muted"><fmt:message
                                                                        key="Rent"/></small>
                                                            </div>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
