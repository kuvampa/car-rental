<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Accident Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4"
                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:forEach var="accident" items="${requestScope.accidents}">
                    <c:forEach var="rent" items="${requestScope.rents}">
                        <c:if test="${rent.idRental==accident.idRental}">
                            <c:forEach var="user" items="${requestScope.users}">
                                <c:if test="${user.idUser==rent.idUser&&user.login==loginStart}">
                                    <c:forEach var="car" items="${requestScope.cars}">
                                        <c:if test="${car.idCar==rent.idCar}">
                                            <div class="col-md-4">
                                                <div class="card mb-4 box-shadow">
                                                    <img class="card-img-top"
                                                         data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                                         alt="Thumbnail [100%x300]"
                                                         style="height: 300px; width: 100%; display: block;"
                                                    <c:choose>
                                                    <c:when test="${accident.comment == 'overdue'}">
                                                         src="${pageContext.request.contextPath}/file/accident/overdue.jpg"
                                                    </c:when>
                                                    <c:otherwise>
                                                         src="${pageContext.request.contextPath}/file/accident/<c:out value="${accident.idAccident}"/>.jpg"
                                                    </c:otherwise>
                                                    </c:choose>
                                                         data-holder-rendered="true">
                                                    <div class="card-body">
                                                        <p class="card-text">
                                                        <div class="text-center"><c:out
                                                                value="${user.fullName}"/></div>
                                                        <br>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td><p class="text-center"><fmt:message key="Car"/>:</p>
                                                                </td>
                                                                <td><p class="text-center"><c:out
                                                                        value="${car.carName}"/></p></td>
                                                            </tr>
                                                            <tr>
                                                                <td><p class="text-center"><fmt:message
                                                                        key="Price"/>:</p></td>
                                                                <td><p class="text-center"><myTags:price
                                                                        price="${accident.price}" lang="${lang}"/></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><p class="text-center"><fmt:message
                                                                        key="Comment"/>:</p>
                                                                </td>
                                                                <td><p class="text-center"><c:out
                                                                        value="${accident.comment}"/></p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        </p>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="btn-group">
                                                                <c:if test="${accident.status=='not paid'}">
                                                                    <form action="userAccident" method="post">
                                                                        <input name="paid" type="hidden"
                                                                               value="<c:out value="${accident.idAccident}"/>">
                                                                        <button type="submit"
                                                                                class="btn btn-sm btn-outline-secondary">
                                                                            <fmt:message key="Pay for accident"/>
                                                                        </button>
                                                                    </form>
                                                                </c:if>
                                                            </div>
                                                            <small class="text-muted"><c:out
                                                                    value="${accident.status}"/></small>
                                                        </div>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
