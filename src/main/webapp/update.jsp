<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title><fmt:message key="Update Pluto"/></title>
</head>
<body class="text-center">
<%@include file="navbar.jspf" %>
<form class="form-signin" method="post" action="update" enctype="multipart/form-data">
    <div class="col-md-6 mb-4">
        <img class="card-img-top rounded-circle"
             data-src="holder.js/300px100?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
             alt="Thumbnail [100%x300]" style="height: 160px; width: 100%; display: block;"
             src="${pageContext.request.contextPath}/file/users/${imageUser}.jpg"
             data-holder-rendered="true">
        <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Change your image"/></h4>
        <input name="newImage" type="hidden" value="${getLogin}">
        <input type="file" name="fileName">
        <br>
        <input class="btn btn-primary btn-block" type="submit" value="upload">
    </div>
</form>
<form class="form-signin" action="update2" method="post">
    <div class="col-md-6 mb-4">
        <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="Change your information"/></h1>
        <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your full name"/></h4>
        <label for="inputFullName" class="sr-only"><fmt:message key="Full name"/></label>
        <input value="${getFullName}" name="full_name" id="inputFullName" class="form-control"
               placeholder="<fmt:message key="enter your full name"/>" required
               autofocus>
        <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your Telephone number"/></h4>
        <label for="inputTelephone" class="sr-only"><fmt:message key="Telephone number"/></label>
        <input value="${getTelephoneNumber}" name="telephone_number" id="inputTelephone" class="form-control"
               placeholder="<fmt:message key="enter your telephone"/>" required
               autofocus>
    </div>
    <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your Email"/></h4>
    <label for="inputE" class="sr-only"><fmt:message key="Email"/></label>
    <input value="${getEmail}" name="email" id="inputE" class="form-control"
           placeholder="<fmt:message key="enter your email"/>" required autofocus>
    <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your Login"/></h4>
    <label for="inputLogin" class="sr-only"><fmt:message key="Login"/></label>
    <input value="${getLogin}" name="login" id="inputLogin" class="form-control"
           placeholder="<fmt:message key="enter your login"/>" required autofocus>
    <br><br><br>
    <h4 class="h4 mb-3 font-weight-normal"><fmt:message
            key="if you want to change your password, enter the old one first and then the new one"/></h4>
    <br>
    <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your old Password"/></h4>
    <label for="inputPassword" class="sr-only"><fmt:message key="Password"/></label>
    <input name="password" type="password" id="inputPassword" class="form-control"
           placeholder="<fmt:message key="Password"/>">
    <h4 class="h4 mb-3 font-weight-normal"><fmt:message key="Your new Password"/></h4>
    <label for="newPassword" class="sr-only"><fmt:message key="Password"/></label>
    <input name="newPassword" type="password" id="newPassword" class="form-control"
           placeholder="<fmt:message key="Password"/>">
    <div class="checkbox mb-3">
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="Update"/></button>
</form>
<%@include file="footer.jspf" %>
</body>
</html>
