<%@ attribute name="lang" %>
<%@ attribute name="price" %>
<%@ attribute name="USD" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<c:if test="${lang == 'uk'}">
    <c:out value="${price * USD}"/><fmt:message key="$"/>
</c:if>
<c:if test="${lang == 'en'}">
    <c:out value="${price}"/><fmt:message key="$"/>
</c:if>