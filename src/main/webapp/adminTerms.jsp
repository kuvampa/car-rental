<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Admin Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
            <p>
            <form action="adminTerms" method="get">
                <input name="Insert-Term" type="hidden" value="New">
                <button type="submit" class="btn btn-secondary my-2"><fmt:message key="Add new terms"/></button>
            </form>
            </p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:if test="${newTerm!=null}">
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">
                                <form method="post" action="adminTerms">
                                    <div class="text-center">
                                        <input name="new_term_name"/>
                                    </div>
                                    <br>
                                    <table>
                                        <tbody>
                                        <tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Fuel Conditions"/>:</p></td>
                                            <td><p class="text-left">
                                                <input name="new_term_fuel"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Mileage Limit"/>:</p></td>
                                            <td><p class="text-left"><input name="new_term_Limit"/></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <input name="new_accept" type="hidden" value="accept">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Accept"/>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <form action="adminTerms" method="post">
                                            <input name="new_cancel" type="hidden" value="cancel">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Cancel"/>
                                            </button>
                                        </form>
                                    </div>
                                    <small class="text-muted"><fmt:message key="created"/></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:forEach var="termsOfUse" items="${requestScope.terms}">
                    <c:choose>
                        <c:when test="${termsOfUse.status == 'available'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <div class="text-center"><c:out value="${termsOfUse.name}"/></div>
                                        <br>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Fuel Conditions"/>:</p>
                                                </td>
                                                <td><p class="text-center"><c:out
                                                        value="${termsOfUse.fuelConditions}"/></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Mileage Limit"/>:</p></td>
                                                <td><p class="text-center"><c:out
                                                        value="${termsOfUse.mileageLimit}"/></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminTerms" method="post">
                                                    <input name="Update-Term" type="hidden"
                                                           value="<c:out value="${termsOfUse.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Update information"/>
                                                    </button>
                                                </form>
                                                <form action="adminTerms" method="post">
                                                    <input name="Delete-Term" type="hidden"
                                                           value="<c:out value="${termsOfUse.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Delete this terms"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${termsOfUse.status}"/></small>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${termsOfUse.status == 'updated'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <form method="post" action="adminTerms">
                                            <div class="text-center"><input name="term_name"
                                                                            value="<c:out value="${termsOfUse.name}"/>"/>
                                            </div>
                                            <br>
                                            <table>
                                                <tbody>
                                                <tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Fuel Conditions"/>:</p>
                                                    </td>
                                                    <td><p class="text-left"><input name="term_fuel"
                                                                                    value="<c:out value="${termsOfUse.fuelConditions}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Mileage Limit"/>:</p>
                                                    </td>
                                                    <td><p class="text-left"><input name="term_Limit"
                                                                                    value="<c:out value="${termsOfUse.mileageLimit}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <input name="accept" type="hidden"
                                                       value="<c:out value="${termsOfUse.name}"/>">
                                                <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                    <fmt:message key="Accept"/>
                                                </button>
                                            </div>
                                        </form>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminTerms" method="post">
                                                    <input name="cancel" type="hidden"
                                                           value="<c:out value="${termsOfUse.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Cancel"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${termsOfUse.status}"/></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
