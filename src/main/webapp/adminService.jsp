<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Admin Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
            <p>
            <form action="adminService" method="get">
                <input name="InsertService" type="hidden" value="New">
                <button type="submit" class="btn btn-secondary my-2"><fmt:message key="Add new Service"/></button>
            </form>
            </p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:if test="${newService!=null}">
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">
                                <form method="post" action="adminService">
                                    <div class="text-center">
                                        <input name="new_service_name"/></div>
                                    <br>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Description"/>:</p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left">
                                                <textarea name="new_service_desc"></textarea>
                                            </p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Price"/>:</p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left">
                                                <input name="new_service_price"/>
                                            </p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <input name="new_accept" type="hidden" value="accept">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Accept"/>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <form action="adminService" method="post">
                                            <input name="new_cancel" type="hidden" value="cancel">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Cancel"/>
                                            </button>
                                        </form>
                                    </div>
                                    <small class="text-muted"><fmt:message key="created"/></small>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:forEach var="service" items="${requestScope.services}">
                    <c:choose>
                        <c:when test="${service.status == 'available'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <div class="text-center"><c:out value="${service.name}"/></div>
                                        <br>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Description"/>:</p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><c:out value="${service.description}"/></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Price"/>:</p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><myTags:price price="${service.price}" lang="${lang}"/></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminService" method="post">
                                                    <input name="Update-Service" type="hidden"
                                                           value="<c:out value="${service.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Update information"/>
                                                    </button>
                                                </form>
                                                </form>
                                                <form action="adminService" method="post">
                                                    <input name="Delete-Service" type="hidden"
                                                           value="<c:out value="${service.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Delete this service"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${service.status}"/></small>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${service.status == 'updated'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <form method="post" action="adminService">
                                            <div class="text-center"><input name="service_name"
                                                                            value="<c:out value="${service.name}"/>"/>
                                            </div>
                                            <br>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Description"/>:</p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left">
                                                        <input name="service_desc"
                                                               value="<c:out value="${service.description}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Price"/>:</p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left">
                                                        <input name="service_price"
                                                               value="<c:out value="${service.price}"/>"/>
                                                    </p></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <input name="accept" type="hidden"
                                                           value="<c:out value="${service.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Accept"/>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminService" method="post">
                                                    <input name="cancel" type="hidden"
                                                           value="<c:out value="${service.name}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Cancel"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${service.status}"/></small>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
