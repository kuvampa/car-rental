<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="myTagsLib" uri="MyTag" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Admin Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <img class="d-block mx-auto mb-4" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
                 alt="" width="72" height="72">
            <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
            <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
            <p>
            <form action="adminCars" method="get">
                <input name="insertCar" type="hidden" value="New">
                <button type="submit" class="btn btn-secondary my-2"><fmt:message key="Add new car"/></button>
            </form>
            </p>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:if test="${newCar!=null}">
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">
                                <form method="post" action="adminCars">
                                    <div class="text-center"><input name="new_car_name"/></div>
                                    <br>
                                    <table>
                                        <tbody>
                                        <tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Price for 1 day"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_price"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Type"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_type"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Color"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_color"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Capacity"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_capacity"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Rating"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_rating"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Fuel type"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_fuelType"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Plate number"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_plate"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Manufacturer"/>:</p></td>
                                            <td><p class="text-left"><input name="new_car_manufacturer"/></p></td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Office"/>:</p></td>
                                            <td>
                                                <p class="text-left">
                                                    <select name="new_officeName">
                                                        <c:forEach var="office" items="${requestScope.offices}">
                                                            <c:choose>
                                                                <c:when test="${office.idOffice==car.idOffice}">
                                                                    <option selected="selected"><c:out
                                                                            value="${office.name}"/></option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option><c:out
                                                                            value="${office.name}"/></option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Terms of use"/>:</p></td>
                                            <td>
                                                <p class="text-left">
                                                    <select name="new_termName">
                                                        <c:forEach var="term" items="${requestScope.terms}">
                                                            <c:choose>
                                                                <c:when test="${term.idTermsPackage==car.idTermsPackage}">
                                                                    <option selected="selected"><c:out
                                                                            value="${term.name}"/></option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option><c:out
                                                                            value="${term.name}"/></option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><p class="text-left"><fmt:message key="Insurance package"/>:</p></td>
                                            <td>
                                                <p class="text-left">
                                                    <select name="new_insuranceName">
                                                        <c:forEach var="insurance"
                                                                   items="${requestScope.insurances}">
                                                            <c:choose>
                                                                <c:when test="${insurance.idInsurance==car.idInsurance}">
                                                                    <option selected="selected"><c:out
                                                                            value="${insurance.name}"/></option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option><c:out
                                                                            value="${insurance.name}"/></option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <input name="new_accept" type="hidden" value="accept">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Accept"/>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <form action="adminCars" method="post">
                                            <input name="new_cancel" type="hidden" value="cancel">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                <fmt:message key="Cancel"/>
                                            </button>
                                        </form>
                                    </div>
                                    <small class="text-muted"><fmt:message key="created"/></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:forEach var="car" items="${requestScope.cars}">
                    <c:choose>
                        <c:when test="${car.status == 'available'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <img class="card-img-top"
                                         data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                         alt="Thumbnail [100%x300]" style="height: 300px; width: 100%; display: block;"
                                         src="${pageContext.request.contextPath}/file/cars/<c:out value="${car.plateNumber}"/>.jpg"
                                         data-holder-rendered="true">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <div class="text-center"><c:out value="${car.carName}"/></div>
                                        <br>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Price for 1 day"/>:</p>
                                                </td>
                                                <td><p class="text-center"><myTags:price price="${car.price}" lang="${lang}"/></p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Type"/>:</p></td>
                                                <td><p class="text-center"><c:out value="${car.type}"/></p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Rating"/>:</p></td>
                                                <td><p class="text-center"><myTagsLib:rating rating="${car.rating}"/></p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text-center"><fmt:message key="Fuel type"/>:</p></td>
                                                <td><p class="text-center"><c:out value="${car.fuelType}"/></p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminCars" method="post">
                                                    <input name="Update-Car" type="hidden"
                                                           value="<c:out value="${car.plateNumber}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Update information"/>
                                                    </button>
                                                </form>
                                                </form>
                                                <form action="adminCars" method="post">
                                                    <input name="Delete-Car" type="hidden"
                                                           value="<c:out value="${car.plateNumber}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Delete this car"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${car.status}"/></small>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${car.status == 'updated'}">
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <img class="card-img-top"
                                         data-src="holder.js/100px300?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                         alt="Thumbnail [100%x300]"
                                         style="height: 300px; width: 100%; display: block;"
                                         src="${pageContext.request.contextPath}/file/cars/<c:out value="${car.plateNumber}"/>.jpg"
                                         data-holder-rendered="true">
                                    <div class="card-body">
                                        <p class="card-text">
                                        <form method="post" action="adminCars" enctype="multipart/form-data">
                                            <input name="newImage" type="hidden"
                                                   value="<c:out value="${car.plateNumber}"/>">
                                            <input type="file" name="fileName">
                                            <input type="submit" value="upload">
                                        </form>
                                        <form method="post" action="adminCars">
                                            <div class="text-center"><input name="car_name"
                                                                            value="<c:out value="${car.carName}"/>"/>
                                            </div>
                                            <br>
                                            <table>
                                                <tbody>
                                                <tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Price for 1 day"/>:</p>
                                                    </td>
                                                    <td><p class="text-left"><input name="car_price"
                                                                                    value="<c:out value="${car.price}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Type"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_type"
                                                                                    value="<c:out value="${car.type}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Color"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_color"
                                                                                    value="<c:out value="${car.color}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Capacity"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_capacity"
                                                                                    value="<c:out value="${car.capacity}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Rating"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_rating"
                                                                                    value="<c:out value="${car.rating}"/>"/>
                                                    </p></td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Fuel type"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_fuelType"
                                                                                    value="<c:out value="${car.fuelType}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Plate number"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_plate"
                                                                                    value="<c:out value="${car.plateNumber}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Manufacturer"/>:</p></td>
                                                    <td><p class="text-left"><input name="car_manufacturer"
                                                                                    value="<c:out value="${car.manufacturer}"/>"/>
                                                    </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Office"/>:</p></td>
                                                    <td>
                                                        <p class="text-left">
                                                            <select name="officeName">
                                                                <c:forEach var="office" items="${requestScope.offices}">
                                                                    <c:choose>
                                                                        <c:when test="${office.idOffice==car.idOffice}">
                                                                            <option selected="selected"><c:out
                                                                                    value="${office.name}"/></option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option><c:out
                                                                                    value="${office.name}"/></option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </select>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Terms of use"/>:</p></td>
                                                    <td>
                                                        <p class="text-left">
                                                            <select name="termName">
                                                                <c:forEach var="term" items="${requestScope.terms}">
                                                                    <c:choose>
                                                                        <c:when test="${term.idTermsPackage==car.idTermsPackage}">
                                                                            <option selected="selected"><c:out
                                                                                    value="${term.name}"/></option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option><c:out
                                                                                    value="${term.name}"/></option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </select>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><p class="text-left"><fmt:message key="Insurance package"/>:</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-left">
                                                            <select name="insuranceName">
                                                                <c:forEach var="insurance"
                                                                           items="${requestScope.insurances}">
                                                                    <c:choose>
                                                                        <c:when test="${insurance.idInsurance==car.idInsurance}">
                                                                            <option selected="selected"><c:out
                                                                                    value="${insurance.name}"/></option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option><c:out
                                                                                    value="${insurance.name}"/></option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </select>
                                                        </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <input name="accept" type="hidden"
                                                           value="<c:out value="${car.plateNumber}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Accept"/>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <form action="adminCars" method="post">
                                                    <input name="cancel" type="hidden"
                                                           value="<c:out value="${car.plateNumber}"/>">
                                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                                        <fmt:message key="Cancel"/>
                                                    </button>
                                                </form>
                                            </div>
                                            <small class="text-muted"><c:out value="${car.status}"/></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</main>
<%@include file="footer.jspf" %>
</body>
</html>
