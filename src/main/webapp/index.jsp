<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <style>
        <%@include file="css/style.css" %>
    </style>
    <title>Pluto</title>
</head>
<body class="text-center">
<div class="bg-image"
     style="background-image: url('https://images5.alphacoders.com/682/682929.jpg'); height: 100vh">
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
        <form class="form-signin" action="${pageContext.request.contextPath}/app/start" method="get">
            <p class="lead">
                <input name="defaultLanguage" type="hidden" value="en">
            <div class="checkbox mb-3">
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Start</button>
            </p>
        </form>
    </div>
</div>
</body>
</html>
