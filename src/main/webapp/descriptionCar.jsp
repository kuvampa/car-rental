<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html>
<head>
    <myTags:header/>
    <title><fmt:message key="Description Pluto"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<section class="jumbotron text-center">
    <div class="container">
        <img class="d-block mx-auto mb-4"
             src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Nh-pluto-in-true-color_2x.jpg/1024px-Nh-pluto-in-true-color_2x.jpg"
             alt="" width="72" height="72">
        <h1 class="jumbotron-heading"><fmt:message key="Pluto"/></h1>
        <p class="lead text-muted"><fmt:message key="For the best people the best cars at the best prices."/></p>
    </div>
</section>
<div class="container">
        <div class="col-md-8 order-md-1">
            <div class="row">
                <h3 class="mb-3"><c:out value="${carDescr.carName}"/></h3>
                <div class="col-md-6 mb-3">
                    <label><fmt:message key="Photo"/></label>
                    <img width="350" height="350"
                         src="${pageContext.request.contextPath}/file/cars/<c:out value="${carDescr.plateNumber}"/>.jpg">
                </div>
                <div class="col-md-6 mb-3">
                    <label><c:out value="${officeDescr.location}"/></label>
                    <iframe src="<c:out value="${officeDescr.map}"/>"
                            width="350" height="350" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <legend></legend>
                <div class="col-md-6 mb-3 text-right">
                    <h4>
                        <label><fmt:message key="Price for 1 day"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Type"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Color"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Capacity"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Rating"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Manufacturer"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Fuel type"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Mileage Limit"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Fuel Conditions"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Name of insurance package"/></label><br>
                        <legend></legend>
                        <label><fmt:message key="Description of insurance package"/></label><br>
                    </h4>
                </div>
                <div class="col-md-6 mb-3">
                    <h4>
                        <label><myTags:price price="${carDescr.price}" lang="${lang}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.type}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.color}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.capacity}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.rating}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.manufacturer}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${carDescr.fuelType}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${termsOfUseDescr.mileageLimit}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${termsOfUseDescr.fuelConditions}"/></label><br>
                        <legend></legend>
                        <label><c:out value="${insurancePackageDescr.name}"/></label><br>
                        <legend></legend>
                        <label> <c:out value="${insurancePackageDescr.description}"/></label><br>
                    </h4>
                </div>
                <legend></legend>
                <div class="col-md-6 mb-3">
                    <h4>
                        <form action="rent" method="get">
                            <input name="Car_plate" type="hidden" value="<c:out value="${carDescr.plateNumber}"/>">
                            <input type="submit" value="Book this car">
                        </form>
                    </h4>
                </div>
            </div>
        </div>
</div>
<%@include file="footer.jspf" %>
</body>
</html>