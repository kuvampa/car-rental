package carrental.db.dao;


import carrental.db.DBException;

import java.util.List;

public interface DAO<T> {

    List<T> findAll() throws DBException;

    void insert(T t) throws DBException;

    void update(T t) throws DBException;

    void deleteById(int id) throws DBException;

    T findById(int id) throws DBException;
}
