package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.TermsOfUse;


public interface TermsDAO extends DAO<TermsOfUse> {
    void updateTermStatus(String name, String status) throws DBException;

    TermsOfUse findByName(String name) throws DBException;
}
