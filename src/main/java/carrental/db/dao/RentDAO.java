package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Rental;

import java.time.LocalDateTime;
import java.util.List;

public interface RentDAO extends DAO<Rental>{

    List<Rental> findByIdSortByReturnDate(int idCar) throws DBException;

    List<Rental> findByIdSortByReceivingDate(int idCar) throws DBException;

    Rental findByDate(LocalDateTime rec, LocalDateTime ret) throws DBException;
}
