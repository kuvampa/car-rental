package carrental.db.dao;


import carrental.db.DBException;
import carrental.db.entity.Accident;

public interface AccidentDAO extends DAO<Accident>{
    void updateAccidentStatus(int id, String status) throws DBException;

    void updateAccidentManager(int id, int manager) throws DBException;
}
