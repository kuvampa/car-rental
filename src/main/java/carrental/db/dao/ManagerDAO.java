package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Manager;

public interface ManagerDAO extends DAO<Manager>{
    Manager findManager(String login) throws DBException;

    void deleteByName(String login) throws DBException;

    void updateManagerDocs(String login, String docs) throws DBException;
}
