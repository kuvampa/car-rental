package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Car;

import java.util.List;


public interface CarDAO extends DAO<Car> {
    int countSortedCars(String sort, List<String> manufactures, String name, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity) throws DBException;

    List<Car> findSortedLimitedCars(int pagination, String sort, List<String> manufactures, String name, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity) throws DBException;

    List<String> findUniqueManufacture() throws DBException;

    List<String> findUniqueColor() throws DBException;

    List<Integer> findUniqueRating() throws DBException;

    List<String> findUniqueType() throws DBException;

    List<String> findUniqueFuelType() throws DBException;

    List<Integer> findUniqueCapacity() throws DBException;

    void updateCarStatus(String plate, String status) throws DBException;

    Car findCarByPlate(String plateNumber) throws DBException;
}
