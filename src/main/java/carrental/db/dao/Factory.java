package carrental.db.dao;

import carrental.db.dao.impl.FactoryDAOImpl;

public abstract class Factory {
    public abstract AdminDAO getAdminDAO();

    public abstract UserDAO getUserDAO();

    public abstract CarDAO getCarDAO();

    public abstract InsuranceDAO getInsuranseDAO();

    public abstract OfficeDAO getOfficeDAO();

    public abstract RentDAO getRentDAO();

    public abstract ServiceDAO getServiceDAO();

    public abstract TermsDAO getTermsDAO();

    public abstract ServiceRentDAO getServiceRentDAO();

    public abstract ManagerDAO getManagerDAO();

    public abstract AccidentDAO getAccidentDAO();

    public abstract VerificationDAO getVerificationDAO();
    public abstract UserSupportDAO getUserSupportDAO();

    public static Factory getDAOFactory() {
        return new FactoryDAOImpl();
    }
}
