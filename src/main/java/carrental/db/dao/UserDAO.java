package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.User;


public interface UserDAO extends DAO<User> {

    User findUser(String login) throws DBException;

    void deleteByName(String login) throws DBException;

    void updateUserDocs(String login, String docs) throws DBException;

    void updateUserStatus(String login, int status) throws DBException;
}
