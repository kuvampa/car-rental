package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.InsuranceDAO;
import carrental.db.entity.InsurancePackage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class InsuranceDAOImpl implements InsuranceDAO {
    private static final Logger logger = LogManager.getLogger(InsuranceDAOImpl.class);
    public static final String SELECT_ALL_INSURANCE = "SELECT * FROM insurance_package";
    public static final String SELECT_INSURANCE_BY_ID = "SELECT * FROM insurance_package WHERE id_insurance = ?";
    public static final String SELECT_INSURANCE_BY_NAME = "SELECT * FROM insurance_package WHERE name = ?";
    public static final String INSERT_NEW_INSURANCE = "INSERT INTO insurance_package(name, description, status) VALUES(?,?,?)";
    public static final String UPDATE_INSURANCE_BY_ID = "UPDATE insurance_package SET name = ?, description = ?, status = ? WHERE id_insurance = ?";
    public static final String DELETE_INSURANCE_BY_ID = "DELETE FROM insurance_package WHERE (id_insurance = ?)";
    public static final String UPDATE_INSURANCE_STATUS_BY_NAME = "UPDATE insurance_package SET status = ?  WHERE (name = ?)";
    private final Connection connection;

    public InsuranceDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<InsurancePackage> findAll() throws DBException {
        logger.debug("a find all insurance query");
        List<InsurancePackage> insurancePackages = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_INSURANCE)) {
            while (resultSet.next()) {
                InsurancePackage insurancePackage = new InsurancePackage();
                insurancePackage.setIdInsurance(resultSet.getInt("id_insurance"));
                insurancePackage.setDescription(resultSet.getString("description"));
                insurancePackage.setName(resultSet.getString("name"));
                insurancePackage.setStatus(resultSet.getString("status"));
                insurancePackages.add(insurancePackage);
            }
        } catch (SQLException e) {
            logger.error("can't find all insurance packages", e);
            throw new DBException("can't find all insurance packages", e);
        }
        logger.debug("Found [{}] insurance ", insurancePackages.size());
        return insurancePackages;
    }

    @Override
    public void insert(InsurancePackage insurancePackage) throws DBException {
        logger.debug("insert insurancePackage query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_INSURANCE)) {
            preparedStatement.setString(1, insurancePackage.getName());
            preparedStatement.setString(2, insurancePackage.getDescription());
            preparedStatement.setString(3, insurancePackage.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] insurance", insurancePackage);
        } catch (SQLException e) {
            logger.error("can't add new insurance package", e);
            throw new DBException("can't add new insurance package", e);
        }
    }

    @Override
    public void update(InsurancePackage insurancePackage) throws DBException {
        logger.debug("update insurancePackage query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_INSURANCE_BY_ID)) {
            preparedStatement.setString(1, insurancePackage.getName());
            preparedStatement.setString(2, insurancePackage.getDescription());
            preparedStatement.setString(3, insurancePackage.getStatus());
            preparedStatement.setInt(4, insurancePackage.getIdInsurance());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] insurance", insurancePackage);
        } catch (SQLException e) {
            logger.error("can't update insurance package", e);
            throw new DBException("can't update insurance package", e);
        }
    }

    @Override
    public void updateInsuranceStatus(String name, String status) throws DBException {
        logger.debug("update insurancePackage status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_INSURANCE_STATUS_BY_NAME)) {
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, name);
            preparedStatement.executeUpdate();
            logger.debug("Update insurance status [{}] by [{}] name", status, name);
        } catch (SQLException e) {
            logger.error("can't update insurance status", e);
            throw new DBException("can't update insurance status", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete insurancePackage query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_INSURANCE_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete insurance by [{}] id", id);
        } catch (SQLException e) {
            logger.error("can't delete insurance package by id", e);
            throw new DBException("can't delete insurance package by id", e);
        }
    }

    @Override
    public InsurancePackage findById(int idInsurance) throws DBException {
        logger.debug("Find insurance by id query");
        InsurancePackage insurancePackage = new InsurancePackage();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INSURANCE_BY_ID)) {
            preparedStatement.setInt(1, idInsurance);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                insurancePackage.setIdInsurance(resultSet.getInt("id_insurance"));
                insurancePackage.setDescription(resultSet.getString("description"));
                insurancePackage.setName(resultSet.getString("name"));
                insurancePackage.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find insurance by id [{}]", insurancePackage);
        } catch (SQLException e) {
            logger.error("can't find insurance by id", e);
            throw new DBException("can't find insurance by id", e);
        }
        return insurancePackage;
    }

    @Override
    public InsurancePackage findByName(String name) throws DBException {
        logger.debug("Find insurance by id query");
        InsurancePackage insurancePackage = new InsurancePackage();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INSURANCE_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                insurancePackage.setIdInsurance(resultSet.getInt("id_insurance"));
                insurancePackage.setDescription(resultSet.getString("description"));
                insurancePackage.setName(resultSet.getString("name"));
                insurancePackage.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find insurance by name [{}]", insurancePackage);
        } catch (SQLException e) {
            logger.error("can't find insurance by name", e);
            throw new DBException("can't find insurance by name", e);
        }
        return insurancePackage;
    }
}
