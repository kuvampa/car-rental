package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.VerificationDAO;
import carrental.db.entity.Verification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VerificationDAOImpl implements VerificationDAO {
    private static final Logger logger = LogManager.getLogger(VerificationDAOImpl.class);
    private final Connection connection;
    public static final String INSERT_NEW_VERIFICATION = "INSERT INTO verification_of_documents(id_rental, status) VALUES(?,?)";
    public static final String SELECT_ALL_VERIFICATIONS = "SELECT * FROM verification_of_documents";
    public static final String SELECT_VERIFICATION_BY_ID = "SELECT * FROM verification_of_documents WHERE id_verification_of_documents = ?";
    public static final String SELECT_VERIFICATION_BY_RENT_ID = "SELECT * FROM verification_of_documents WHERE id_rental = ?";
    public static final String UPDATE_VERIFICATION_BY_ID = "UPDATE verification_of_documents SET id_rental = ?, id_manager = ?, status = ?, comment = ? WHERE id_verification_of_documents = ?";
    public static final String DELETE_VERIFICATION_BY_ID = "DELETE FROM verification_of_documents WHERE (id_verification_of_documents = ?)";
    public static final String UPDATE_VERIFICATION_STATUS_BY_ID = "UPDATE verification_of_documents SET status = ?  WHERE (id_verification_of_documents = ?)";
    public static final String UPDATE_VERIFICATION_MANAGER_BY_ID = "UPDATE verification_of_documents SET id_manager = ?  WHERE (id_verification_of_documents = ?)";

    public VerificationDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Verification> findAll() throws DBException {
        logger.debug("a find all verifications of use query");
        List<Verification> verifications = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_VERIFICATIONS)) {
            while (resultSet.next()) {
                Verification verification = new Verification();
                verification.setIdVerificationOfDocuments(resultSet.getInt("id_verification_of_documents"));
                verification.setIdRental(resultSet.getInt("id_rental"));
                verification.setIdManager(resultSet.getInt("id_manager"));
                verification.setStatus(resultSet.getString("status"));
                verification.setComment(resultSet.getString("comment"));
                verifications.add(verification);
            }
        } catch (SQLException e) {
            logger.error("can't find all Verifications", e);
            throw new DBException("can't find all Verifications", e);
        }
        logger.debug("Found [{}] verifications", verifications.size());
        return verifications;
    }

    @Override
    public void insert(Verification verification) throws DBException {
        logger.debug("insert verification query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_VERIFICATION)) {
            preparedStatement.setInt(1, verification.getIdRental());
            preparedStatement.setString(2, verification.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] terms", verification);
        } catch (SQLException e) {
            logger.error("can't add new verification", e);
            throw new DBException("can't add new verification", e);
        }
    }

    @Override
    public void update(Verification verification) throws DBException {
        logger.debug("Update verification query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_VERIFICATION_BY_ID)) {
            preparedStatement.setInt(1, verification.getIdRental());
            preparedStatement.setInt(2, verification.getIdManager());
            preparedStatement.setString(3, verification.getStatus());
            preparedStatement.setString(4, verification.getComment());
            preparedStatement.setInt(5, verification.getIdVerificationOfDocuments());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] verification", verification);
        } catch (SQLException e) {
            logger.error("can't update verification", e);
            throw new DBException("can't update verification", e);
        }
    }

    @Override
    public void updateVerificationStatus(int id, String status) throws DBException {
        logger.debug("Update verification status by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_VERIFICATION_STATUS_BY_ID)) {
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update terms status [{}] by id [{}]", status, id);
        } catch (SQLException e) {
            logger.error("can't update verification status", e);
            throw new DBException("can't update verification status", e);
        }
    }
    @Override
    public void updateVerificationManager(int id, int manager) throws DBException {
        logger.debug("Update verification manager by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_VERIFICATION_MANAGER_BY_ID)) {
            preparedStatement.setInt(1, manager);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update verification manager [{}] by id [{}]", manager, id);
        } catch (SQLException e) {
            logger.error("can't update verification manager", e);
            throw new DBException("can't update verification manager", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete verification query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_VERIFICATION_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete verification by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete verification by id", e);
            throw new DBException("can't delete verification by id", e);
        }
    }

    @Override
    public Verification findById(int id) throws DBException {
        logger.debug("Find verification by id query");
        Verification verification = new Verification();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_VERIFICATION_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                verification.setIdVerificationOfDocuments(resultSet.getInt("id_verification_of_documents"));
                verification.setIdRental(resultSet.getInt("id_rental"));
                verification.setIdManager(resultSet.getInt("id_manager"));
                verification.setStatus(resultSet.getString("status"));
                verification.setComment(resultSet.getString("comment"));
            }
            logger.debug("Find verification by id [{}]", verification);
        } catch (SQLException e) {
            logger.error("can't find verification by id", e);
            throw new DBException("can't find verification by id", e);
        }
        return verification;
    }
    @Override
    public Verification findByRentId(int id) throws DBException {
        logger.debug("Find verification by rent id query");
        Verification verification = new Verification();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_VERIFICATION_BY_RENT_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                verification.setIdVerificationOfDocuments(resultSet.getInt("id_verification_of_documents"));
                verification.setIdRental(resultSet.getInt("id_rental"));
                verification.setIdManager(resultSet.getInt("id_manager"));
                verification.setStatus(resultSet.getString("status"));
                verification.setComment(resultSet.getString("comment"));
            }
            logger.debug("Find verification by rent id [{}]", verification);
        } catch (SQLException e) {
            logger.error("can't find verification by rent id", e);
            throw new DBException("can't find verification by rent id", e);
        }
        return verification;
    }
}
