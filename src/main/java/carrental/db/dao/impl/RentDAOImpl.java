package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.RentDAO;
import carrental.db.entity.Rental;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RentDAOImpl implements RentDAO {

    private static final Logger logger = LogManager.getLogger(RentDAOImpl.class);
    private final Connection connection;
    public static final String INSERT_NEW_RENT = "INSERT INTO rental(id_user, id_car, date_of_receiving, return_date, return_place, price, right_of_cancellation) VALUES(?,?,?,?,?,?,?)";
    public static final String SELECT_ALL_RENTS = "SELECT * FROM rental";
    public static final String SELECT_RENT_BY_ID = "SELECT * FROM rental WHERE id_rental = ?";
    public static final String SELECT_RENT_BY_DATE = "SELECT * FROM rental WHERE date_of_receiving = ? AND return_date = ?";
    public static final String SELECT_CARS_BY_ID_AND_SORT_BY_RECEIVING_DATE = "SELECT * FROM rental WHERE id_car = ? ORDER BY date_of_receiving";
    public static final String SELECT_CARS_BY_ID_AND_SORT_BY_RETURN_DATE = "SELECT * FROM rental WHERE id_car = ? ORDER BY return_date";
    public static final String UPDATE_RENT_BY_ID = "UPDATE rental SET id_user = ?, id_car = ?, date_of_receiving = ? , return_date = ? , return_place = ? , price = ? , right_of_cancellation = ? WHERE id_rental = ?";
    public static final String DELETE_RENT_BY_ID = "DELETE FROM rental WHERE (id_rental = ?)";


    public RentDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Rental> findAll() throws DBException {
        logger.debug("a find all rents query");
        List<Rental> rentals = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_RENTS)) {
            while (resultSet.next()) {
                Rental rental = new Rental();
                rental.setIdRental(resultSet.getInt("id_rental"));
                rental.setIdCar(resultSet.getInt("id_car"));
                rental.setPrice(resultSet.getDouble("price"));
                rental.setIdUser(resultSet.getInt("id_user"));
                rental.setReturnDate((LocalDateTime) resultSet.getObject("return_date"));
                rental.setDateOfReceiving((LocalDateTime) resultSet.getObject("date_of_receiving"));
                rental.setReturnPlace(resultSet.getString("return_place"));
                rental.setRightOfCancellation(resultSet.getInt("right_of_cancellation"));
                rentals.add(rental);
            }
        } catch (SQLException e) {
            logger.error("can't find all rents", e);
            throw new DBException("can't find all rents", e);
        }
        logger.debug("Found [{}] rents ", rentals.size());
        return rentals;
    }

    @Override
    public List<Rental> findByIdSortByReturnDate(int idCar) throws DBException {
        logger.debug("a find all rents by id sort by return date query");
        List<Rental> rentals = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_CARS_BY_ID_AND_SORT_BY_RETURN_DATE)) {
            pStmt.setInt(1, idCar);
            try (ResultSet resultSet = pStmt.executeQuery()) {
                while (resultSet.next()) {
                    Rental rental = new Rental();
                    rental.setIdRental(resultSet.getInt("id_rental"));
                    rental.setIdCar(resultSet.getInt("id_car"));
                    rental.setPrice(resultSet.getDouble("price"));
                    rental.setIdUser(resultSet.getInt("id_user"));
                    rental.setReturnDate((LocalDateTime) resultSet.getObject("return_date"));
                    rental.setDateOfReceiving((LocalDateTime) resultSet.getObject("date_of_receiving"));
                    rental.setReturnPlace(resultSet.getString("return_place"));
                    rental.setRightOfCancellation(resultSet.getInt("right_of_cancellation"));
                    rentals.add(rental);
                }
            }
        } catch (SQLException e) {
            logger.error("can't find rents by car id sort by return date", e);
            throw new DBException("can't find rents by car id sort by return date", e);
        }
        logger.debug("Found [{}] rents ", rentals.size());
        return rentals;
    }

    @Override
    public List<Rental> findByIdSortByReceivingDate(int idCar) throws DBException {
        logger.debug("a find all rents by id sort by return date query");
        List<Rental> rentals = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_CARS_BY_ID_AND_SORT_BY_RECEIVING_DATE)) {
            pStmt.setInt(1, idCar);
            try (ResultSet resultSet = pStmt.executeQuery()) {
                while (resultSet.next()) {
                    Rental rental = new Rental();
                    rental.setIdRental(resultSet.getInt("id_rental"));
                    rental.setIdCar(resultSet.getInt("id_car"));
                    rental.setPrice(resultSet.getDouble("price"));
                    rental.setIdUser(resultSet.getInt("id_user"));
                    rental.setReturnDate((LocalDateTime) resultSet.getObject("return_date"));
                    rental.setDateOfReceiving((LocalDateTime) resultSet.getObject("date_of_receiving"));
                    rental.setReturnPlace(resultSet.getString("return_place"));
                    rental.setRightOfCancellation(resultSet.getInt("right_of_cancellation"));
                    rentals.add(rental);
                }
            }
        } catch (SQLException e) {
            logger.error("can't find rents by car id sort by receiving date", e);
            throw new DBException("can't find rents by car id sort by receiving date", e);
        }
        logger.debug("Found [{}] rents ", rentals.size());
        return rentals;
    }

    @Override
    public void insert(Rental rental) throws DBException {
        logger.debug("insert rent query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_RENT)) {
            preparedStatement.setInt(1, rental.getIdUser());
            preparedStatement.setInt(2, rental.getIdCar());
            preparedStatement.setString(3, String.valueOf(rental.getDateOfReceiving()));
            preparedStatement.setString(4, String.valueOf(rental.getReturnDate()));
            preparedStatement.setString(5, rental.getReturnPlace());
            preparedStatement.setDouble(6, rental.getPrice());
            preparedStatement.setInt(7, rental.getRightOfCancellation());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] rent", rental);
        } catch (SQLException e) {
            logger.error("can't add new rent", e);
            throw new DBException("can't add new rent", e);
        }
    }

    @Override
    public void update(Rental rental) throws DBException {
        logger.debug("update rent query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RENT_BY_ID)) {
            preparedStatement.setInt(1, rental.getIdUser());
            preparedStatement.setInt(2, rental.getIdCar());
            preparedStatement.setString(3, String.valueOf(rental.getDateOfReceiving()));
            preparedStatement.setString(4, String.valueOf(rental.getReturnDate()));
            preparedStatement.setString(5, rental.getReturnPlace());
            preparedStatement.setDouble(6, rental.getPrice());
            preparedStatement.setInt(7, rental.getRightOfCancellation());
            preparedStatement.setInt(8, rental.getIdRental());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] rent", rental);
        } catch (SQLException e) {
            logger.error("can't update rent", e);
            throw new DBException("can't update rent", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete rent by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_RENT_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete rent by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete rent by id", e);
            throw new DBException("can't delete rent by id", e);
        }
    }

    @Override
    public Rental findById(int id) throws DBException {
        logger.debug("Find rent by id query");
        Rental rental = new Rental();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RENT_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                rental.setIdRental(resultSet.getInt("id_rental"));
                rental.setIdCar(resultSet.getInt("id_car"));
                rental.setPrice(resultSet.getDouble("price"));
                rental.setIdUser(resultSet.getInt("id_user"));
                rental.setReturnDate((LocalDateTime) resultSet.getObject("return_date"));
                rental.setDateOfReceiving((LocalDateTime) resultSet.getObject("date_of_receiving"));
                rental.setReturnPlace(resultSet.getString("return_place"));
                rental.setRightOfCancellation(resultSet.getInt("right_of_cancellation"));
            }
            logger.debug("Find rent by id [{}]", rental);
        } catch (SQLException e) {
            logger.error("can't find rental by id", e);
            throw new DBException("can't find rental by id", e);
        }
        logger.debug("Find rent by id [{}]", rental);
        return rental;
    }

    @Override
    public Rental findByDate(LocalDateTime rec, LocalDateTime ret) throws DBException {
        logger.debug("Find rental by id query");
        Rental rental = new Rental();
        String recS = String.valueOf(rec);
        String retS = String.valueOf(ret);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RENT_BY_DATE)) {
            preparedStatement.setObject(1, recS);
            preparedStatement.setObject(2, retS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                rental.setIdRental(resultSet.getInt("id_rental"));
                rental.setIdCar(resultSet.getInt("id_car"));
                rental.setPrice(resultSet.getDouble("price"));
                rental.setIdUser(resultSet.getInt("id_user"));
                rental.setReturnDate((LocalDateTime) resultSet.getObject("return_date"));
                rental.setDateOfReceiving((LocalDateTime) resultSet.getObject("date_of_receiving"));
                rental.setReturnPlace(resultSet.getString("return_place"));
                rental.setRightOfCancellation(resultSet.getInt("right_of_cancellation"));
            }
            logger.debug("Find rent by date [{}]", rental);
        } catch (SQLException e) {
            logger.error("can't find rental by date", e);
            throw new DBException("can't find rental by date", e);
        }
        logger.debug("Find rent by date [{}]", rental);
        return rental;
    }
}
