package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.UserDAO;
import carrental.db.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {
    private static final Logger logger = LogManager.getLogger(UserDAOImpl.class);
    private final Connection connection;
    public static final String INSERT_NEW_USER = "INSERT INTO user(status, full_name, telephone_number, login, password, email) VALUES(?,?,?,?,?,?)";
    public static final String UPDATE_USER_BY_ID = "UPDATE user SET full_name = ?, telephone_number = ?, email = ?, login= ?, password = ?  WHERE id_user = ?";
    public static final String SELECT_ALL_USER = "SELECT * FROM user";
    public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM user WHERE login = ?";
    public static final String SELECT_USER_BY_ID = "SELECT * FROM user WHERE id_user = ?";
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM user WHERE (login = ?)";
    public static final String DELETE_USER_BY_ID = "DELETE FROM user WHERE (id_user = ?)";
    public static final String UPDATE_USER_DOCS_BY_NAME = "UPDATE user SET documents = ?  WHERE (login = ?)";
    public static final String UPDATE_USER_STATUS_BY_NAME = "UPDATE user SET status = ?  WHERE (login = ?)";

    public UserDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void insert(User user) throws DBException {
        logger.debug("insert user query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_USER)) {
            preparedStatement.setInt(1, user.getStatus());
            preparedStatement.setString(2, user.getFullName());
            preparedStatement.setString(3, user.getTelephoneNumber());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setString(6, user.getEmail());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] user", user);
        } catch (SQLException e) {
            logger.error("can't add new user", e);
            throw new DBException("can't add new user", e);
        }
    }

    @Override
    public List<User> findAll() throws DBException {
        logger.debug("a find all users query");
        List<User> users = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_USER)) {
            while (resultSet.next()) {
                User user = new User();
                user.setIdUser(resultSet.getInt("id_user"));
                user.setStatus(resultSet.getInt("status"));
                user.setFullName(resultSet.getString("full_name"));
                user.setTelephoneNumber(resultSet.getString("telephone_number"));
                user.setEmail(resultSet.getString("email"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setDocuments(resultSet.getString("documents"));
                users.add(user);
            }
        } catch (SQLException e) {
            logger.error("can't find all users", e);
            throw new DBException("can't find all users", e);
        }
        logger.debug("Found [{}] users ", users.size());
        return users;
    }

    @Override
    public User findUser(String login) throws DBException {
        logger.debug("Find user by login query");
        User user = new User();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setIdUser(resultSet.getInt("id_user"));
                user.setStatus(resultSet.getInt("status"));
                user.setFullName(resultSet.getString("full_name"));
                user.setTelephoneNumber(resultSet.getString("telephone_number"));
                user.setEmail(resultSet.getString("email"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find user by login [{}]", user);
        } catch (SQLException e) {
            logger.error("can't find user by login", e);
            throw new DBException("can't find user by login", e);
        }
        return user;
    }

    @Override
    public User findById(int id) throws DBException {
        logger.debug("Find user by id query");
        User user = new User();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setIdUser(resultSet.getInt("id_user"));
                user.setStatus(resultSet.getInt("status"));
                user.setFullName(resultSet.getString("full_name"));
                user.setTelephoneNumber(resultSet.getString("telephone_number"));
                user.setEmail(resultSet.getString("email"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find user by id [{}]", user);
        } catch (SQLException e) {
            logger.error("can't find user by id", e);
            throw new DBException("can't find user by id", e);
        }
        return user;
    }

    @Override
    public void deleteByName(String login) throws DBException {
        logger.debug("delete user by name query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            preparedStatement.executeUpdate();
            logger.debug("Delete user by login [{}]", login);
        } catch (SQLException e) {
            logger.error("can't delete user by login", e);
            throw new DBException("can't delete user by login", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete user by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete user by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete user by id", e);
            throw new DBException("can't delete user by id", e);
        }
    }

    @Override
    public void update(User user) throws DBException {
        logger.debug("update user query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_BY_ID)) {
            preparedStatement.setString(1, user.getFullName());
            preparedStatement.setString(2, user.getTelephoneNumber());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setInt(6, user.getIdUser());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] user", user);
        } catch (SQLException e) {
            logger.error("can't update user", e);
            throw new DBException("can't update user", e);
        }
    }

    @Override
    public void updateUserDocs(String login, String docs) throws DBException {
        logger.debug("update user documents query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_DOCS_BY_NAME)) {
            preparedStatement.setString(1, docs);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();
            logger.debug("Update user documents [{}] by [{}] login", docs, login);
        } catch (SQLException e) {
            logger.error("can't update user docs", e);
            throw new DBException("can't update user docs", e);
        }
    }

    @Override
    public void updateUserStatus(String login, int status) throws DBException {
        logger.debug("update user status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_STATUS_BY_NAME)) {
            preparedStatement.setInt(1, status);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();
            logger.debug("Update user status [{}] by [{}] login", status, login);
        } catch (SQLException e) {
            logger.error("can't update user status", e);
            throw new DBException("can't update user status", e);
        }
    }
}
