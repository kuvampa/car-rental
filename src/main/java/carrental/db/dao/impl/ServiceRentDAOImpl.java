package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.ServiceRentDAO;
import carrental.db.entity.ServiceRent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceRentDAOImpl implements ServiceRentDAO {
    private static final Logger logger = LogManager.getLogger(ServiceRentDAOImpl.class);
    public static final String SELECT_ALL_SERVICES_IN_RENTS = "SELECT * FROM service_rent";
    public static final String SELECT_SERVICE_BY_ID = "SELECT * FROM service_rent WHERE id_service = ?";
    public static final String INSERT_NEW_SERVICES_IN_RENTS = "INSERT INTO service_rent(id_rent, id_service) VALUES(?,?)";
    public static final String UPDATE_SERVICES_IN_RENTS_BY_ID = "UPDATE service_rent SET id_rent = ?, id_service = ? WHERE id_rent = ?";
    public static final String DELETE_SERVICES_IN_RENTS_BY_ID = "DELETE FROM service_rent WHERE (id_rent = ?)";
    private final Connection connection;

    public ServiceRentDAOImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public List<ServiceRent> findAll() throws DBException {
        logger.debug("a find all services in rents query");
        List<ServiceRent> serviceRents = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_SERVICES_IN_RENTS)) {
            while (resultSet.next()) {
                ServiceRent serviceRent = new ServiceRent();
                serviceRent.setIdService(resultSet.getInt("id_service"));
                serviceRent.setIdRental(resultSet.getInt("id_rent"));
                serviceRent.setIdRentService(resultSet.getInt("id_rent_service"));
                serviceRents.add(serviceRent);
            }
        } catch (SQLException e) {
            logger.error("can't find all services in rents", e);
            throw new DBException("can't find all services in rents", e);
        }
        logger.debug("Found [{}] services in rents", serviceRents.size());
        return serviceRents;
    }

    @Override
    public void insert(ServiceRent serviceRent) throws DBException {
        logger.debug("insert services in rents query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_SERVICES_IN_RENTS)) {
            preparedStatement.setInt(1, serviceRent.getIdRental());
            preparedStatement.setInt(2, serviceRent.getIdService());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] services in rents", serviceRent);
        } catch (SQLException e) {
            logger.error("can't add new service in rent", e);
            throw new DBException("can't add new service in rent", e);
        }
    }

    @Override
    public void update(ServiceRent serviceRent) throws DBException {
        logger.debug("Update service in rent query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SERVICES_IN_RENTS_BY_ID)) {
            preparedStatement.setInt(1, serviceRent.getIdRental());
            preparedStatement.setInt(2, serviceRent.getIdService());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] service in rent", serviceRent);
        } catch (SQLException e) {
            logger.error("can't update service in rent", e);
            throw new DBException("can't update service in rent", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete service in rent query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SERVICES_IN_RENTS_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete service in rent by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete service in rent by id", e);
            throw new DBException("can't delete service in rent by id", e);
        }
    }

    @Override
    public ServiceRent findById(int id) throws DBException {
        logger.debug("Find service in rent by id query");
        ServiceRent serviceRent = new ServiceRent();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SERVICE_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                serviceRent.setIdService(resultSet.getInt("id_service"));
                serviceRent.setIdRental(resultSet.getInt("id_rent"));
                serviceRent.setIdRentService(resultSet.getInt("id_rent_service"));
            }
            logger.debug("Find service rent by id [{}]", serviceRent);
        } catch (SQLException e) {
            logger.error("can't find service rent by id", e);
            throw new DBException("can't find service rent by id", e);
        }
        return serviceRent;
    }
}
