package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.ServiceDAO;
import carrental.db.entity.AdditionalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceDAOImpl implements ServiceDAO {
    private static final Logger logger = LogManager.getLogger(ServiceDAOImpl.class);
    public static final String SELECT_ALL_SERVICES = "SELECT * FROM additional_service";
    public static final String SELECT_SERVICE_BY_ID = "SELECT * FROM additional_service WHERE id_service = ?";
    public static final String SELECT_SERVICE_BY_NAME = "SELECT * FROM additional_service WHERE name = ?";
    public static final String INSERT_NEW_SERVICE = "INSERT INTO additional_service(name, price, description, status) VALUES(?,?,?,?)";
    public static final String UPDATE_SERVICE_BY_ID = "UPDATE additional_service SET name = ?, price = ?, description = ?,status = ? WHERE id_service = ?";
    public static final String DELETE_SERVICE_BY_ID = "DELETE FROM additional_service WHERE (id_service = ?)";
    public static final String UPDATE_SERVICE_STATUS_BY_NAME = "UPDATE additional_service SET status = ?  WHERE (name = ?)";

    private final Connection connection;

    public ServiceDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<AdditionalService> findAll() throws DBException {
        logger.debug("a find all service query");
        List<AdditionalService> additionalServices = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_SERVICES)) {
            while (resultSet.next()) {
                AdditionalService additionalService = new AdditionalService();
                additionalService.setIdService(resultSet.getInt("id_service"));
                additionalService.setDescription(resultSet.getString("description"));
                additionalService.setName(resultSet.getString("name"));
                additionalService.setPrice(resultSet.getDouble("price"));
                additionalService.setStatus(resultSet.getString("status"));
                additionalServices.add(additionalService);
            }
        } catch (SQLException e) {
            logger.error("can't find all services", e);
            throw new DBException("can't find all services", e);
        }
        logger.debug("Found [{}] services ", additionalServices.size());
        return additionalServices;
    }

    @Override
    public void insert(AdditionalService additionalService) throws DBException {
        logger.debug("insert service query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_SERVICE)) {
            preparedStatement.setString(1, additionalService.getName());
            preparedStatement.setDouble(2, additionalService.getPrice());
            preparedStatement.setString(3, additionalService.getDescription());
            preparedStatement.setString(4, additionalService.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] service", additionalService);
        } catch (SQLException e) {
            logger.error("can't add new service", e);
            throw new DBException("can't add new service", e);
        }
    }

    @Override
    public void update(AdditionalService additionalService) throws DBException {
        logger.debug("Update service query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SERVICE_BY_ID)) {
            preparedStatement.setString(1, additionalService.getName());
            preparedStatement.setDouble(2, additionalService.getPrice());
            preparedStatement.setString(3, additionalService.getDescription());
            preparedStatement.setString(4, additionalService.getStatus());
            preparedStatement.setInt(5, additionalService.getIdService());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] service", additionalService);
        } catch (SQLException e) {
            logger.error("can't update service", e);
            throw new DBException("can't update service", e);
        }
    }

    @Override
    public void updateServiceStatus(String name, String status) throws DBException {
        logger.debug("Update service status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SERVICE_STATUS_BY_NAME)) {
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, name);
            preparedStatement.executeUpdate();
            logger.debug("Update service status [{}] by name [{}]", status, name);
        } catch (SQLException e) {
            logger.error("can't update service status", e);
            throw new DBException("can't update service status", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete service query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SERVICE_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete service by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete service by id", e);
            throw new DBException("can't delete service by id", e);
        }
    }

    @Override
    public AdditionalService findById(int id) throws DBException {
        logger.debug("Find service by id query");
        AdditionalService additionalService = new AdditionalService();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SERVICE_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                additionalService.setIdService(resultSet.getInt("id_service"));
                additionalService.setDescription(resultSet.getString("description"));
                additionalService.setName(resultSet.getString("name"));
                additionalService.setPrice(resultSet.getDouble("price"));
                additionalService.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find service by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't find service by id", e);
            throw new DBException("can't find service by id", e);
        }
        return additionalService;
    }

    @Override
    public AdditionalService findServiceByName(String name) throws DBException {
        logger.debug("Find service by name query");
        AdditionalService additionalService = new AdditionalService();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SERVICE_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                additionalService.setIdService(resultSet.getInt("id_service"));
                additionalService.setDescription(resultSet.getString("description"));
                additionalService.setName(resultSet.getString("name"));
                additionalService.setPrice(resultSet.getDouble("price"));
                additionalService.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find service by name [{}] ", additionalService);
        } catch (SQLException e) {
            logger.error("can't find service by name", e);
            throw new DBException("can't find service by name", e);
        }
        return additionalService;
    }
}
