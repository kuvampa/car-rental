package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.ManagerDAO;
import carrental.db.entity.Manager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ManagerDAOImpl implements ManagerDAO {
    private static final Logger logger = LogManager.getLogger(ManagerDAOImpl.class);
    private final Connection connection;
    public static final String INSERT_NEW_MANAGER = "INSERT INTO manager(id_manager, full_name, telephone_number, login, password, email) VALUES(?,?,?,?,?,?)";
    public static final String UPDATE_MANAGER_BY_ID = "UPDATE manager SET full_name = ?, telephone_number = ?, email = ?, login= ?, password = ?  WHERE id_manager = ?";
    public static final String SELECT_ALL_MANAGERS = "SELECT * FROM manager";
    public static final String SELECT_MANAGER_BY_ID = "SELECT * FROM manager WHERE id_manager = ?";
    public static final String SELECT_MANAGER_BY_LOGIN = "SELECT * FROM manager WHERE login = ?";
    public static final String DELETE_MANAGER_BY_LOGIN = "DELETE FROM manager WHERE (login = ?)";
    public static final String DELETE_MANAGER_BY_ID = "DELETE FROM manager WHERE (id_manager = ?)";
    public static final String UPDATE_MANAGER_DOCS_BY_ID = "UPDATE manager SET documents = ?  WHERE (login = ?)";

    public ManagerDAOImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public List<Manager> findAll() throws DBException {
        logger.debug("a find all managers query");
        List<Manager> managers = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_MANAGERS)) {
            while (resultSet.next()) {
                Manager manager = new Manager();
                manager.setIdManager(resultSet.getInt("id_manager"));
                manager.setEmail(resultSet.getString("email"));
                manager.setPassword(resultSet.getString("password"));
                manager.setLogin(resultSet.getString("login"));
                manager.setFullName(resultSet.getString("full_name"));
                manager.setTelephoneNumber(resultSet.getString("telephone_number"));
                manager.setDocuments(resultSet.getString("documents"));
                managers.add(manager);
            }
        } catch (SQLException e) {
            logger.error("can't find all managers", e);
            throw new DBException("can't find all managers", e);
        }
        logger.debug("Found [{}] managers ", managers.size());
        return managers;
    }

    @Override
    public void insert(Manager manager) throws DBException {
        logger.debug("insert manager query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_MANAGER)) {
            preparedStatement.setInt(1, manager.getIdManager());
            preparedStatement.setString(2, manager.getFullName());
            preparedStatement.setString(3, manager.getTelephoneNumber());
            preparedStatement.setString(4, manager.getLogin());
            preparedStatement.setString(5, manager.getPassword());
            preparedStatement.setString(6, manager.getEmail());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] manager", manager);
        } catch (SQLException e) {
            logger.error("can't add new manager", e);
            throw new DBException("can't add new manager", e);
        }
    }

    @Override
    public void update(Manager manager) throws DBException {
        logger.debug("update manager query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MANAGER_BY_ID)) {
            preparedStatement.setString(1, manager.getFullName());
            preparedStatement.setString(2, manager.getTelephoneNumber());
            preparedStatement.setString(3, manager.getEmail());
            preparedStatement.setString(4, manager.getLogin());
            preparedStatement.setString(5, manager.getPassword());
            preparedStatement.setInt(6, manager.getIdManager());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] manager", manager);
        } catch (SQLException e) {
            logger.error("can't update manager", e);
            throw new DBException("can't update manager", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete manager by id query");;
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_MANAGER_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete manager by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete manager by id", e);
            throw new DBException("can't delete manager by id", e);
        }
    }

    @Override
    public Manager findById(int id) throws DBException {
        logger.debug("Find manager by id query");
        Manager manager = new Manager();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_MANAGER_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                manager.setIdManager(resultSet.getInt("id_manager"));
                manager.setEmail(resultSet.getString("email"));
                manager.setPassword(resultSet.getString("password"));
                manager.setLogin(resultSet.getString("login"));
                manager.setFullName(resultSet.getString("full_name"));
                manager.setTelephoneNumber(resultSet.getString("telephone_number"));
                manager.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find manager by id [{}]", manager);
        } catch (SQLException e) {
            logger.error("can't find manager by id", e);
            throw new DBException("can't find manager by id", e);
        }
        return manager;
    }

    @Override
    public Manager findManager(String login) throws DBException {
        logger.debug("Find manager by login query");
        Manager manager = new Manager();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_MANAGER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                manager.setIdManager(resultSet.getInt("id_manager"));
                manager.setEmail(resultSet.getString("email"));
                manager.setPassword(resultSet.getString("password"));
                manager.setLogin(resultSet.getString("login"));
                manager.setFullName(resultSet.getString("full_name"));
                manager.setTelephoneNumber(resultSet.getString("telephone_number"));
                manager.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find manager by login [{}]", manager);
        } catch (SQLException e) {
            logger.error("can't find manager by login", e);
            throw new DBException("can't find manager by login", e);
        }
        return manager;
    }

    @Override
    public void deleteByName(String login) throws DBException {
        logger.debug("delete manager by name query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_MANAGER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            preparedStatement.executeUpdate();
            logger.debug("Delete manager by login [{}]", login);
        } catch (SQLException e) {
            logger.error("can't delete manager by login", e);
            throw new DBException("can't delete manager by login", e);
        }
    }

    @Override
    public void updateManagerDocs(String login, String docs) throws DBException {
        logger.debug("update manager documents query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MANAGER_DOCS_BY_ID)) {
            preparedStatement.setString(1, docs);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();
            logger.debug("Update manager documents [{}] by [{}] login", docs, login);
        } catch (SQLException e) {
            logger.error("can't update manager docs", e);
            throw new DBException("can't update manager docs", e);
        }
    }
}
