package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.TermsDAO;
import carrental.db.entity.TermsOfUse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TermsDAOImpl implements TermsDAO {
    private static final Logger logger = LogManager.getLogger(TermsDAOImpl.class);
    private final Connection connection;
    public static final String SELECT_ALL_TERMS = "SELECT * FROM terms_of_use_package";
    public static final String SELECT_TERM_BY_ID = "SELECT * FROM terms_of_use_package WHERE id_terms_package = ?";
    public static final String SELECT_TERM_BY_NAME = "SELECT * FROM terms_of_use_package WHERE name = ?";
    public static final String INSERT_NEW_TERM = "INSERT INTO terms_of_use_package(name, fuel_conditions, mileage_limit, status) VALUES(?,?,?,?)";
    public static final String UPDATE_TERM_BY_ID = "UPDATE terms_of_use_package SET name = ?, fuel_conditions = ?, mileage_limit = ?, status = ? WHERE id_terms_package = ?";
    public static final String DELETE_TERM_BY_ID = "DELETE FROM terms_of_use_package WHERE (id_terms_package = ?)";
    public static final String UPDATE_TERM_STATUS_BY_NAME = "UPDATE terms_of_use_package SET status = ?  WHERE (name = ?)";

    public TermsDAOImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public List<TermsOfUse> findAll() throws DBException {
        logger.debug("a find all terms of use query");
        List<TermsOfUse> termsOfUses = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_TERMS)) {
            while (resultSet.next()) {
                TermsOfUse termsOfUse = new TermsOfUse();
                termsOfUse.setIdTermsPackage(resultSet.getInt("id_terms_package"));
                termsOfUse.setFuelConditions(resultSet.getString("fuel_conditions"));
                termsOfUse.setName(resultSet.getString("name"));
                termsOfUse.setMileageLimit(resultSet.getString("mileage_limit"));
                termsOfUse.setStatus(resultSet.getString("status"));
                termsOfUses.add(termsOfUse);
            }
        } catch (SQLException e) {
            logger.error("can't find all terms of use", e);
            throw new DBException("can't find all terms of use", e);
        }
        logger.debug("Found [{}] terms", termsOfUses.size());
        return termsOfUses;
    }

    @Override
    public void insert(TermsOfUse termsOfUse) throws DBException {
        logger.debug("insert term query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_TERM)) {
            preparedStatement.setString(1, termsOfUse.getName());
            preparedStatement.setString(2, termsOfUse.getFuelConditions());
            preparedStatement.setString(3, termsOfUse.getMileageLimit());
            preparedStatement.setString(4, termsOfUse.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] terms", termsOfUse);
        } catch (SQLException e) {
            logger.error("can't add new term of use", e);
            throw new DBException("can't add new term of use", e);
        }
    }

    @Override
    public void update(TermsOfUse termsOfUse) throws DBException {
        logger.debug("Update term query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TERM_BY_ID)) {
            preparedStatement.setString(1, termsOfUse.getName());
            preparedStatement.setString(2, termsOfUse.getFuelConditions());
            preparedStatement.setString(3, termsOfUse.getMileageLimit());
            preparedStatement.setString(4, termsOfUse.getStatus());
            preparedStatement.setInt(5, termsOfUse.getIdTermsPackage());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] terms", termsOfUse);
        } catch (SQLException e) {
            logger.error("can't update term of use", e);
            throw new DBException("can't update term of use", e);
        }
    }

    @Override
    public void updateTermStatus(String name, String status) throws DBException {
        logger.debug("Update term status by name query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TERM_STATUS_BY_NAME)) {
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, name);
            preparedStatement.executeUpdate();
            logger.debug("Update terms status [{}] car by plate [{}]", status, name);
        } catch (SQLException e) {
            logger.error("can't update term status", e);
            throw new DBException("can't update term status", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete term query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TERM_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete term by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete term of use by id", e);
            throw new DBException("can't delete term of use by id", e);
        }
    }

    @Override
    public TermsOfUse findById(int idTermsPackage) throws DBException {
        logger.debug("Find term by id query");
        TermsOfUse termsOfUse = new TermsOfUse();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TERM_BY_ID)) {
            preparedStatement.setInt(1, idTermsPackage);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                termsOfUse.setIdTermsPackage(resultSet.getInt("id_terms_package"));
                termsOfUse.setFuelConditions(resultSet.getString("fuel_conditions"));
                termsOfUse.setName(resultSet.getString("name"));
                termsOfUse.setMileageLimit(resultSet.getString("mileage_limit"));
                termsOfUse.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find term by id [{}]", termsOfUse);
        } catch (SQLException e) {
            logger.error("can't find term by id", e);
            throw new DBException("can't find term by id", e);
        }
        return termsOfUse;
    }

    @Override
    public TermsOfUse findByName(String name) throws DBException {
        logger.debug("Find term by name query");
        TermsOfUse termsOfUse = new TermsOfUse();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TERM_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                termsOfUse.setIdTermsPackage(resultSet.getInt("id_terms_package"));
                termsOfUse.setFuelConditions(resultSet.getString("fuel_conditions"));
                termsOfUse.setName(resultSet.getString("name"));
                termsOfUse.setMileageLimit(resultSet.getString("mileage_limit"));
                termsOfUse.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find term by name [{}]", termsOfUse);
        } catch (SQLException e) {
            logger.error("can't find term by name", e);
            throw new DBException("can't find term by name", e);
        }
        return termsOfUse;
    }
}
