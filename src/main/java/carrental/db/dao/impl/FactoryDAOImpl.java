package carrental.db.dao.impl;

import carrental.db.dao.*;

public class FactoryDAOImpl extends Factory {
    final ConnectionPoolHandler connectionPoolHandler = ConnectionPoolHandler.getInstance();

    @Override
    public AdminDAO getAdminDAO() {
        return new AdminDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public UserDAO getUserDAO() {
        return new UserDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public CarDAO getCarDAO() {
        return new CarDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public InsuranceDAO getInsuranseDAO() {
        return new InsuranceDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public OfficeDAO getOfficeDAO() {
        return new OfficeDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public RentDAO getRentDAO() {
        return new RentDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public ServiceDAO getServiceDAO() {
        return new ServiceDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public TermsDAO getTermsDAO() {
        return new TermsDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public ServiceRentDAO getServiceRentDAO() {
        return new ServiceRentDAOImpl(connectionPoolHandler.getConnection());
    }

    @Override
    public ManagerDAO getManagerDAO() { return new ManagerDAOImpl(connectionPoolHandler.getConnection()); }
    @Override
    public AccidentDAO getAccidentDAO() { return new AccidentDAOImpl(connectionPoolHandler.getConnection()); }
    @Override
    public VerificationDAO getVerificationDAO() { return new VerificationDAOImpl(connectionPoolHandler.getConnection()); }
    @Override
    public UserSupportDAO getUserSupportDAO() { return new UserSupportDAOImpl(connectionPoolHandler.getConnection()); }

}
