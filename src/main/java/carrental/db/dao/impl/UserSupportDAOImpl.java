package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.UserSupportDAO;
import carrental.db.entity.UserSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserSupportDAOImpl implements UserSupportDAO {

    private static final Logger logger = LogManager.getLogger(UserSupportDAOImpl.class);
    private final Connection connection;
    public static final String SELECT_ALL_USER_SUPPORT = "SELECT * FROM user_support";
    public static final String SELECT_USER_SUPPORT_BY_ID = "SELECT * FROM user_support WHERE id_user_support = ?";
    public static final String INSERT_NEW_USER_SUPPORT = "INSERT INTO user_support(id_user, message, status) VALUES(?,?,?)";
    public static final String UPDATE_USER_SUPPORT_BY_ID = "UPDATE user_support SET id_user = ?, id_admin = ?, message = ?, status = ? WHERE id_user_support = ?";
    public static final String UPDATE_USER_SUPPORT_STATUS_BY_ID = "UPDATE user_support SET status = ?  WHERE (id_user_support = ?)";
    public static final String UPDATE_USER_SUPPORT_ADMIN_BY_ID = "UPDATE user_support SET id_admin = ?  WHERE (id_user_support = ?)";
    public static final String DELETE_USER_SUPPORT_BY_ID = "DELETE FROM user_support WHERE (id_user_support = ?)";

    public UserSupportDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<UserSupport> findAll() throws DBException {
        logger.debug("a find all user support of use query");
        List<UserSupport> userSupports = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_USER_SUPPORT)) {
            while (resultSet.next()) {
                UserSupport userSupport = new UserSupport();
                userSupport.setIdUser(resultSet.getInt("id_user"));
                userSupport.setIdUserSupport(resultSet.getInt("id_user_support"));
                userSupport.setIdAdmin(resultSet.getInt("id_admin"));
                userSupport.setMessage(resultSet.getString("message"));
                userSupport.setStatus(resultSet.getString("status"));
                userSupports.add(userSupport);
            }
        } catch (SQLException e) {
            logger.error("can't find all user supports", e);
            throw new DBException("can't find all user supports", e);
        }
        logger.debug("Found [{}] user supports", userSupports.size());
        return userSupports;
    }

    @Override
    public void insert(UserSupport userSupport) throws DBException {
        logger.debug("insert user support query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_USER_SUPPORT)) {
            preparedStatement.setInt(1, userSupport.getIdUser());
            preparedStatement.setString(2, userSupport.getMessage());
            preparedStatement.setString(3, userSupport.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] user support ", userSupport);
        } catch (SQLException e) {
            logger.error("can't add new user support", e);
            throw new DBException("can't add new user support", e);
        }
    }

    @Override
    public void update(UserSupport userSupport) throws DBException {
        logger.debug("update user support query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_SUPPORT_BY_ID)) {
            preparedStatement.setInt(1, userSupport.getIdUser());
            preparedStatement.setInt(2, userSupport.getIdAdmin());
            preparedStatement.setString(3, userSupport.getMessage());
            preparedStatement.setString(4, userSupport.getStatus());
            preparedStatement.setInt(5, userSupport.getIdUserSupport());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] user support ", userSupport);
        } catch (SQLException e) {
            logger.error("can't update user support", e);
            throw new DBException("can't update user support", e);
        }
    }

    @Override
    public void updateUserSupportStatus(int id, String status) throws DBException {
        logger.debug("update user support status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_SUPPORT_STATUS_BY_ID)) {
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] status ", status);
        } catch (SQLException e) {
            logger.error("can't update user support status", e);
            throw new DBException("can't update user support status", e);
        }
    }

    @Override
    public void updateUserSupportAdmin(int id, int admin) throws DBException {
        logger.debug("update user support admin query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_SUPPORT_ADMIN_BY_ID)) {
            preparedStatement.setInt(1, admin);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] admin ", admin);
        } catch (SQLException e) {
            logger.error("can't update user support admin", e);
            throw new DBException("can't update user support admin", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete user support by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_SUPPORT_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete user support by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete user support by id", e);
            throw new DBException("can't delete user support by id", e);
        }
    }

    @Override
    public UserSupport findById(int id) throws DBException {
        logger.debug("Find user support by id query");
        UserSupport userSupport = new UserSupport();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_SUPPORT_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userSupport.setIdUser(resultSet.getInt("id_user"));
                userSupport.setIdUserSupport(resultSet.getInt("id_user_support"));
                userSupport.setIdAdmin(resultSet.getInt("id_admin"));
                userSupport.setMessage(resultSet.getString("message"));
                userSupport.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find user support by id [{}]", userSupport);
        } catch (SQLException e) {
            logger.error("can't find user support by id", e);
            throw new DBException("can't find user support by id", e);
        }
        logger.debug("Find user support by id [{}]", userSupport);
        return userSupport;
    }
}
