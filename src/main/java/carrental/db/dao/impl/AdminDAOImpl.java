package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.AdminDAO;
import carrental.db.entity.Admin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdminDAOImpl implements AdminDAO {
    private static final Logger logger = LogManager.getLogger(AdminDAOImpl.class);
    private final Connection connection;
    public static final String INSERT_NEW_ADMIN = "INSERT INTO admin(id_admin, login, password, full_name, telephone_number, email) VALUES(?,?,?,?,?,?)";
    public static final String UPDATE_ADMIN_BY_ID = "UPDATE admin SET login= ?, password = ?, full_name = ?, telephone_number = ?, email = ?  WHERE id_admin = ?";
    public static final String SELECT_ALL_ADMINS = "SELECT * FROM admin";
    public static final String SELECT_ADMIN_BY_ID = "SELECT * FROM admin WHERE id_admin = ?";
    public static final String SELECT_ADMIN_BY_LOGIN = "SELECT * FROM admin WHERE login = ?";
    public static final String DELETE_ADMIN_BY_LOGIN = "DELETE FROM admin WHERE (login = ?)";
    public static final String DELETE_ADMIN_BY_ID = "DELETE FROM admin WHERE (id_admin = ?)";
    public static final String UPDATE_ADMIN_DOCS_BY_ID = "UPDATE admin SET documents = ?  WHERE (login = ?)";

    public AdminDAOImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public List<Admin> findAll() throws DBException {
        logger.debug("a find all admins query");
        List<Admin> admins = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_ADMINS)) {
            while (resultSet.next()) {
                Admin admin = new Admin();
                admin.setIdAdmin(resultSet.getInt("id_admin"));
                admin.setEmail(resultSet.getString("email"));
                admin.setPassword(resultSet.getString("password"));
                admin.setLogin(resultSet.getString("login"));
                admin.setFullName(resultSet.getString("full_name"));
                admin.setTelephoneNumber(resultSet.getString("telephone_number"));
                admin.setDocuments(resultSet.getString("documents"));
                admins.add(admin);
            }
        } catch (SQLException e) {
            logger.error("can't find all admins", e);
            throw new DBException("can't find all admins", e);
        }
        logger.debug("Found [{}] admins ", admins.size());
        return admins;
    }

    @Override
    public void insert(Admin admin) throws DBException {
        logger.debug("insert admin query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_ADMIN)) {
            preparedStatement.setInt(1, admin.getIdAdmin());
            preparedStatement.setString(2, admin.getLogin());
            preparedStatement.setString(3, admin.getPassword());
            preparedStatement.setString(4, admin.getFullName());
            preparedStatement.setString(5, admin.getTelephoneNumber());
            preparedStatement.setString(6, admin.getEmail());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] admin", admin);
        } catch (SQLException e) {
            logger.error("can't add new admin", e);
            throw new DBException("can't add new admin", e);
        }
    }

    @Override
    public void update(Admin admin) throws DBException {
        logger.debug("update admin query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ADMIN_BY_ID)) {
            preparedStatement.setString(1, admin.getLogin());
            preparedStatement.setString(2, admin.getPassword());
            preparedStatement.setString(3, admin.getFullName());
            preparedStatement.setString(4, admin.getTelephoneNumber());
            preparedStatement.setString(5, admin.getEmail());
            preparedStatement.setInt(6, admin.getIdAdmin());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] admin", admin);
        } catch (SQLException e) {
            logger.error("can't update admin", e);
            throw new DBException("can't update admin", e);
        }
    }

    @Override
    public Admin findById(int id) throws DBException {
        logger.debug("Find admin by id query");
        Admin admin = new Admin();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ADMIN_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                admin.setIdAdmin(resultSet.getInt("id_admin"));
                admin.setEmail(resultSet.getString("email"));
                admin.setPassword(resultSet.getString("password"));
                admin.setLogin(resultSet.getString("login"));
                admin.setFullName(resultSet.getString("full_name"));
                admin.setTelephoneNumber(resultSet.getString("telephone_number"));
                admin.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find admin [{}]", admin);
        } catch (SQLException e) {
            logger.error("can't find admin by id", e);
            throw new DBException("can't find admin by id", e);
        }
        return admin;
    }

    @Override
    public Admin findAdmin(String login) throws DBException {
        logger.debug("Find admin by login query");
        Admin admin = new Admin();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ADMIN_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                admin.setIdAdmin(resultSet.getInt("id_admin"));
                admin.setEmail(resultSet.getString("email"));
                admin.setPassword(resultSet.getString("password"));
                admin.setLogin(resultSet.getString("login"));
                admin.setFullName(resultSet.getString("full_name"));
                admin.setTelephoneNumber(resultSet.getString("telephone_number"));
                admin.setDocuments(resultSet.getString("documents"));
            }
            logger.debug("Find admin [{}]", admin);
        } catch (SQLException e) {
            logger.error("can't find admin by login", e);
            throw new DBException("can't find admin by login", e);
        }
        return admin;
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete admin by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ADMIN_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete admin by [{}] id", id);
        } catch (SQLException e) {
            logger.error("can't delete admin by id", e);
            throw new DBException("can't delete admin by id", e);
        }
    }

    @Override
    public void deleteByName(String login) throws DBException {
        logger.debug("delete admin by name query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ADMIN_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            preparedStatement.executeUpdate();
            logger.debug("Delete admin by [{}] login", login);
        } catch (SQLException e) {
            logger.error("can't delete admin by login", e);
            throw new DBException("can't delete admin by login", e);
        }
    }

    @Override
    public void updateAdminDocs(String login, String docs) throws DBException {
        logger.debug("update admin documents query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ADMIN_DOCS_BY_ID)) {
            preparedStatement.setString(1, docs);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();
            logger.debug("Update admin documents [{}] by [{}] login", docs, login);
        } catch (SQLException e) {
            logger.error("can't update admin docs", e);
            throw new DBException("can't update admin docs", e);
        }
    }
}
