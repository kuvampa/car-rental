package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.AccidentDAO;
import carrental.db.entity.Accident;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccidentDAOImpl implements AccidentDAO {
    private static final Logger logger = LogManager.getLogger(AccidentDAOImpl.class);
    private final Connection connection;
    public static final String SELECT_ALL_ACCIDENTS = "SELECT * FROM accident";
    public static final String SELECT_ACCIDENT_BY_ID = "SELECT * FROM accident WHERE id_accident = ?";
    public static final String INSERT_NEW_ACCIDENT = "INSERT INTO accident(id_rent, id_manager, price, comment, status) VALUES(?,?,?,?,?)";
    public static final String UPDATE_ACCIDENT_BY_ID = "UPDATE accident SET id_rent = ?, id_manager = ?, comment = ?, price = ?, status = ? WHERE id_accident = ?";
    public static final String DELETE_ACCIDENT_BY_ID = "DELETE FROM accident WHERE (id_accident = ?)";
    public static final String UPDATE_ACCIDENT_STATUS_BY_ID = "UPDATE accident SET status = ?  WHERE (id_accident = ?)";
    public static final String UPDATE_ACCIDENT_MANAGER_BY_ID = "UPDATE accident SET id_manager = ?  WHERE (id_accident = ?)";

    public AccidentDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Accident> findAll() throws DBException {
        logger.debug("a find all accident query");
        List<Accident> accidents = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_ACCIDENTS)) {
            while (resultSet.next()) {
                Accident accident = new Accident();
                accident.setIdRental(resultSet.getInt("id_rent"));
                accident.setPrice(resultSet.getDouble("price"));
                accident.setIdAccident(resultSet.getInt("id_accident"));
                accident.setIdManager(resultSet.getInt("id_manager"));
                accident.setComment(resultSet.getString("comment"));
                accident.setStatus(resultSet.getString("status"));
                accidents.add(accident);
            }
        } catch (SQLException e) {
            logger.error("can't find all accidents", e);
            throw new DBException("can't find all accidents", e);
        }
        logger.debug("Found [{}] accidents ", accidents.size());
        return accidents;
    }

    @Override
    public void insert(Accident accident) throws DBException {
        logger.debug("insert accident query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_ACCIDENT)) {
            preparedStatement.setInt(1, accident.getIdRental());
            preparedStatement.setInt(2, accident.getIdManager());
            preparedStatement.setDouble(3, accident.getPrice());
            preparedStatement.setString(4, accident.getComment());
            preparedStatement.setString(5, accident.getStatus());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] accident ", accident);
        } catch (SQLException e) {
            logger.error("can't add new accident", e);
            throw new DBException("can't add new accident", e);
        }
    }

    @Override
    public void update(Accident accident) throws DBException {
        logger.debug("update accident query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ACCIDENT_BY_ID)) {
            preparedStatement.setInt(1, accident.getIdRental());
            preparedStatement.setInt(2, accident.getIdManager());
            preparedStatement.setString(3, accident.getComment());
            preparedStatement.setDouble(4, accident.getPrice());
            preparedStatement.setString(5, accident.getStatus());
            preparedStatement.setInt(6, accident.getIdAccident());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] accident ", accident);
        } catch (SQLException e) {
            logger.error("can't update accident", e);
            throw new DBException("can't update accident", e);
        }
    }

    @Override
    public void updateAccidentStatus(int id, String status) throws DBException {
        logger.debug("update accident status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ACCIDENT_STATUS_BY_ID)) {
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] status ", status);
        } catch (SQLException e) {
            logger.error("can't update accident status", e);
            throw new DBException("can't update accident status", e);
        }
    }

    @Override
    public void updateAccidentManager(int id, int manager) throws DBException {
        logger.debug("update accident manager query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ACCIDENT_MANAGER_BY_ID)) {
            preparedStatement.setInt(1, manager);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] manager ", manager);
        } catch (SQLException e) {
            logger.error("can't update accident manager", e);
            throw new DBException("can't update accident manager", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete accident by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ACCIDENT_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete accident by [{}] id", id);
        } catch (SQLException e) {
            logger.error("can't delete accident", e);
            throw new DBException("can't delete accident", e);
        }
    }

    @Override
    public Accident findById(int id) throws DBException {
        logger.debug("Find accident by id query");
        Accident accident = new Accident();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACCIDENT_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                accident.setIdRental(resultSet.getInt("id_rent"));
                accident.setPrice(resultSet.getDouble("price"));
                accident.setIdAccident(resultSet.getInt("id_accident"));
                accident.setIdManager(resultSet.getInt("id_manager"));
                accident.setComment(resultSet.getString("comment"));
                accident.setStatus(resultSet.getString("status"));
            }
            logger.debug("Find accident [{}]", accident);
        } catch (SQLException e) {
            logger.error("can't find accident", e);
            throw new DBException("can't find accident", e);
        }
        return accident;
    }
}
