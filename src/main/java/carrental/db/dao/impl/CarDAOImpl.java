package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.CarDAO;
import carrental.db.entity.Car;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CarDAOImpl implements CarDAO {
    private static final Logger logger = LogManager.getLogger(CarDAOImpl.class);
    private static final String DELETE_CAR_BY_ID = "DELETE FROM car WHERE (id_car = ?)";
    private static final String INSERT_NEW_CAR = "INSERT INTO car(id_office, id_insurance, id_terms_package, capacity, rating, color, car_name, plate_number, type, manufacturer, price, status, fuel_type) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_CAR_BY_ID = "UPDATE car SET id_office = ?, id_insurance = ?, id_terms_package = ?, capacity= ?, rating = ?,color = ?,car_name = ?,plate_number = ? ,type = ? ,manufacturer = ? ,price = ? ,status = ?,fuel_type = ?  WHERE id_car = ?";
    private static final String UPDATE_CAR_STATUS_BY_PLATE = "UPDATE car SET status = ?  WHERE (plate_number = ?)";
    private static final String SELECT_ALL_CARS = "SELECT * FROM car";
    private static final String SELECT_CAR_BY_ID = "SELECT * FROM car WHERE id_car = ?";
    private static final String SELECT_CAR_BY_PLATE = "SELECT * FROM car WHERE plate_number = ?";
    private static final String SELECT_UNIQUE_COLOR = "SELECT DISTINCT color FROM car";
    private static final String SELECT_UNIQUE_FUEL_TYPE = "SELECT DISTINCT fuel_type FROM car";
    private static final String SELECT_UNIQUE_CAPACITY = "SELECT DISTINCT capacity FROM car";
    private static final String SELECT_UNIQUE_MANUFACTURER = "SELECT DISTINCT manufacturer FROM car";
    private static final String SELECT_UNIQUE_TYPE = "SELECT DISTINCT type FROM car";
    private static final String SELECT_UNIQUE_RATING = "SELECT DISTINCT rating FROM car";
    private static final String COUNT_CARS_FOR_SEARCH = "SELECT COUNT(id_car) FROM car";
    private final Connection connection;

    public CarDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Car> findAll() throws DBException {
        logger.debug("a find all cars query");
        List<Car> cars = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_CARS)) {
            cars = fillList(cars, resultSet);
        } catch (SQLException e) {
            logger.error("can't find all cars", e);
            throw new DBException("can't find all cars", e);
        }
        logger.debug("Found [{}] cars", cars.size());
        return cars;
    }

    @Override
    public int countSortedCars(String sort, List<String> manufactures, String name, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity) throws DBException {
        logger.debug("a count all chosen cars query");
        List<String> emptyStringList = new ArrayList<>();
        List<Integer> emptyIntegerList = new ArrayList<>();
        int countCars = 0;
        int countPS = 1;
        String searchQuery = requestBuilder(emptyStringList, emptyIntegerList, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, sort, true);
        try (PreparedStatement pStmt = connection.prepareStatement(searchQuery)) {
            if (name != null && !name.isEmpty()) {
                pStmt.setString(countPS, name + "%");
                countPS++;
            }
            if (min != 0 || max != 0) {
                pStmt.setInt(countPS, min);
                countPS++;
                pStmt.setInt(countPS, max);
                countPS++;
            }
            countPS = addPStmt(pStmt, countPS, emptyStringList, capacity);
            countPS = addPStmt(pStmt, countPS, emptyStringList, ratings);
            countPS = addPStmt(pStmt, countPS, colors, emptyIntegerList);
            countPS = addPStmt(pStmt, countPS, types, emptyIntegerList);
            countPS = addPStmt(pStmt, countPS, fuelTypes, emptyIntegerList);
            addPStmt(pStmt, countPS, manufactures, emptyIntegerList);
            ResultSet resultSet = pStmt.executeQuery();
            if (resultSet.next()) {
                countCars = resultSet.getInt("COUNT(id_car)");
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.error("can't count all chosen cars query", e);
            throw new DBException("can't count all chosen cars query", e);
        }
        logger.trace("Count [{}] all chosen cars query",countCars);
        return countCars;
    }

    @Override
    public List<Car> findSortedLimitedCars(int page, String sort, List<String> manufactures, String name, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity) throws DBException {
        logger.debug("a find all chosen sorted and limited cars query");
        List<Car> cars = new ArrayList<>();
        List<String> emptyStringList = new ArrayList<>();
        List<Integer> emptyIntegerList = new ArrayList<>();
        int countPS = 1;
        String searchQuery = requestBuilder(emptyStringList, emptyIntegerList, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, sort, false);
        try (PreparedStatement pStmt = connection.prepareStatement(searchQuery)) {
            if (name != null && !name.isEmpty()) {
                pStmt.setString(countPS, name + "%");
                countPS++;
            }
            if (min != 0 || max != 0) {
                pStmt.setInt(countPS, min);
                countPS++;
                pStmt.setInt(countPS, max);
                countPS++;
            }
            countPS = addPStmt(pStmt, countPS, emptyStringList, capacity);
            countPS = addPStmt(pStmt, countPS, emptyStringList, ratings);
            countPS = addPStmt(pStmt, countPS, colors, emptyIntegerList);
            countPS = addPStmt(pStmt, countPS, types, emptyIntegerList);
            countPS = addPStmt(pStmt, countPS, fuelTypes, emptyIntegerList);
            countPS = addPStmt(pStmt, countPS, manufactures, emptyIntegerList);
            int pagination = 0;
            if (page > 1) {
                pagination = (page - 1) * 9;
            }
            pStmt.setInt(countPS, pagination);
            countPS++;
            pStmt.setInt(countPS, 9);
            ResultSet resultSet = pStmt.executeQuery();
            cars = fillList(cars, resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("can't find all chosen sorted and limited cars query", e);
            throw new DBException("can't find all chosen sorted and limited cars query", e);
        }
        logger.trace("Found [{}] all chosen sorted and limited cars query", cars.size());
        return cars;
    }

    @Override
    public List<String> findUniqueManufacture() throws DBException {
        logger.debug("a find unique manufactures query");
        List<String> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_MANUFACTURER); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getString("manufacturer"));
            }
        } catch (SQLException e) {
            logger.error("can't find manufacture", e);
            throw new DBException("can't find manufacture", e);
        }
        logger.debug("Found [{}] manufactures ", columns.size());
        return columns;
    }

    @Override
    public List<String> findUniqueColor() throws DBException {
        logger.debug("a find unique color query");
        List<String> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_COLOR); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getString("color"));
            }
        } catch (SQLException e) {
            logger.error("can't find colors", e);
            throw new DBException("can't find colors", e);
        }
        logger.debug("Found [{}] color ", columns.size());
        return columns;
    }

    @Override
    public List<Integer> findUniqueRating() throws DBException {
        logger.debug("a find unique rating query");
        List<Integer> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_RATING); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getInt("rating"));
            }
        } catch (SQLException e) {
            logger.error("can't find rating", e);
            throw new DBException("can't find rating", e);
        }

        logger.debug("Found [{}] rating ", columns.size());
        return columns;
    }

    @Override
    public List<String> findUniqueType() throws DBException {
        logger.debug("a find unique type query");
        List<String> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_TYPE); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getString("type"));
            }
        } catch (SQLException e) {
            logger.error("can't find type", e);
            throw new DBException("can't find type", e);
        }
        logger.debug("Found [{}] type ", columns.size());
        return columns;
    }

    @Override
    public List<String> findUniqueFuelType() throws DBException {
        logger.debug("a find unique fuel type query");
        List<String> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_FUEL_TYPE); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getString("fuel_type"));
            }
        } catch (SQLException e) {
            logger.error("can't find fuel type", e);
            throw new DBException("can't find fuel type", e);
        }
        logger.debug("Found [{}] fuel type ", columns.size());
        return columns;
    }

    @Override
    public List<Integer> findUniqueCapacity() throws DBException {
        logger.debug("a find unique capacity query");
        List<Integer> columns = new ArrayList<>();
        try (PreparedStatement pStmt = connection.prepareStatement(SELECT_UNIQUE_CAPACITY); ResultSet resultSet = pStmt.executeQuery()) {
            while (resultSet.next()) {
                columns.add(resultSet.getInt("capacity"));
            }
        } catch (SQLException e) {
            logger.error("can't find capacity", e);
            throw new DBException("can't find capacity", e);
        }
        logger.debug("Found [{}] capacity ", columns.size());
        return columns;
    }

    @Override
    public void insert(Car car) throws DBException {
        logger.debug("insert admin query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_CAR)) {
            preparedStatement.setInt(1, car.getIdOffice());
            preparedStatement.setInt(2, car.getIdInsurance());
            preparedStatement.setInt(3, car.getIdTermsPackage());
            preparedStatement.setInt(4, car.getCapacity());
            preparedStatement.setInt(5, car.getRating());
            preparedStatement.setString(6, car.getColor());
            preparedStatement.setString(7, car.getCarName());
            preparedStatement.setString(8, car.getPlateNumber());
            preparedStatement.setString(9, car.getType());
            preparedStatement.setString(10, car.getManufacturer());
            preparedStatement.setDouble(11, car.getPrice());
            preparedStatement.setString(12, car.getStatus());
            preparedStatement.setString(13, car.getFuelType());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] car", car);
        } catch (SQLException e) {
            logger.error("can't add new car", e);
            throw new DBException("can't add new car", e);
        }
    }

    @Override
    public void update(Car car) throws DBException {
        logger.debug("Update car query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CAR_BY_ID)) {
            preparedStatement.setInt(1, car.getIdOffice());
            preparedStatement.setInt(2, car.getIdInsurance());
            preparedStatement.setInt(3, car.getIdTermsPackage());
            preparedStatement.setInt(4, car.getCapacity());
            preparedStatement.setInt(5, car.getRating());
            preparedStatement.setString(6, car.getColor());
            preparedStatement.setString(7, car.getCarName());
            preparedStatement.setString(8, car.getPlateNumber());
            preparedStatement.setString(9, car.getType());
            preparedStatement.setString(10, car.getManufacturer());
            preparedStatement.setDouble(11, car.getPrice());
            preparedStatement.setString(12, car.getStatus());
            preparedStatement.setString(13, car.getFuelType());
            preparedStatement.setInt(14, car.getIdCar());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] car", car);
        } catch (SQLException e) {
            logger.error("can't update car", e);
            throw new DBException("can't update car", e);
        }
    }

    @Override
    public void updateCarStatus(String plate, String status) throws DBException {
        logger.debug("Update car status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CAR_STATUS_BY_PLATE)) {
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, plate);
            preparedStatement.executeUpdate();
            logger.debug("Update car status [{}] car by plate [{}]", status, plate);
        } catch (SQLException e) {
            logger.error("can't update car status", e);
            throw new DBException("can't update car status", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("Delete car query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CAR_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete car by [{}] id", id);
        } catch (SQLException e) {
            logger.error("can't delete car", e);
            throw new DBException("can't delete car", e);
        }
    }

    @Override
    public Car findById(int id) throws DBException {
        logger.debug("Find car by id query");
        Car car = new Car();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                car.setIdCar(resultSet.getInt("id_car"));
                car.setIdOffice(resultSet.getInt("id_office"));
                car.setIdTermsPackage(resultSet.getInt("id_terms_package"));
                car.setIdInsurance(resultSet.getInt("id_insurance"));
                car.setFuelType(resultSet.getString("fuel_type"));
                car.setCarName(resultSet.getString("car_name"));
                car.setType(resultSet.getString("type"));
                car.setManufacturer(resultSet.getString("manufacturer"));
                car.setStatus(resultSet.getString("status"));
                car.setColor(resultSet.getString("color"));
                car.setPrice(resultSet.getDouble("price"));
                car.setRating(resultSet.getInt("rating"));
                car.setCapacity(resultSet.getInt("capacity"));
                car.setPlateNumber(resultSet.getString("plate_number"));
            }
            logger.debug("Find car [{}]", car);
        } catch (SQLException e) {
            logger.error("can't find car", e);
            throw new DBException("can't find car", e);
        }
        return car;
    }

    @Override
    public Car findCarByPlate(String plateNumber) throws DBException {
        logger.debug("Find car by plateNumber query");
        Car car = new Car();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_BY_PLATE)) {
            preparedStatement.setString(1, plateNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                car.setIdCar(resultSet.getInt("id_car"));
                car.setIdOffice(resultSet.getInt("id_office"));
                car.setIdTermsPackage(resultSet.getInt("id_terms_package"));
                car.setIdInsurance(resultSet.getInt("id_insurance"));
                car.setFuelType(resultSet.getString("fuel_type"));
                car.setCarName(resultSet.getString("car_name"));
                car.setType(resultSet.getString("type"));
                car.setManufacturer(resultSet.getString("manufacturer"));
                car.setStatus(resultSet.getString("status"));
                car.setColor(resultSet.getString("color"));
                car.setPrice(resultSet.getDouble("price"));
                car.setRating(resultSet.getInt("rating"));
                car.setCapacity(resultSet.getInt("capacity"));
                car.setPlateNumber(resultSet.getString("plate_number"));
            }
            logger.debug("Find car [{}]", car);
        } catch (SQLException e) {
            logger.error("can't find car", e);
            throw new DBException("can't find car", e);
        }
        return car;
    }

    public List<Car> fillList(List<Car> cars, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            Car car = new Car();
            car.setIdCar(resultSet.getInt("id_car"));
            car.setIdOffice(resultSet.getInt("id_office"));
            car.setIdTermsPackage(resultSet.getInt("id_terms_package"));
            car.setIdInsurance(resultSet.getInt("id_insurance"));
            car.setFuelType(resultSet.getString("fuel_type"));
            car.setCarName(resultSet.getString("car_name"));
            car.setType(resultSet.getString("type"));
            car.setManufacturer(resultSet.getString("manufacturer"));
            car.setStatus(resultSet.getString("status"));
            car.setColor(resultSet.getString("color"));
            car.setPrice(resultSet.getDouble("price"));
            car.setRating(resultSet.getInt("rating"));
            car.setCapacity(resultSet.getInt("capacity"));
            car.setPlateNumber(resultSet.getString("plate_number"));
            cars.add(car);
        }
        return cars;
    }

    private boolean checkLists(int ourList, String name, List<String> manufactures, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity) {
        if (ourList < 1 && (name != null)) {
            return true;
        }
        if (ourList < 2 && (min != 0 || max != 0)) {
            return true;
        }
        if (ourList < 3 && !capacity.isEmpty()) {
            return true;
        }
        if (ourList < 4 && !ratings.isEmpty()) {
            return true;
        }
        if (ourList < 5 && !colors.isEmpty()) {
            return true;
        }
        if (ourList < 6 && !types.isEmpty()) {
            return true;
        }
        if (ourList < 7 && !fuelTypes.isEmpty()) {
            return true;
        }
        return ourList < 8 && !manufactures.isEmpty();
    }

    private String requestBuilder(List<String> emptyStringList, List<Integer> emptyIntegerList, String name, List<String> manufactures, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity, String sort, boolean limitOrSearch) {
        StringBuilder searchQuery;
        if (!limitOrSearch) {
            searchQuery = new StringBuilder(SELECT_ALL_CARS);
        }else {
            searchQuery = new StringBuilder(COUNT_CARS_FOR_SEARCH);
        }
        if (checkLists(0, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity)) {
            searchQuery.append(" WHERE ");
        }
        if (name != null && !name.isEmpty()) {
            searchQuery.append("car_name LIKE ? ");
            if (checkLists(1, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity)) {
                searchQuery.append("AND ");
            }
            logger.info(searchQuery);
        }
        if (min != 0 || max != 0) {
            searchQuery.append("`price` >= ? AND `price` <= ? ");
            if (checkLists(2, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity)) {
                searchQuery.append("AND (");
            }
        }
        addSpecificField(emptyStringList, capacity, 3, "capacity", name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, searchQuery);
        addSpecificField(emptyStringList, ratings, 4, "rating", name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, searchQuery);
        addSpecificField(colors, emptyIntegerList, 5, "color", name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, searchQuery);
        addSpecificField(types, emptyIntegerList, 6, "type", name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, searchQuery);
        addSpecificField(fuelTypes, emptyIntegerList, 7, "fuel_type", name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity, searchQuery);
        for (String manufacture : manufactures) {
            searchQuery.append("manufacturer = ?");
            if (Objects.equals(manufacture, manufactures.get(manufactures.size() - 1))) {
                searchQuery.append(") ");
            } else {
                searchQuery.append(" OR ");
            }
        }
        if (!limitOrSearch) {
            if (!sort.isEmpty() && !sort.equals("5")) {
                searchQuery.append("ORDER BY ");
            }
            switch (sort) {
                case "1":
                    searchQuery.append("car_name ");
                    break;
                case "2":
                    searchQuery.append("car_name desc ");
                    break;
                case "3":
                    searchQuery.append("price ");
                    break;
                case "4":
                    searchQuery.append("price desc ");
                    break;
                default:
                    break;
            }
            searchQuery.append("LIMIT ?, ?");
        }
        return String.valueOf(searchQuery);
    }

    private void addSpecificField(List<String> stringFields, List<Integer> intFields, int ourList, String form, String name, List<String> manufactures, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> colors, List<Integer> capacity, StringBuilder searchQuery) {
        for (String field : stringFields) {
            searchQuery.append(form).append(" = ?");
            if (checkLists(ourList, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity) || !Objects.equals(field, stringFields.get(stringFields.size() - 1))) {
                if (!Objects.equals(field, stringFields.get(stringFields.size() - 1))) {
                    searchQuery.append(" OR ");
                } else {
                    searchQuery.append(") AND (");
                }
            } else {
                searchQuery.append(") ");
            }
        }
        for (int field : intFields) {
            searchQuery.append(form).append(" = ?");
            if (checkLists(ourList, name, manufactures, types, fuelTypes, min, max, ratings, colors, capacity) || field != intFields.get(intFields.size() - 1)) {
                if (field != intFields.get(intFields.size() - 1)) {
                    searchQuery.append(" OR ");
                } else {
                    searchQuery.append(") AND (");
                }
            } else {
                searchQuery.append(") ");
            }
        }
    }

    private int addPStmt(PreparedStatement pStmt, int countPS, List<String> stringFields, List<Integer> intFields) throws SQLException {
        for (String field : stringFields) {
            pStmt.setString(countPS, field);
            countPS++;
        }
        for (int field : intFields) {
            pStmt.setInt(countPS, field);
            countPS++;
        }
        return countPS;
    }
}
