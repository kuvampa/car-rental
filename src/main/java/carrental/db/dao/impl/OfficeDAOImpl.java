package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.dao.OfficeDAO;
import carrental.db.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OfficeDAOImpl implements OfficeDAO {
    private static final Logger logger = LogManager.getLogger(OfficeDAOImpl.class);
    private final Connection connection;
    public static final String SELECT_ALL_OFFICES = "SELECT * FROM office";
    public static final String SELECT_OFFICE_BY_ID = "SELECT * FROM office WHERE id_office = ?";
    public static final String SELECT_OFFICE_BY_NAME = "SELECT * FROM office WHERE name = ?";
    public static final String INSERT_NEW_OFFICE = "INSERT INTO office(name, location, labor_hours, status, telephone_number, map) VALUES(?,?,?,?,?,?)";
    public static final String UPDATE_OFFICE_BY_ID = "UPDATE office SET name = ?, location = ?, labor_hours = ?, status = ?, telephone_number = ?, map = ? WHERE (id_office = ?)";
    public static final String DELETE_OFFICE_BY_ID = "DELETE FROM office WHERE (id_office = ?)";
    public static final String UPDATE_OFFICE_STATUS_BY_NAME = "UPDATE office SET status = ?  WHERE (name = ?)";

    public OfficeDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Office> findAll() throws DBException {
        logger.debug("a find all offices query");
        List<Office> offices = new ArrayList<>();
        try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery(SELECT_ALL_OFFICES)) {
            while (resultSet.next()) {
                Office office = new Office();
                office.setMap(resultSet.getString("map"));
                office.setIdOffice(resultSet.getInt("id_office"));
                office.setLocation(resultSet.getString("location"));
                office.setName(resultSet.getString("name"));
                office.setTelephoneNumber(resultSet.getString("telephone_number"));
                office.setStatus(resultSet.getString("status"));
                office.setLaborHours(resultSet.getString("labor_hours"));
                offices.add(office);
            }
        } catch (SQLException e) {
            logger.error("can't find all offices", e);
            throw new DBException("can't find all offices", e);
        }
        logger.debug("Found [{}] admins ", offices.size());
        return offices;
    }

    @Override
    public void insert(Office office) throws DBException {
        logger.debug("insert office query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_OFFICE)) {
            preparedStatement.setString(1, office.getName());
            preparedStatement.setString(2, office.getLocation());
            preparedStatement.setString(3, office.getLaborHours());
            preparedStatement.setString(4, office.getStatus());
            preparedStatement.setString(5, office.getTelephoneNumber());
            preparedStatement.setString(6, office.getMap());
            preparedStatement.executeUpdate();
            logger.debug("Insert [{}] office", office);
        } catch (SQLException e) {
            logger.error("can't add new office", e);
            throw new DBException("can't add new office", e);
        }
    }

    @Override
    public void update(Office office) throws DBException {
        logger.debug("update office query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_OFFICE_BY_ID)) {
            preparedStatement.setString(1, office.getName());
            preparedStatement.setString(2, office.getLocation());
            preparedStatement.setString(3, office.getLaborHours());
            preparedStatement.setString(4, office.getStatus());
            preparedStatement.setString(5, office.getTelephoneNumber());
            preparedStatement.setString(6, office.getMap());
            preparedStatement.setInt(7, office.getIdOffice());
            preparedStatement.executeUpdate();
            logger.debug("Update [{}] office", office);
        } catch (SQLException e) {
            logger.error("can't update office", e);
            throw new DBException("can't update office", e);
        }
    }

    @Override
    public void updateOfficeStatus(String name, String status) throws DBException {
        logger.debug("update office status query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_OFFICE_STATUS_BY_NAME)) {
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, name);
            preparedStatement.executeUpdate();
            logger.debug("Update admin status [{}] by [{}] name", status, name);
        } catch (SQLException e) {
            logger.error("can't update office status", e);
            throw new DBException("can't update office status", e);
        }
    }

    @Override
    public void deleteById(int id) throws DBException {
        logger.debug("delete office by id query");
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_OFFICE_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.debug("Delete office by id [{}]", id);
        } catch (SQLException e) {
            logger.error("can't delete office by id", e);
            throw new DBException("can't delete office by id", e);
        }
    }

    @Override
    public Office findOfficeByName(String name) throws DBException {
        logger.debug("find office by name query");
        Office office = new Office();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_OFFICE_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                office.setMap(resultSet.getString("map"));
                office.setIdOffice(resultSet.getInt("id_office"));
                office.setLocation(resultSet.getString("location"));
                office.setName(resultSet.getString("name"));
                office.setTelephoneNumber(resultSet.getString("telephone_number"));
                office.setStatus(resultSet.getString("status"));
                office.setLaborHours(resultSet.getString("labor_hours"));
            }
            logger.debug("Find office by name [{}]", office);
        } catch (SQLException e) {
            logger.error("can't find office by name", e);
            throw new DBException("can't find office by name", e);
        }
        return office;
    }

    @Override
    public Office findById(int officeId) throws DBException {
        logger.debug("Find office by id query");
        Office office = new Office();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_OFFICE_BY_ID)) {
            preparedStatement.setInt(1, officeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                office.setMap(resultSet.getString("map"));
                office.setIdOffice(resultSet.getInt("id_office"));
                office.setLocation(resultSet.getString("location"));
                office.setName(resultSet.getString("name"));
                office.setTelephoneNumber(resultSet.getString("telephone_number"));
                office.setStatus(resultSet.getString("status"));
                office.setLaborHours(resultSet.getString("labor_hours"));
            }
            logger.debug("Find office by id [{}]", office);
        } catch (SQLException e) {
            logger.error("can't find office by id", e);
            throw new DBException("can't find office by id", e);
        }
        return office;
    }
}
