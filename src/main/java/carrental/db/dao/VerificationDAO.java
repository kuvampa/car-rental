package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Verification;

public interface VerificationDAO extends DAO<Verification>{
    void updateVerificationStatus(int id, String status) throws DBException;

    void updateVerificationManager(int id, int manager) throws DBException;

    Verification findByRentId(int id) throws DBException;
}
