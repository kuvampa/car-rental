package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.UserSupport;

public interface UserSupportDAO extends DAO<UserSupport>{
    void updateUserSupportStatus(int id, String status) throws DBException;

    void updateUserSupportAdmin(int id, int admin) throws DBException;
}
