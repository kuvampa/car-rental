package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.AdditionalService;

public interface ServiceDAO extends DAO<AdditionalService> {
    void updateServiceStatus(String name, String status) throws DBException;

    AdditionalService findServiceByName(String name) throws DBException;
}
