package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.InsurancePackage;

public interface InsuranceDAO extends DAO<InsurancePackage>{
    void updateInsuranceStatus(String name, String status) throws DBException;

    InsurancePackage findByName(String name) throws DBException;
}
