package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Office;


public interface OfficeDAO extends DAO<Office>{
    void updateOfficeStatus(String name, String status) throws DBException;

    Office findOfficeByName(String name) throws DBException;
}
