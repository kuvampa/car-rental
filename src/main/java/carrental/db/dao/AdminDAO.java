package carrental.db.dao;

import carrental.db.DBException;
import carrental.db.entity.Admin;

public interface AdminDAO extends DAO<Admin> {
    Admin findAdmin(String login) throws DBException;

    void deleteByName(String login) throws DBException;

    void updateAdminDocs(String login, String docs) throws DBException;
}
