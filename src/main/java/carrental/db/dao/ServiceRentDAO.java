package carrental.db.dao;

import carrental.db.entity.ServiceRent;

public interface ServiceRentDAO extends DAO<ServiceRent> {
}
