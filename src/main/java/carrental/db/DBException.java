package carrental.db;

public class DBException extends Exception {
    public DBException() {
        super();
    }

    public DBException(String message, Throwable c) {
        super(message, c);
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(Throwable c) {
        super(c);
    }

}
