package carrental.db.entity;

import java.util.Objects;

public class Car {
    private int idCar;
    private int idOffice;
    private int idInsurance;
    private int idTermsPackage;
    private int capacity;
    private int rating;
    private double price;
    private String status;
    private String color;
    private String carName;
    private String plateNumber;
    private String type;
    private String manufacturer;
    private String fuelType;


    public Car() {

    }

    public Car(int idOffice, int idInsurance, int idTermsPackage, int capacity, int rating, double price, String status, String color, String carName, String plateNumber, String type, String manufacturer, String fuelType) {
        this.idOffice = idOffice;
        this.idInsurance = idInsurance;
        this.idTermsPackage = idTermsPackage;
        this.capacity = capacity;
        this.rating = rating;
        this.price = price;
        this.status = status;
        this.color = color;
        this.carName = carName;
        this.plateNumber = plateNumber;
        this.type = type;
        this.manufacturer = manufacturer;
        this.fuelType = fuelType;
    }

    public Car(int idCar, int idOffice, int idInsurance, int idTermsPackage, int capacity, int rating, double price, String status, String color, String carName, String plateNumber, String type, String manufacturer, String fuelType) {
        this.idCar = idCar;
        this.idOffice = idOffice;
        this.idInsurance = idInsurance;
        this.idTermsPackage = idTermsPackage;
        this.capacity = capacity;
        this.rating = rating;
        this.price = price;
        this.status = status;
        this.color = color;
        this.carName = carName;
        this.plateNumber = plateNumber;
        this.type = type;
        this.manufacturer = manufacturer;
        this.fuelType = fuelType;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public String getFuelType() {
        return fuelType;
    }

    public int getIdOffice() {
        return idOffice;
    }

    public void setIdOffice(int idOffice) {
        this.idOffice = idOffice;
    }

    public int getIdInsurance() {
        return idInsurance;
    }

    public void setIdInsurance(int idInsurance) {
        this.idInsurance = idInsurance;
    }

    public int getIdTermsPackage() {
        return idTermsPackage;
    }

    public void setIdTermsPackage(int idTermsPackage) {
        this.idTermsPackage = idTermsPackage;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return getIdCar() == car.getIdCar() && getIdOffice() == car.getIdOffice() && getIdInsurance() == car.getIdInsurance() && getIdTermsPackage() == car.getIdTermsPackage() && getCapacity() == car.getCapacity() && getRating() == car.getRating() && Double.compare(car.getPrice(), getPrice()) == 0 && getStatus().equals(car.getStatus()) && getColor().equals(car.getColor()) && getCarName().equals(car.getCarName()) && getPlateNumber().equals(car.getPlateNumber()) && getType().equals(car.getType()) && getManufacturer().equals(car.getManufacturer()) && getFuelType().equals(car.getFuelType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdCar(), getIdOffice(), getIdInsurance(), getIdTermsPackage(), getCapacity(), getRating(), getPrice(), getStatus(), getColor(), getCarName(), getPlateNumber(), getType(), getManufacturer(), getFuelType());
    }

    @Override
    public String toString() {
        return "Car{" +
                "idCar=" + idCar +
                ", idOffice=" + idOffice +
                ", idInsurance=" + idInsurance +
                ", idTermsPackage=" + idTermsPackage +
                ", capacity=" + capacity +
                ", rating=" + rating +
                ", price=" + price +
                ", status='" + status + '\'' +
                ", color='" + color + '\'' +
                ", carName='" + carName + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                ", type='" + type + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }
}

