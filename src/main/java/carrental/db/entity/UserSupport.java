package carrental.db.entity;

import java.util.Objects;

public class UserSupport {
    private int idUserSupport;
    private int idUser;
    private int idAdmin;
    private String message;
    private String status;

    public UserSupport() {
    }

    public UserSupport(int idUser, String message, String status) {
        this.idUser = idUser;
        this.message = message;
        this.status = status;
    }
    public UserSupport(int idUserSupport, int idUser, int idAdmin, String message, String status) {
        this.idUserSupport = idUserSupport;
        this.idUser = idUser;
        this.idAdmin = idAdmin;
        this.message = message;
        this.status = status;
    }

    public int getIdUserSupport() {
        return idUserSupport;
    }

    public void setIdUserSupport(int idUserSupport) {
        this.idUserSupport = idUserSupport;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserSupport)) return false;
        UserSupport that = (UserSupport) o;
        return getIdUserSupport() == that.getIdUserSupport() && getIdUser() == that.getIdUser() && getIdAdmin() == that.getIdAdmin() && getMessage().equals(that.getMessage()) && getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdUserSupport(), getIdUser(), getIdAdmin(), getMessage(), getStatus());
    }

    @Override
    public String toString() {
        return "UserSupport{" +
                "idUserSupport=" + idUserSupport +
                ", idUser=" + idUser +
                ", idAdmin=" + idAdmin +
                ", message='" + message + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

