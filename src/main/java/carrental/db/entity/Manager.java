package carrental.db.entity;

import java.util.Objects;

public class Manager {
    private int idManager;
    private String login;
    private String password;
    private String fullName;
    private String telephoneNumber;
    private String email;
    private String documents;

    public Manager() {
    }

    public Manager(String login, String password, String fullName, String telephoneNumber, String email, String documents) {
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.documents = documents;
    }

    public Manager(int idManager, String login, String password, String fullName, String telephoneNumber, String email, String documents) {
        this.idManager = idManager;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.documents = documents;
    }

    public Manager(int idManager, String fullName, String telephoneNumber, String email, String login, String password) {
        this.idManager = idManager;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
    }

    public int getIdManager() {
        return idManager;
    }

    public void setIdManager(int idManager) {
        this.idManager = idManager;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Manager)) return false;
        Manager manager = (Manager) o;
        return getIdManager() == manager.getIdManager() && getLogin().equals(manager.getLogin()) && getPassword().equals(manager.getPassword()) && getFullName().equals(manager.getFullName()) && getTelephoneNumber().equals(manager.getTelephoneNumber()) && getEmail().equals(manager.getEmail()) && Objects.equals(getDocuments(), manager.getDocuments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdManager(), getLogin(), getPassword(), getFullName(), getTelephoneNumber(), getEmail(), getDocuments());
    }

    @Override
    public String toString() {
        return "Manager{" +
                "idManager=" + idManager +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", documents='" + documents + '\'' +
                '}';
    }
}
