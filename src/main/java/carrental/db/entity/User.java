package carrental.db.entity;

import java.util.Objects;

public class User {
    private int idUser;
    private int status;
    private String fullName;
    private String telephoneNumber;
    private String email;
    private String login;
    private String password;
    private String documents;

    public User() {

    }

    public User(int idUser, String fullName, String telephoneNumber, String email, String login, String password) {
        this.idUser = idUser;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public User(int idUser, String fullName, String telephoneNumber, String email, String login, String password, String documents) {
        this.idUser = idUser;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
        this.documents = documents;
    }

    public User(String fullName, String telephoneNumber, String email, String login, String password, int status) {
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
        this.status = status;
    }

    public User(int idUser, int status, String fullName, String telephoneNumber, String email, String login, String password, String documents) {
        this.idUser = idUser;
        this.status = status;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
        this.documents = documents;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getFullName() {
        return fullName;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getEmail() {
        return email;
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getIdUser() == user.getIdUser() && getStatus() == user.getStatus() && getFullName().equals(user.getFullName()) && getTelephoneNumber().equals(user.getTelephoneNumber()) && getEmail().equals(user.getEmail()) && getLogin().equals(user.getLogin()) && getPassword().equals(user.getPassword()) && Objects.equals(getDocuments(), user.getDocuments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdUser(), getStatus(), getFullName(), getTelephoneNumber(), getEmail(), getLogin(), getPassword(), getDocuments());
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", status=" + status +
                ", fullName='" + fullName + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", documents='" + documents + '\'' +
                '}';
    }
}
