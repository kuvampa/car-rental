package carrental.db.entity;

import java.util.Objects;

public class Verification {
    private int idVerificationOfDocuments;
    private int idRental;
    private int idManager;
    private String status;
    private String comment;

    public Verification() {
    }

    public Verification(int idRental, String status) {
        this.idRental = idRental;
        this.status = status;
    }

    public Verification(int idVerificationOfDocuments, int idRental, int idManager, String status, String comment) {
        this.idVerificationOfDocuments = idVerificationOfDocuments;
        this.idRental = idRental;
        this.idManager = idManager;
        this.status = status;
        this.comment = comment;
    }

    public int getIdVerificationOfDocuments() {
        return idVerificationOfDocuments;
    }

    public void setIdVerificationOfDocuments(int idVerificationOfDocuments) {
        this.idVerificationOfDocuments = idVerificationOfDocuments;
    }

    public int getIdRental() {
        return idRental;
    }

    public void setIdRental(int idRental) {
        this.idRental = idRental;
    }

    public int getIdManager() {
        return idManager;
    }

    public void setIdManager(int idManager) {
        this.idManager = idManager;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Verification)) return false;
        Verification that = (Verification) o;
        return getIdVerificationOfDocuments() == that.getIdVerificationOfDocuments() && getIdRental() == that.getIdRental() && getIdManager() == that.getIdManager() && getStatus().equals(that.getStatus()) && Objects.equals(getComment(), that.getComment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdVerificationOfDocuments(), getIdRental(), getIdManager(), getStatus(), getComment());
    }

    @Override
    public String toString() {
        return "Verification{" +
                "idVerificationOfDocuments=" + idVerificationOfDocuments +
                ", idRental=" + idRental +
                ", idManager=" + idManager +
                ", status='" + status + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
