package carrental.db.entity;

import java.util.Objects;

public class Accident {
    private int idAccident;
    private int idRental;
    private int idManager;
    private double price;
    private String comment;
    private String status;

    public Accident() {
    }

    public Accident(int rent, int manager, double price, String comment, String status) {
        this.idRental = rent;
        this.idManager = manager;
        this.price = price;
        this.comment = comment;
        this.status = status;
    }

    public Accident(int idAccident, int rent, int manager, double price, String comment, String status) {
        this.idAccident = idAccident;
        this.idRental = rent;
        this.idManager = manager;
        this.price = price;
        this.comment = comment;
        this.status = status;
    }

    public int getIdAccident() {
        return idAccident;
    }

    public void setIdAccident(int idAccident) {
        this.idAccident = idAccident;
    }

    public int getIdRental() {
        return idRental;
    }

    public void setIdRental(int rent) {
        this.idRental = rent;
    }

    public int getIdManager() {
        return idManager;
    }

    public void setIdManager(int manager) {
        this.idManager = manager;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Accident)) return false;
        Accident accident = (Accident) o;
        return getIdAccident() == accident.getIdAccident() && getIdRental() == accident.getIdRental() && getIdManager() == accident.getIdManager() && Double.compare(accident.getPrice(), getPrice()) == 0 && getComment().equals(accident.getComment()) && getStatus().equals(accident.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdAccident(), getIdRental(), getIdManager(), getPrice(), getComment(), getStatus());
    }

    @Override
    public String toString() {
        return "Accident{" +
                "idAccident=" + idAccident +
                ", idRental=" + idRental +
                ", idManager=" + idManager +
                ", price=" + price +
                ", comment='" + comment + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
