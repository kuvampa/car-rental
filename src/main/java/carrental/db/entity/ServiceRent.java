package carrental.db.entity;

import java.util.Objects;

public class ServiceRent {
    private int idRentService;
    private int idRental;
    private int idService;

    public ServiceRent(int idRental, int idService) {
        this.idRental = idRental;
        this.idService = idService;
    }

    public ServiceRent(int idRentService, int idRental, int idService) {
        this.idRentService = idRentService;
        this.idRental = idRental;
        this.idService = idService;
    }

    public ServiceRent() {
    }

    public int getIdRentService() {
        return idRentService;
    }

    public void setIdRentService(int idRentService) {
        this.idRentService = idRentService;
    }

    public int getIdRental() {
        return idRental;
    }

    public void setIdRental(int idRental) {
        this.idRental = idRental;
    }

    public int getIdService() {
        return idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }

    @Override
    public String toString() {
        return "ServiceRent{" +
                "idRentService=" + idRentService +
                ", idRental=" + idRental +
                ", idService=" + idService +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceRent)) return false;
        ServiceRent that = (ServiceRent) o;
        return getIdRentService() == that.getIdRentService() && getIdRental() == that.getIdRental() && getIdService() == that.getIdService();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdRentService(), getIdRental(), getIdService());
    }
}
