package carrental.db.entity;

import java.util.Objects;

public class AdditionalService {
    private int idService;
    private String name;
    private double price;
    private String description;
    private String status;

    public AdditionalService() {

    }

    public AdditionalService(String name, double price, String description, String status) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.status = status;
    }

    public AdditionalService(int idService, String name, double price, String description, String status) {
        this.idService = idService;
        this.name = name;
        this.price = price;
        this.description = description;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdService() {
        return idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdditionalService)) return false;
        AdditionalService that = (AdditionalService) o;
        return getIdService() == that.getIdService() && Double.compare(that.getPrice(), getPrice()) == 0 && getName().equals(that.getName()) && Objects.equals(getDescription(), that.getDescription()) && getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdService(), getName(), getPrice(), getDescription(), getStatus());
    }

    @Override
    public String toString() {
        return "AdditionalService{" +
                "idService=" + idService +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
