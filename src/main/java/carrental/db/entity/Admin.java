package carrental.db.entity;

import java.util.Objects;

public class Admin {
    private int idAdmin;
    private String login;
    private String password;
    private String fullName;
    private String telephoneNumber;
    private String email;
    private String documents;

    public Admin() {
    }

    public Admin(int idAdmin, String login, String password, String fullName, String telephoneNumber, String email) {
        this.idAdmin = idAdmin;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
    }
    public Admin(int idAdmin, String login, String password, String fullName, String telephoneNumber, String email, String documents) {
        this.idAdmin = idAdmin;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.documents = documents;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return getIdAdmin() == admin.getIdAdmin() && getLogin().equals(admin.getLogin()) && getPassword().equals(admin.getPassword()) && getFullName().equals(admin.getFullName()) && getTelephoneNumber().equals(admin.getTelephoneNumber()) && Objects.equals(getEmail(), admin.getEmail()) && Objects.equals(getDocuments(), admin.getDocuments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdAdmin(), getLogin(), getPassword(), getFullName(), getTelephoneNumber(), getEmail(), getDocuments());
    }

    @Override
    public String toString() {
        return "Admin{" +
                "idAdmin=" + idAdmin +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", documents='" + documents + '\'' +
                '}';
    }
}
