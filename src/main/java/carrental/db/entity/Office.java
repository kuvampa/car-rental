package carrental.db.entity;

import java.util.Objects;

public class Office {

    private int idOffice;
    private String name;
    private String location;
    private String laborHours;
    private String status;
    private String telephoneNumber;
    private String map;

    public Office() {

    }

    public Office(String name, String location, String laborHours, String status, String telephoneNumber, String map) {
        this.name = name;
        this.location = location;
        this.laborHours = laborHours;
        this.status = status;
        this.telephoneNumber = telephoneNumber;
        this.map = map;
    }

    public Office(int idOffice, String name, String location, String laborHours, String status, String telephoneNumber, String map) {
        this.idOffice = idOffice;
        this.name = name;
        this.location = location;
        this.laborHours = laborHours;
        this.status = status;
        this.telephoneNumber = telephoneNumber;
        this.map = map;
    }

    public int getIdOffice() {
        return idOffice;
    }

    public void setIdOffice(int idOffice) {
        this.idOffice = idOffice;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLaborHours() {
        return laborHours;
    }

    public void setLaborHours(String laborHours) {
        this.laborHours = laborHours;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Office)) return false;
        Office office = (Office) o;
        return getIdOffice() == office.getIdOffice() && getName().equals(office.getName()) && getLocation().equals(office.getLocation()) && getLaborHours().equals(office.getLaborHours()) && getStatus().equals(office.getStatus()) && getTelephoneNumber().equals(office.getTelephoneNumber()) && Objects.equals(getMap(), office.getMap());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdOffice(), getName(), getLocation(), getLaborHours(), getStatus(), getTelephoneNumber(), getMap());
    }

    @Override
    public String toString() {
        return "Office{" +
                "idOffice=" + idOffice +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", laborHours='" + laborHours + '\'' +
                ", status='" + status + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", map='" + map + '\'' +
                '}';
    }
}
