package carrental.db.entity;

import java.util.Objects;

public class TermsOfUse {
    private int idTermsPackage;
    private String name;
    private String fuelConditions;
    private String mileageLimit;
    private String status;

    public TermsOfUse() {

    }

    public TermsOfUse(String name, String fuelConditions, String mileageLimit, String status) {
        this.name = name;
        this.fuelConditions = fuelConditions;
        this.mileageLimit = mileageLimit;
        this.status = status;
    }
    public TermsOfUse(int idTermsPackage, String name, String fuelConditions, String mileageLimit, String status) {
        this.idTermsPackage = idTermsPackage;
        this.name = name;
        this.fuelConditions = fuelConditions;
        this.mileageLimit = mileageLimit;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdTermsPackage() {
        return idTermsPackage;
    }

    public void setIdTermsPackage(int idTermsPackage) {
        this.idTermsPackage = idTermsPackage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFuelConditions() {
        return fuelConditions;
    }

    public void setFuelConditions(String fuelConditions) {
        this.fuelConditions = fuelConditions;
    }

    public String getMileageLimit() {
        return mileageLimit;
    }

    public void setMileageLimit(String mileageLimit) {
        this.mileageLimit = mileageLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TermsOfUse)) return false;
        TermsOfUse that = (TermsOfUse) o;
        return getIdTermsPackage() == that.getIdTermsPackage() && getName().equals(that.getName()) && getFuelConditions().equals(that.getFuelConditions()) && getMileageLimit().equals(that.getMileageLimit()) && getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdTermsPackage(), getName(), getFuelConditions(), getMileageLimit(), getStatus());
    }

    @Override
    public String toString() {
        return "TermsOfUse{" +
                "idTermsPackage=" + idTermsPackage +
                ", name='" + name + '\'' +
                ", fuelConditions='" + fuelConditions + '\'' +
                ", mileageLimit='" + mileageLimit + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
