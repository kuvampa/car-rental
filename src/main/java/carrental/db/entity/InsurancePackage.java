package carrental.db.entity;

import java.util.Objects;

public class InsurancePackage {
    private int idInsurance;
    private String name;
    private String description;
    private String status;

    public InsurancePackage() {

    }

    public InsurancePackage(String name, String description, String status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }
    public InsurancePackage(int idInsurance, String name, String description, String status) {
        this.idInsurance = idInsurance;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public int getIdInsurance() {
        return idInsurance;
    }

    public void setIdInsurance(int idInsurance) {
        this.idInsurance = idInsurance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InsurancePackage)) return false;
        InsurancePackage that = (InsurancePackage) o;
        return getIdInsurance() == that.getIdInsurance() && getName().equals(that.getName()) && getDescription().equals(that.getDescription()) && getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdInsurance(), getName(), getDescription(), getStatus());
    }

    @Override
    public String toString() {
        return "InsurancePackage{" +
                "idInsurance=" + idInsurance +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
