package carrental.db.entity;

import java.time.LocalDateTime;
import java.util.Objects;

public class Rental {
    private int idRental;
    private int idUser;
    private int idCar;
    private int rightOfCancellation;
    private double price;
    private String returnPlace;
    private LocalDateTime dateOfReceiving;
    private LocalDateTime returnDate;

    public Rental() {

    }

    public Rental(int idUser, int idCar, int rightOfCancellation, double price, String returnPlace, LocalDateTime dateOfReceiving, LocalDateTime returnDate) {
        this.idUser = idUser;
        this.idCar = idCar;
        this.rightOfCancellation = rightOfCancellation;
        this.price = price;
        this.returnPlace = returnPlace;
        this.dateOfReceiving = dateOfReceiving;
        this.returnDate = returnDate;
    }

    public Rental(int idRental, int idUser, int idCar, int rightOfCancellation, double price, String returnPlace,LocalDateTime dateOfReceiving, LocalDateTime returnDate) {
        this.idRental = idRental;
        this.idUser = idUser;
        this.idCar = idCar;
        this.rightOfCancellation = rightOfCancellation;
        this.price = price;
        this.returnPlace = returnPlace;
        this.dateOfReceiving = dateOfReceiving;
        this.returnDate = returnDate;
    }

    public int getIdRental() {
        return idRental;
    }

    public void setIdRental(int idRental) {
        this.idRental = idRental;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public LocalDateTime getDateOfReceiving() {
        return dateOfReceiving;
    }

    public void setDateOfReceiving(LocalDateTime dateOfReceiving) {
        this.dateOfReceiving = dateOfReceiving;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnPlace() {
        return returnPlace;
    }

    public void setReturnPlace(String returnPlace) {
        this.returnPlace = returnPlace;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getRightOfCancellation() {
        return rightOfCancellation;
    }

    public void setRightOfCancellation(int rightOfCancellation) {
        this.rightOfCancellation = rightOfCancellation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rental)) return false;
        Rental rental = (Rental) o;
        return getIdRental() == rental.getIdRental() && getIdUser() == rental.getIdUser() && getIdCar() == rental.getIdCar() && getRightOfCancellation() == rental.getRightOfCancellation() && Double.compare(rental.getPrice(), getPrice()) == 0 && getReturnPlace().equals(rental.getReturnPlace()) && getDateOfReceiving().equals(rental.getDateOfReceiving()) && getReturnDate().equals(rental.getReturnDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdRental(), getIdUser(), getIdCar(), getRightOfCancellation(), getPrice(), getReturnPlace(), getDateOfReceiving(), getReturnDate());
    }

    @Override
    public String toString() {
        return "Rental{" +
                "idRental=" + idRental +
                ", idUser=" + idUser +
                ", idCar=" + idCar +
                ", rightOfCancellation=" + rightOfCancellation +
                ", price=" + price +
                ", returnPlace='" + returnPlace + '\'' +
                ", dateOfReceiving=" + dateOfReceiving +
                ", returnDate=" + returnDate +
                '}';
    }
}
