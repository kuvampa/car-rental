package carrental.controller;

import carrental.controller.command.*;
import carrental.controller.util.ImageOutput;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Here I have implemented my only servlet class which is a special type of Java class
 * that runs on a web server and that handles requests and returns the processing result.
 * @author Novikov Artem
 * @version 1.1
 * @see WebServlet
 */

@WebServlet(urlPatterns = {"/app/*", "/file/*"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024,     // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)

public class AlphaOmegaServlet extends HttpServlet {
    private final Map<String, Command> commandMap = new HashMap<>();
    private static final String DIRECTORY_PATH = "C://Users/Kuvam/Desktop/data/";
    private static final Logger logger = LogManager.getLogger(AlphaOmegaServlet.class);

    @Override
    public void init() {
        logger.info("Initializing AlphaOmegaServlet#init");
        commandMap.put("/adminCars", new AdminCarsCommand());
        commandMap.put("/adminUsers", new AdminUsersCommand());
        commandMap.put("/adminTerms", new AdminTermsCommand());
        commandMap.put("/adminService", new AdminServiceCommand());
        commandMap.put("/adminSupport", new AdminSupportCommand());
        commandMap.put("/adminInsurance", new AdminInsurance());
        commandMap.put("/adminOffices", new AdminOfficesCommand());
        commandMap.put("/managerAccident", new ManagerAccidentCommand());
        commandMap.put("/managerVerification", new ManagerVerificationCommand());
        commandMap.put("/managerAllRents", new ManagerAllRentsCommand());
        commandMap.put("/userAccident", new UserAccidentCommand());
        commandMap.put("/userRent", new UserRentCommand());
        commandMap.put("/log", new LogCommand());
        commandMap.put("/find", new FindCommand());
        commandMap.put("/register", new RegisterCommand());
        commandMap.put("/inf", new InfCommand());
        commandMap.put("/quit", new QuitCommand());
        commandMap.put("/start", new StartCommand());
        commandMap.put("/update", new UpdateCommand());
        commandMap.put("/update2", new Update2Command());
        commandMap.put("/delete", new DeleteCommand());
        commandMap.put("/delete2", new Delete2Command());
        commandMap.put("/addDocs", new AddDocsCommand());
        commandMap.put("/fileUpload", new FileUploadCommand());
        commandMap.put("/error", new ErrorCommand());
        commandMap.put("/descriptionCar", new DescriptionCarCommand());
        commandMap.put("/rent", new RentCommand());
        commandMap.put("/verification", new VerificationCommand());
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private void process(HttpServletRequest req, HttpServletResponse resp, String method) throws ServletException, IOException {
        logger.info("Initializing AlphaOmegaServlet#process");
        logger.debug("Received request path: [{}].", req.getPathInfo());
        HttpSession session = req.getSession();
        session.setAttribute("directoryPath",DIRECTORY_PATH);
        if (req.getServletPath().equals("/file")) {
            ImageOutput.output(req, resp);
        } else {
            Command command = commandMap.getOrDefault(req.getPathInfo(), new StartCommand());
            logger.debug("[{}] command interpreted as [{}]", req.getPathInfo(), command);
            String result = command.execute(req, method);
            logger.debug("Execute result: {}", result);
            if (method.equals("post")||result.equals("/app/error")) {
                resp.sendRedirect(req.getContextPath() + result);
            }
            if (method.equals("get")&&!result.equals("/app/error")) {
                req.getRequestDispatcher(result).forward(req, resp);
            }
        }
    }

    /**
     * Handles the HTTP <code>Post</code> method.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws RuntimeException if these are unchecked exceptions.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, RuntimeException {
        logger.info("Initializing AlphaOmegaServlet#doPost");
        process(req, resp, "post");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws RuntimeException if these are unchecked exceptions.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, RuntimeException {
        logger.info("Initializing AlphaOmegaServlet#doGet");
        process(req, resp, "get");
    }
}