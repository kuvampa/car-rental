package carrental.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * UnauthorizedFilter used to redirect unregistered users to the right pages
 * @author Novikov Artem
 * @version 1.1
 * @see WebFilter
 */
@WebFilter(filterName = "UnauthorizedFilter", urlPatterns = {"/app/verification", "/app/userRent", "/app/userAccident", "/app/update2", "/app/update", "/app/rent", "/app/quit", "/app/fileuploadservlet", "/app/delete2", "/app/delete", "/app/addDocs.jsp"})
public class UnauthorizedFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getSession() != null && req.getSession().getAttribute("login") != null) {
            chain.doFilter(request, response);
        } else resp.sendRedirect(req.getContextPath() + "/app/log");
    }
}