package carrental.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * ManagerFilter used to redirect users if they want to go to the manager page
 * @author Novikov Artem
 * @version 1.1
 * @see WebFilter
 */
@WebFilter(filterName = "ManagerFilter", urlPatterns = {"/app/managerVerification", "/app/managerAllRents","/app/managerAccident"})
public class ManagerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getSession() != null && req.getSession().getAttribute("login") != null) {
            if (req.getSession().getAttribute("manager") != null) {
                chain.doFilter(request, response);
            } else {
                resp.sendRedirect(req.getContextPath() + "/app/index");
            }
        } else resp.sendRedirect(req.getContextPath() + "/app/log");
    }
}
