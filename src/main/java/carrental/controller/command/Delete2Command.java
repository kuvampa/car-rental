package carrental.controller.command;

import carrental.controller.util.EncryptPassword;
import carrental.db.DBException;
import carrental.handling.UserHandling;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Delete2Command implements Command {
    private static final Logger logger = LogManager.getLogger(Delete2Command.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        String loginS = "login";
        try {
            String login = session.getAttribute(loginS).toString();
            String password = req.getParameter("password");
            String repeatPassword = req.getParameter("repeat password");
            if (password.equals(repeatPassword) && EncryptPassword.encrypt(password).equals(userHandling.findUserByName(login).getPassword())) {
                userHandling.deleteByName(login);
                session.removeAttribute(loginS);
                session.removeAttribute("admin");
                session.removeAttribute("manager");
                req.setAttribute(loginS, login + " deleted");
            } else {
                req.setAttribute(loginS, login + "wrong password");
            }
        } catch (DBException | NoSuchAlgorithmException e) {
            return generateErrorPage("can't enter to confirm delete process ", logger, req);
        }
        return "/app/start";
    }
}
