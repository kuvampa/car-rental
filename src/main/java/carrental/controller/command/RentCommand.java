package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.*;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class RentCommand implements Command {
    private static final Logger logger = LogManager.getLogger(RentCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling);
        } else {
            return getPart(req, session, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "rent");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/rent");
        } catch (DBException e) {
            return generateErrorPage("can't enter to rent page", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        TermsHandling termsHandling = new TermsHandling();
        InsuranseHandling insuranseHandling = new InsuranseHandling();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        AdditionalServiceHandling additionalServiceHandling = new AdditionalServiceHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String carPlateRent = req.getParameter("CarPlateRent");
            if (carPlateRent == null) {
                carPlateRent = session.getAttribute("car").toString();
            }
            Object loginRent = session.getAttribute("login");
            User userRent = userHandling.findUserByName(loginRent.toString());
            Car carRent = carHandling.findCarByPlate(carPlateRent);
            TermsOfUse termsOfUseRent = termsHandling.findTermById(carRent.getIdTermsPackage());
            InsurancePackage insurancePackageRent = insuranseHandling.findInsuranceById(carRent.getIdInsurance());
            List<AdditionalService> additionalServicesRent = additionalServiceHandling.findAllServices();
            List<Rental> rentalsByRet = rentHandling.findRentsByCarIdSortByReturnDate(carHandling.findCarByPlate(carPlateRent).getIdCar());
            List<Rental> rentalsByRec = rentHandling.findRentsByCarIdSortByReturnDate(carHandling.findCarByPlate(carPlateRent).getIdCar());
            String docsInUser = userRent.getDocuments();
            session.setAttribute("car", carPlateRent);
            req.setAttribute("min", LocalDate.now() + "T" + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute());
            req.setAttribute("max", LocalDate.now().plusYears(1) + "T" + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute());
            req.setAttribute("rentalsByRet", rentalsByRet);
            req.setAttribute("rentalsByRec", rentalsByRec);
            req.setAttribute("additionalServicesRent", additionalServicesRent);
            req.setAttribute("carRent", carRent);
            req.setAttribute("termsOfUseRent", termsOfUseRent);
            req.setAttribute("insurancePackageRent", insurancePackageRent);
            req.setAttribute("userRent", userRent);
            req.setAttribute("docs", docsInUser);
            return "/rent.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't enter to rental process", logger, req);
        }
    }
}
