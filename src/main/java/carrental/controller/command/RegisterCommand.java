package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class RegisterCommand implements Command {
    private static final Logger logger = LogManager.getLogger(RegisterCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session);
        } else {
            return getPart(req, session);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "register");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/register");
        } catch (DBException e) {
            return generateErrorPage("can't enter to register page", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        OfficeHandling officeHandling = new OfficeHandling();
        String action = req.getPathInfo();
        try {
            navbarShell(req, session, officeHandling, action);
            return "/register.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't enter to registration process ", logger, req);
        }
    }
}
