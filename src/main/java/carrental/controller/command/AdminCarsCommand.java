package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.Car;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class AdminCarsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminCarsCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        InsuranseHandling insuranseHandling = new InsuranseHandling();
        TermsHandling termsHandling = new TermsHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, officeHandling, carHandling, termsHandling, insuranseHandling);
        } else {
            return getPart(req, session, officeHandling, carHandling, termsHandling, insuranseHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, OfficeHandling officeHandling, CarHandling carHandling, TermsHandling termsHandling, InsuranseHandling insuranseHandling) throws ServletException, IOException {
        String available = "available";
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        UserHandling userHandling = new UserHandling();
        try {
            String newCancel = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            String updateCar = req.getParameter("Update-Car");
            String deleteCar = req.getParameter("Delete-Car");
            String acceptUpdate = req.getParameter("accept");
            String newImage = req.getParameter("newImage");
            String cancel = req.getParameter("cancel");
            if (newCancel != null) {
                req.removeAttribute("newCar");
            }
            String acceptInsertString = acceptInsert(req, officeHandling, insuranseHandling, termsHandling, carHandling, acceptInsert, available);
            if (acceptInsertString != null) {
                return acceptInsertString;
            }
            String acceptUpdateString = acceptUpdate(req, officeHandling, insuranseHandling, termsHandling, carHandling, acceptUpdate, available);
            if (acceptUpdateString != null) {
                return acceptUpdateString;
            }
            String newImageS = acceptUpdate(req,session,newImage);
            if(newImageS!=null){
                return newImageS;
            }
            if (updateCar != null) {
                carHandling.updateCarStatus(updateCar, "updated");
            }
            if (deleteCar != null) {
                carHandling.deleteCar(carHandling.findCarByPlate(deleteCar).getIdCar());
                Files.delete(Path.of(session.getAttribute("directoryPath") + "cars/" + newImage + ".jpg"));
            }
            if (cancel != null) {
                carHandling.updateCarStatus(cancel, available);
            }
            req.setAttribute("file", "adminCars");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminCars");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin cars ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, OfficeHandling officeHandling, CarHandling carHandling, TermsHandling termsHandling, InsuranseHandling insuranseHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertCar = req.getParameter("insertCar");
            if (insertCar != null) {
                req.setAttribute("newCar", "insertCar");
            }
            req.setAttribute("cars", carHandling.findAllCars());
            req.setAttribute("terms", termsHandling.findAllTerms());
            req.setAttribute("insurances", insuranseHandling.findAllInsurances());
            return "/adminCars.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin cars ", logger, req);
        }
    }
    private String acceptUpdate(HttpServletRequest req,HttpSession session,String newImage) throws ServletException, IOException {
        if (newImage != null) {
            boolean empty = true;
            for (Part part : req.getParts()) {
                if (part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                    empty = false;
                }
                if (!empty) {
                    part.write(session.getAttribute("directoryPath") + "cars/" + newImage + ".jpg");
                    logger.debug("new photo for {}", newImage);
                }
            }
            if (empty) {
                return "/app/adminCars";
            }
        }
        return null;
    }

    private String acceptInsert(HttpServletRequest req, OfficeHandling officeHandling, InsuranseHandling insuranseHandling, TermsHandling termsHandling, CarHandling carHandling, String acceptInsert, String available) throws DBException, ServletException, IOException {
        if (acceptInsert != null) {
            String carName = req.getParameter("new_car_name");
            String carPriceS = req.getParameter("new_car_price");
            double carPrice = 0;
            if (carPriceS != null) {
                carPrice = Double.parseDouble(carPriceS);
            }
            String carType = req.getParameter("new_car_type");
            String carColor = req.getParameter("new_car_color");
            String carCapacityS = req.getParameter("new_car_capacity");
            int carCapacity = 0;
            if (carCapacityS != null) {
                carCapacity = Integer.parseInt(carCapacityS);
            }
            String carRatingS = req.getParameter("new_car_rating");
            int carRating = 0;
            if (carRatingS != null) {
                carRating = Integer.parseInt(carRatingS);
            }
            String carFuelType = req.getParameter("new_car_fuelType");
            String carManufacturer = req.getParameter("new_car_manufacturer");
            String officeName = req.getParameter("new_officeName");
            String termName = req.getParameter("new_termName");
            String insuranceName = req.getParameter("new_insuranceName");
            String carPlate = req.getParameter("new_car_plate");
            Car carInsert = new Car(officeHandling.findOfficeByName(officeName).getIdOffice(), insuranseHandling.findInsuranceByName(insuranceName).getIdInsurance(), termsHandling.findTermByName(termName).getIdTermsPackage(), carCapacity, carRating, carPrice, available, carColor, carName, carPlate, carType, carManufacturer, carFuelType);
            if (!Validator.Insert.carCreate(carInsert)) {
                return generateErrorPage("can't pass car create validation", logger, req);
            }
            carHandling.insertCar(carInsert);
        }
        return null;
    }

    private String acceptUpdate(HttpServletRequest req, OfficeHandling officeHandling, InsuranseHandling insuranseHandling, TermsHandling termsHandling, CarHandling carHandling, String acceptUpdate, String available) throws DBException, ServletException, IOException {
        if (acceptUpdate != null) {
            String carName = req.getParameter("car_name");
            String carPriceS = req.getParameter("car_price");
            double carPrice = 0;
            if (carPriceS != null) {
                carPrice = Double.parseDouble(carPriceS);
            }
            String carType = req.getParameter("car_type");
            String carColor = req.getParameter("car_color");
            String carCapacityS = req.getParameter("car_capacity");
            int carCapacity = 0;
            if (carCapacityS != null) {
                carCapacity = Integer.parseInt(carCapacityS);
            }
            String carRatingS = req.getParameter("car_rating");
            int carRating = 0;
            if (carRatingS != null) {
                carRating = Integer.parseInt(carRatingS);
            }
            String carFuelType = req.getParameter("car_fuelType");
            String carManufacturer = req.getParameter("car_manufacturer");
            String officeName = req.getParameter("officeName");
            String termName = req.getParameter("termName");
            String insuranceName = req.getParameter("insuranceName");
            String carPlate = req.getParameter("car_plate");
            Car carUp = carHandling.findCarByPlate(acceptUpdate);
            Car carUpdate = new Car(carUp.getIdCar(), officeHandling.findOfficeByName(officeName).getIdOffice(), insuranseHandling.findInsuranceByName(insuranceName).getIdInsurance(), termsHandling.findTermByName(termName).getIdTermsPackage(), carCapacity, carRating, carPrice, available, carColor, carName, carPlate, carType, carManufacturer, carFuelType);
            if (!Validator.Update.carUpdate(carUpdate)) {
                return generateErrorPage("can't pass car update validation", logger, req);
            }
            carHandling.updateCar(carUpdate);
        }
        return null;
    }
}
