package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.Office;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminOfficesCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminOfficesCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        OfficeHandling officeHandling = new OfficeHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, officeHandling);
        } else {
            return getPart(req, session, officeHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, OfficeHandling officeHandling) throws ServletException, IOException {
        String available = "available";
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "adminOffices");
            String newCancel = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            String updateOffice = req.getParameter("Update-Office");
            String deleteOffice = req.getParameter("Delete-Office");
            String acceptUpdate = req.getParameter("accept");
            String cancel = req.getParameter("cancel");
            if (newCancel != null) {
                req.removeAttribute("newOffice");
            }
            String acceptInsertString = acceptInsert(req, acceptInsert, officeHandling, available);
            if (acceptInsertString != null) {
                return acceptInsertString;
            }
            String acceptUpdateString = acceptUpdate(req, acceptUpdate, officeHandling, available);
            if (acceptUpdateString != null) {
                return acceptUpdateString;
            }
            if (updateOffice != null) {
                officeHandling.updateOfficeStatus(updateOffice, "updated");
            }
            if (deleteOffice != null) {
                officeHandling.deleteOffice(officeHandling.findOfficeByName(deleteOffice).getIdOffice());
            }
            if (cancel != null) {
                officeHandling.updateOfficeStatus(cancel, available);
            }
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminOffices");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin offices file ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, OfficeHandling officeHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertOffice = req.getParameter("InsertOffice");
            if (insertOffice != null) {
                req.setAttribute("newOffice", "insert");
            }
            return "/adminOffices.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't enter a admin office page ", logger, req);
        }
    }

    private String acceptInsert(HttpServletRequest req, String acceptInsert, OfficeHandling officeHandling, String available) throws ServletException, IOException, DBException {
        if (acceptInsert != null) {
            String officeName = req.getParameter("new_office_name");
            String officeLocation = req.getParameter("new_office_location");
            String officeLaborHours = req.getParameter("new_office_labor_hours");
            String officeTelephoneNumber = req.getParameter("new_office_telephone_number");
            String officeMap = req.getParameter("new_office_map");
            Office officeInsert = new Office(officeName, officeLocation, officeLaborHours, available, officeTelephoneNumber, officeMap);
            if (!Validator.Insert.officeCreate(officeInsert)) {
                return generateErrorPage("can't pass create new office validation", logger, req);
            }
            officeHandling.insertOffice(officeInsert);
        }
        return null;
    }

    private String acceptUpdate(HttpServletRequest req, String acceptUpdate, OfficeHandling officeHandling, String available) throws ServletException, IOException, DBException {
        if (acceptUpdate != null) {
            String officeName = req.getParameter("office_name");
            String officeLocation = req.getParameter("office_location");
            String officeLaborHours = req.getParameter("office_labor_hours");
            String officeTelephoneNumber = req.getParameter("office_telephone_number");
            String officeMap = req.getParameter("office_map");
            Office office = officeHandling.findOfficeByName(acceptUpdate);
            Office officeUpdate = new Office(office.getIdOffice(), officeName, officeLocation, officeLaborHours, available, officeTelephoneNumber, officeMap);
            if (!Validator.Update.officeUpdate(officeUpdate)) {
                return generateErrorPage("can't pass update office validation", logger, req);
            }
            officeHandling.updateOffice(officeUpdate);
        }
        return null;
    }
}
