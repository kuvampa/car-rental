package carrental.controller.command;

import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class UserAccidentCommand implements Command {
    private static final Logger logger = LogManager.getLogger(UserAccidentCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AccidentHandling accidentHandling = new AccidentHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, accidentHandling, userHandling);
        } else {
            return getPart(req, session, accidentHandling, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, AccidentHandling accidentHandling, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String paid = req.getParameter("paid");
            if (paid != null) {
                accidentHandling.updateAccidentStatus(Integer.parseInt(paid), "paid");
            }
            req.setAttribute("file", "userAccident");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/userAccident");
        } catch (Exception e) {
            return generateErrorPage("can't enter to user accidents", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, AccidentHandling accidentHandling, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("accidents", accidentHandling.findAllAccidents());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("rents", rentHandling.findAllRents());
            req.setAttribute("cars", carHandling.findAllCars());
            req.setAttribute("verifications", verificationHandling.findAllVerifications());
            return "/userAccident.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to user accidents", logger, req);
        }
    }
}
