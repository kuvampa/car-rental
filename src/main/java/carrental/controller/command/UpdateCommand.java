package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.User;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class UpdateCommand implements Command {
    private static final Logger logger = LogManager.getLogger(UpdateCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling);
        } else {
            return getPart(req, session, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String newImage = req.getParameter("newImage");
            if (newImage != null) {

                boolean empty = true;
                for (Part part : req.getParts()) {
                    if (part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                        empty = false;
                    }
                    if (!empty) {
                        int userId = userHandling.findUserByName(newImage).getIdUser();
                        part.write(session.getAttribute("directoryPath") + "users/" + userId + ".jpg");
                        logger.debug("new photo for {}", newImage);
                    }
                }
                if (empty) {
                    return "/app/update";
                }
            }
            req.setAttribute("file", "update");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/update");
        } catch (DBException e) {
            return generateErrorPage("can't update your user", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            Object loginUpdate = session.getAttribute("login");
            String loginUp = loginUpdate.toString();
            User userUpdate = userHandling.findUserByName(loginUp);
            req.setAttribute("getLogin", userUpdate.getLogin());
            req.setAttribute("getPassword", userUpdate.getPassword());
            req.setAttribute("getFullName", userUpdate.getFullName());
            req.setAttribute("getEmail", userUpdate.getEmail());
            req.setAttribute("getTelephoneNumber", userUpdate.getTelephoneNumber());
            return "/update.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't update your account", logger, req);
        }
    }
}
