package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.*;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

public class VerificationCommand implements Command {
    private static final Logger logger = LogManager.getLogger(VerificationCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session);
        } else {
            return getPart(req, session);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            double price = 0.0;
            Object login = session.getAttribute("login");
            User user = userHandling.findUserByName(login.toString());
            Object carPlate = session.getAttribute("car");
            Car car = carHandling.findCarByPlate(carPlate.toString());
            List<Rental> rentalsByRec = rentHandling.findRentsByCarIdSortByReturnDate(carHandling.findCarByPlate(carPlate.toString()).getIdCar());
            LocalDateTime dateTimePick = LocalDateTime.parse(req.getParameter("date_time_pick"));
            LocalDateTime dateTimeReturn = LocalDateTime.parse(req.getParameter("date_time_return"));
            req.setAttribute("file", "verification");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            if (message != null) {
                return message;
            }
            return createNewVerification(req, price, user, car, rentalsByRec, dateTimePick, dateTimeReturn);
        } catch (DBException | ServletException | IOException e) {
            return generateErrorPage("can't create verification process", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        OfficeHandling officeHandling = new OfficeHandling();
        String action = req.getPathInfo();
        try {
            navbarShell(req, session, officeHandling, action);
            return "/verification.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't withdraw verification page", logger, req);
        }
    }


    private String createNewVerification(HttpServletRequest req, double price, User user, Car car, List<Rental> rentalsByRec, LocalDateTime dateTimePick, LocalDateTime dateTimeReturn) throws ServletException, IOException, DBException {
        if (Validator.Data.dataCheck(dateTimePick, dateTimeReturn, rentalsByRec)) {
            VerificationHandling verificationHandling = new VerificationHandling();
            AdditionalServiceHandling additionalServiceHandling = new AdditionalServiceHandling();
            RentHandling rentHandling = new RentHandling();
            ServiceRentHandling serviceRentHandling = new ServiceRentHandling();
            String returnP = req.getParameter("Return");
            price += car.getPrice();
            price += price * ChronoUnit.DAYS.between(dateTimePick, dateTimeReturn);
            rentHandling.insertRent(new Rental(user.getIdUser(), car.getIdCar(), 1, price, returnP, dateTimePick, dateTimeReturn));
            verificationHandling.insertVerification(new Verification(rentHandling.findRentByDate(dateTimePick, dateTimeReturn).getIdRental(), "not verified"));
            for (AdditionalService additionalService : additionalServiceHandling.findAllServices()) {
                String serviceNameVerification = req.getParameter(additionalService.getName());
                if (Objects.equals(serviceNameVerification, "on")) {
                    int rentalIdVerification = rentHandling.findRentByDate(dateTimePick, dateTimeReturn).getIdRental();
                    price += additionalService.getPrice();
                    rentHandling.updateRent(new Rental(rentalIdVerification, user.getIdUser(), car.getIdCar(), 1, price, returnP, dateTimePick, dateTimeReturn));
                    serviceRentHandling.insertServiceRent(new ServiceRent(rentalIdVerification, additionalService.getIdService()));
                }
            }
            return "/app/verification";
        } else {
            return generateErrorPage("can't pass validation of dates", logger, req);
        }
    }
}
