package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class LogCommand implements Command {
    private static final Logger logger = LogManager.getLogger(LogCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session);
        } else {
            return getPart(req, session);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "log");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/log");
        } catch (DBException e) {
            return generateErrorPage("can't enter to log page", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            return "/log.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't enter to login process", logger, req);
        }
    }
}
