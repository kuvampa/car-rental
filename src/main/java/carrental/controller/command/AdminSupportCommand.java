package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.UserSupport;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminSupportCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminSupportCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userSupportHandling);
        } else {
            return getPart(req, session, userSupportHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserSupportHandling userSupportHandling) throws ServletException, IOException {
        String check = "check";
        UserHandling userHandling = new UserHandling();
        AdminHandling adminHandling = new AdminHandling();
        try {
            if (req.getParameter(check) != null) {
                userSupportHandling.updateUserSupport(new UserSupport(Integer.parseInt(req.getParameter(check)), userSupportHandling.findUserSupportById(Integer.parseInt(req.getParameter(check))).getIdUser(), adminHandling.findAdminByName(session.getAttribute("admin").toString()).getIdAdmin(), userSupportHandling.findUserSupportById(Integer.parseInt(req.getParameter(check))).getMessage(), "checked"));
            }
            req.setAttribute("file", "adminSupport");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminSupport");
        } catch (DBException e) {
            return generateErrorPage("can't enter to admin support page ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserSupportHandling userSupportHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("userSupports", userSupportHandling.findAllUserSupports());
            return "/adminSupport.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't do a admin support ", logger, req);
        }
    }
}
