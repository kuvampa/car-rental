package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.InsurancePackage;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminInsurance implements Command {
    private static final Logger logger = LogManager.getLogger(AdminInsurance.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        InsuranseHandling insuranseHandling = new InsuranseHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, insuranseHandling);
        } else {
            return getPart(req, session, insuranseHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, InsuranseHandling insuranseHandling) throws ServletException, IOException {
        String available = "available";
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String newCancel = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            String updateInsurance = req.getParameter("Update-Insurance");
            String deleteInsurance = req.getParameter("Delete-Insurance");
            String acceptUpdate = req.getParameter("accept");
            String cancel = req.getParameter("cancel");
            if (newCancel != null) {
                req.removeAttribute("newInsurance");
            }
            String acceptInsertString = acceptInsert(req, acceptInsert, insuranseHandling, available);
            if (acceptInsertString != null) {
                return acceptInsertString;
            }
            String acceptUpdateString = acceptUpdate(req, acceptUpdate, insuranseHandling, available);
            if (acceptUpdateString != null) {
                return acceptUpdateString;
            }
            if (updateInsurance != null) {
                insuranseHandling.updateInsuranceStatus(updateInsurance, "updated");
            }
            if (deleteInsurance != null) {
                insuranseHandling.deleteInsurance(insuranseHandling.findInsuranceByName(deleteInsurance).getIdInsurance());
            }
            if (cancel != null) {
                insuranseHandling.updateInsuranceStatus(cancel, available);
            }
            req.setAttribute("file", "adminInsurance");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminInsurance");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin insurance ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, InsuranseHandling insuranseHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertInsurance = req.getParameter("InsertInsurance");
            if (insertInsurance != null) {
                req.setAttribute("newInsurance", "insert");
            }
            req.setAttribute("insurances", insuranseHandling.findAllInsurances());
            return "/adminInsurance.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin insurance", logger, req);
        }
    }

    private String acceptInsert(HttpServletRequest req, String acceptInsert, InsuranseHandling insuranseHandling, String available) throws ServletException, IOException, DBException {
        if (acceptInsert != null) {
            String insuranceName = req.getParameter("new_insurance_name");
            String insuranceDesc = req.getParameter("new_insurance_desc");
            InsurancePackage insurancePackageInsert = new InsurancePackage(insuranceName, insuranceDesc, available);
            if (!Validator.Insert.insuranceCreate(insurancePackageInsert)) {
                return generateErrorPage("can't pass create new insurance validation", logger, req);
            }
            insuranseHandling.insertInsurance(insurancePackageInsert);
        }
        return null;
    }

    private String acceptUpdate(HttpServletRequest req, String acceptUpdate, InsuranseHandling insuranseHandling, String available) throws ServletException, IOException, DBException {
        if (acceptUpdate != null) {
            String insuranceName = req.getParameter("insurance_name");
            String insuranceDesc = req.getParameter("insurance_desc");
            InsurancePackage insuranceUp = insuranseHandling.findInsuranceByName(acceptUpdate);
            InsurancePackage insurancePackageUpdate = new InsurancePackage(insuranceUp.getIdInsurance(), insuranceName, insuranceDesc, available);
            if (!Validator.Update.insuranceUpdate(insurancePackageUpdate)) {
                return generateErrorPage("can't pass update insurance validation", logger, req);
            }
            insuranseHandling.updateInsurance(insurancePackageUpdate);
        }
        return null;
    }
}
