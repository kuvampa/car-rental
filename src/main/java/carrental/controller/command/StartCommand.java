package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.CarHandling;
import carrental.handling.OfficeHandling;
import carrental.handling.UserHandling;
import carrental.handling.UserSupportHandling;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class StartCommand implements Command {
    private static final Logger logger = LogManager.getLogger(StartCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        if (method.equals("get")) {
            return getPart(req, session, userHandling);
        } else {
            return postPart(req, session, userHandling);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        int min = 0;
        int max = 99999;
        String sort = "";
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        CarHandling carHandling = new CarHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            Object statusStart = session.getAttribute("login");
            req.setAttribute("carManufacturer", carHandling.findUniqueManufacture());
            req.setAttribute("carColor", carHandling.findUniqueColor());
            req.setAttribute("carType", carHandling.findUniqueType());
            req.setAttribute("carFuelType", carHandling.findUniqueFuelType());
            req.setAttribute("carCapacity", carHandling.findUniqueCapacity());
            req.setAttribute("carRating", carHandling.findUniqueRating());
            if (req.getParameter("min") != null && !Objects.equals(req.getParameter("min"), "")) {
                min = Integer.parseInt(req.getParameter("min"));
            }
            if ((req.getParameter("max") != null) && !Objects.equals(req.getParameter("max"), "")) {
                max = Integer.parseInt(req.getParameter("max"));
            }
            if (req.getParameter("sort") != null) {
                sort = req.getParameter("sort");
            }
            req.setAttribute("cars", carHandling.searchCars(req.getParameter("name"), carHandling.searchType(req), carHandling.searchFuelType(req), min, max, carHandling.searchRating(req), carHandling.searchManufactures(req), carHandling.searchColor(req), carHandling.searchCapacity(req), sort, req, session));
            if (statusStart != null) {
                req.setAttribute("blockStatus", userHandling.findUserByName(statusStart.toString()).getStatus());
            }
            return "/start.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to start page", logger, req);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "start");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/start");
        } catch (DBException e) {
            return generateErrorPage("can't enter to start page", logger, req);
        }
    }
}
