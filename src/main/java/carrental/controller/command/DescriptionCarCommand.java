package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.*;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class DescriptionCarCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DescriptionCarCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session);
        } else {
            return getPart(req, session);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "descriptionCar");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/descriptionCar");
        } catch (DBException e) {
            return generateErrorPage("can't enter to description page ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session) throws ServletException, IOException {
        String action = req.getPathInfo();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        TermsHandling termsHandling = new TermsHandling();
        InsuranseHandling insuranseHandling = new InsuranseHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String carPlate = req.getParameter("CarPlateDesc");
            if (carPlate == null) {
                carPlate = session.getAttribute("car").toString();
            }
            session.setAttribute("car", carPlate);
            Car carDescr = carHandling.findCarByPlate(carPlate);
            TermsOfUse termsOfUseDescr = termsHandling.findTermById(carDescr.getIdTermsPackage());
            InsurancePackage insurancePackageDescr = insuranseHandling.findInsuranceById(carDescr.getIdInsurance());
            Office officeDescr = officeHandling.findOfficebyId(carDescr.getIdOffice());
            req.setAttribute("officeDescr", officeDescr);
            req.setAttribute("termsOfUseDescr", termsOfUseDescr);
            req.setAttribute("carDescr", carDescr);
            req.setAttribute("insurancePackageDescr", insurancePackageDescr);
            return "/descriptionCar.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't look to description car ", logger, req);
        }
    }
}
