package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.TermsOfUse;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminTermsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminTermsCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        TermsHandling termsHandling = new TermsHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, termsHandling);
        } else {
            return getPart(req, session, termsHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, TermsHandling termsHandling) throws ServletException, IOException {
        String available = "available";
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String newCancel = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            String updateTerm = req.getParameter("Update-Term");
            String deleteTerm = req.getParameter("Delete-Term");
            String acceptUpdate = req.getParameter("accept");
            String cancel = req.getParameter("cancel");
            if (newCancel != null) {
                req.removeAttribute("newTerm");
            }
            String acceptInsertString = acceptInsert(req, acceptInsert, termsHandling, available);
            if (acceptInsertString != null) {
                return acceptInsertString;
            }
            String acceptUpdateString = acceptUpdate(req, acceptUpdate, termsHandling, available);
            if (acceptUpdateString != null) {
                return acceptUpdateString;
            }
            if (updateTerm != null) {
                termsHandling.updateTermStatus(updateTerm, "updated");
            }
            if (deleteTerm != null) {
                termsHandling.deleteTerm(termsHandling.findTermByName(deleteTerm).getIdTermsPackage());
            }
            if (cancel != null) {
                termsHandling.updateTermStatus(cancel, available);
            }
            req.setAttribute("file", "adminTerms");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminTerms");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin terms ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, TermsHandling termsHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertTerm = req.getParameter("Insert-Term");
            if (insertTerm != null) {
                req.setAttribute("newTerm", "insert");
            }
            req.setAttribute("terms", termsHandling.findAllTerms());
            return "/adminTerms.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin terms ", logger, req);
        }
    }

    private String acceptInsert(HttpServletRequest req, String acceptInsert, TermsHandling termsHandling, String available) throws ServletException, IOException, DBException {
        if (acceptInsert != null) {
            String newTermName = req.getParameter("new_term_name");
            String newTermFuel = req.getParameter("new_term_fuel");
            String newTermLimit = req.getParameter("new_term_Limit");
            TermsOfUse termsOfUseInsert = new TermsOfUse(newTermName, newTermFuel, newTermLimit, available);
            if (!Validator.Insert.termsCreate(termsOfUseInsert)) {
                return generateErrorPage("can't pass create new terms validation", logger, req);

            }
            termsHandling.insertTerm(termsOfUseInsert);
        }
        return null;
    }

    private String acceptUpdate(HttpServletRequest req, String acceptUpdate, TermsHandling termsHandling, String available) throws ServletException, IOException, DBException {
        if (acceptUpdate != null) {
            String termName = req.getParameter("term_name");
            String termFuel = req.getParameter("term_fuel");
            String termLimit = req.getParameter("term_Limit");
            TermsOfUse termsOfUse = termsHandling.findTermByName(acceptUpdate);
            TermsOfUse termsOfUseUpdate = new TermsOfUse(termsOfUse.getIdTermsPackage(), termName, termFuel, termLimit, available);
            if (!Validator.Update.termsUpdate(termsOfUseUpdate)) {
                return generateErrorPage("can't pass update new terms validation", logger, req);

            }
            termsHandling.updateTerms(termsOfUseUpdate);
        }
        return null;
    }
}
