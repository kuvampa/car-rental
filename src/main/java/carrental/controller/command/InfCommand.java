package carrental.controller.command;

import carrental.controller.util.EncryptPassword;
import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.User;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class InfCommand implements Command {
    private static final Logger logger = LogManager.getLogger(InfCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        try {
            req.setAttribute("file", "inf");
            String fullNameReg = req.getParameter("full_name");
            String telephoneNumberReg = req.getParameter("telephone_number");
            String emailReg = req.getParameter("email");
            String loginReg = req.getParameter("login");
            String passwordReg = req.getParameter("password");
            if (!Validator.Update.userPassword(passwordReg)) {
                return generateErrorPage("can't pass user create validation", logger, req);
            }
            User user = new User(fullNameReg, telephoneNumberReg, emailReg, loginReg, EncryptPassword.encrypt(passwordReg), 0);
            if (!Validator.Insert.userCreate(user)) {
                return generateErrorPage("can't pass user create validation", logger, req);
            }
            userHandling.insertUser(user);
            session.setAttribute("login", loginReg);
        } catch (DBException | NoSuchAlgorithmException e) {
            return generateErrorPage("can't enter to filling information process ", logger, req);
        }
        return "/app/start";
    }
}
