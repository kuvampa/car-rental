package carrental.controller.command;

import carrental.controller.util.EncryptPassword;
import carrental.db.DBException;
import carrental.db.entity.Admin;
import carrental.db.entity.Manager;
import carrental.db.entity.User;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class FindCommand implements Command {
    private static final Logger logger = LogManager.getLogger(FindCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AdminHandling adminHandling = new AdminHandling();
        ManagerHandling managerHandling = new ManagerHandling();
        HttpSession session = req.getSession();
        try {
            String loginFind = req.getParameter("login");
            String password = req.getParameter("password");
            User ourUser = userHandling.findUserByName(loginFind);
            Admin admin = adminHandling.findAdminByName(loginFind);
            Manager manager = managerHandling.findManagerByName(loginFind);
            if (ourUser.getLogin() == null) {
                return generateErrorPage("Your login or password is incorrect", logger, req);
            }
            if (Objects.equals(ourUser.getPassword(), EncryptPassword.encrypt(password)) && password != null) {
                session.setAttribute("login", loginFind);
                if (admin.getLogin() != null) {
                    session.setAttribute("admin", loginFind);
                }
                if (manager.getLogin() != null) {
                    session.setAttribute("manager", loginFind);
                }
            } else {
                return generateErrorPage("Your password or login is incorrect", logger, req);
            }
            return "/app/start";
        } catch (DBException | NoSuchAlgorithmException e) {
            return generateErrorPage("can't find your account ", logger, req);
        }
    }
}
