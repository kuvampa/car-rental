package carrental.controller.command;

import carrental.db.entity.Verification;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class UserRentCommand implements Command {
    private static final Logger logger = LogManager.getLogger(UserRentCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        RentHandling rentHandling = new RentHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling, rentHandling, verificationHandling);
        } else {
            return getPart(req, session, verificationHandling, userHandling, rentHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling, RentHandling rentHandling, VerificationHandling verificationHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String cancel = req.getParameter("cancel");
            String paid = req.getParameter("paid");
            String delete = req.getParameter("delete");
            String returnCar = req.getParameter("return");
            if (paid != null) {
                verificationHandling.updateVerificationStatus(Integer.parseInt(paid), "paid");
            }
            if (cancel != null) {
                verificationHandling.updateVerificationStatus(Integer.parseInt(cancel), "cancel");
            }
            if (returnCar != null) {
                verificationHandling.updateVerificationStatus(Integer.parseInt(returnCar), "returned");
            }
            if (delete != null) {
                Verification verification = verificationHandling.findVerificationById(Integer.parseInt(delete));
                verificationHandling.deleteVerification(Integer.parseInt(delete));
                rentHandling.deleteRent(verification.getIdRental());
            }
            req.setAttribute("file", "userRent");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/userRent");
        } catch (Exception e) {
            return generateErrorPage("can't enter to user rent", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, VerificationHandling verificationHandling, UserHandling userHandling, RentHandling rentHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("verifications", verificationHandling.findAllVerifications());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("rents", rentHandling.findAllRents());
            req.setAttribute("cars", carHandling.findAllCars());
            return "/userRent.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to your rent", logger, req);
        }
    }
}
