package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class QuitCommand implements Command{
    private static final Logger logger = LogManager.getLogger(QuitCommand.class);
    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        OfficeHandling officeHandling = new OfficeHandling();
        HttpSession session = req.getSession();
        String action = req.getPathInfo();
        try {
            navbarShell(req, session, officeHandling, action);
            session.removeAttribute("login");
            session.removeAttribute("admin");
            session.removeAttribute("manager");
           return  "/app/start";
        } catch (DBException e) {
           return generateErrorPage("can't quit from account process",logger, req);
        }
    }
}
