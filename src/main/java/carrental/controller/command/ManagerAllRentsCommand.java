package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class ManagerAllRentsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ManagerAllRentsCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling);
        } else {
            return getPart(req, session, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "managerAllRents");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/managerAllRents");
        } catch (DBException e) {
            return generateErrorPage("can't enter to manager all rents page ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("verifications", verificationHandling.findAllVerifications());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("rents", rentHandling.findAllRents());
            req.setAttribute("cars", carHandling.findAllCars());
            return "/managerAllRents.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't look to all rents ", logger, req);
        }
    }
}
