package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class ErrorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ErrorCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String error = "error";
        if (method.equals("post")) {
            return postPart(req, session, error);
        } else {
            return getPart(req, session, error);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, String error) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", error);
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/error");
        } catch (DBException e) {
            return generateErrorPage("can't enter to error page ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, String errorS) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String error = (String) session.getAttribute(errorS);
            session.removeAttribute(errorS);
            req.setAttribute(errorS, error);
            return "/error.jsp";
        } catch (DBException e) {
            return generateErrorPage("Error on error page", logger, req);
        }
    }
}
