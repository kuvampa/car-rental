package carrental.controller.command;

import carrental.controller.util.EncryptPassword;
import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.Admin;
import carrental.db.entity.Manager;
import carrental.db.entity.User;
import carrental.handling.AdminHandling;
import carrental.handling.ManagerHandling;
import carrental.handling.UserHandling;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Update2Command implements Command {
    private static final Logger logger = LogManager.getLogger(Update2Command.class);

    @Override
    public String execute(HttpServletRequest req,  String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AdminHandling adminHandling = new AdminHandling();
        ManagerHandling managerHandling = new ManagerHandling();
        HttpSession session = req.getSession();
        String admin = "admin";
        String manager = "manager";
        String loginS = "login";
        try {
            String oldLogin = session.getAttribute(loginS).toString();
            String fullName = req.getParameter("full_name");
            String telephoneNumber = req.getParameter("telephone_number");
            String email = req.getParameter("email");
            String login = req.getParameter(loginS);
            String oldPassword = req.getParameter("password");
            String newPassword = req.getParameter("newPassword");
            User user;
            if (oldPassword.isEmpty() || newPassword.isEmpty() || !EncryptPassword.encrypt(oldPassword).equals(userHandling.findUserByName(oldLogin).getPassword())) {
                if (!EncryptPassword.encrypt(oldPassword).equals(userHandling.findUserByName(oldLogin).getPassword()) && !oldPassword.isEmpty() && !newPassword.isEmpty()) {
                    return generateErrorPage("wrong password", logger, req);
                }
                user = new User(userHandling.findUserByName(oldLogin).getIdUser(), fullName, telephoneNumber, email, login, userHandling.findUserByName(oldLogin).getPassword());
            } else {
                if (!Validator.Update.userPassword(newPassword)) {
                    return generateErrorPage("can't pass user update validation", logger, req);
                }
                user = new User(userHandling.findUserByName(oldLogin).getIdUser(), fullName, telephoneNumber, email, login, EncryptPassword.encrypt(newPassword));
            }
            if (!Validator.Update.userUpdate(user)) {
                return generateErrorPage("can't pass user update validation", logger, req);
            }
            userHandling.updateUser(user);
            session.removeAttribute(loginS);
            session.setAttribute(loginS, login);
            if (session.getAttribute(admin) != null) {
                adminHandling.updateAdmin(new Admin(userHandling.findUserByName(oldLogin).getIdUser(), fullName, telephoneNumber, email, login, user.getPassword()));
                session.removeAttribute(admin);
                session.setAttribute(admin, login);
            }
            if (session.getAttribute(manager) != null) {
                managerHandling.updateManager(new Manager(userHandling.findUserByName(oldLogin).getIdUser(), fullName, telephoneNumber, email, login, user.getPassword()));
                session.removeAttribute(manager);
                session.setAttribute(manager, login);
            }
        } catch (DBException | NoSuchAlgorithmException e) {
            return generateErrorPage("can't enter to confirm update process", logger, req);
        }
        return "/app/start";
    }
}
