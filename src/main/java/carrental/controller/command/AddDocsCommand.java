package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AddDocsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AddDocsCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling);
        } else {
            return getPart(req, session, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            req.setAttribute("file", "addDocs");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/addDocs");
        } catch (DBException e) {
            return generateErrorPage("can't add documents ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("userDoc", userHandling.findUserByName((String) session.getAttribute("login")).getDocuments());
            return "/addDocs.jsp";
        } catch (DBException e) {
            return generateErrorPage("can't add documents ", logger, req);
        }
    }
}
