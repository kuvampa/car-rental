package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.AdditionalService;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminServiceCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminServiceCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        AdditionalServiceHandling additionalServiceHandling = new AdditionalServiceHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, additionalServiceHandling);
        } else {
            return getPart(req, session, additionalServiceHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, AdditionalServiceHandling additionalServiceHandling) throws ServletException, IOException {
        String available = "available";
        UserHandling userHandling = new UserHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String newCancel = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            String updateService = req.getParameter("Update-Service");
            String deleteService = req.getParameter("Delete-Service");
            String acceptUpdate = req.getParameter("accept");
            String cancel = req.getParameter("cancel");
            if (newCancel != null) {
                req.removeAttribute("newService");
            }
            String acceptInsertString = acceptInsert(req, acceptInsert, additionalServiceHandling, available);
            if (acceptInsertString != null) {
                return acceptInsertString;
            }
            String acceptUpdateString = acceptUpdate(req, acceptUpdate, additionalServiceHandling, available);
            if (acceptUpdateString != null) {
                return acceptUpdateString;
            }
            if (updateService != null) {
                additionalServiceHandling.updateServiceStatus(updateService, "updated");
            }
            if (deleteService != null) {
                additionalServiceHandling.deleteService(additionalServiceHandling.findServiceByName(deleteService).getIdService());
            }
            if (cancel != null) {
                additionalServiceHandling.updateServiceStatus(cancel, available);
            }
            req.setAttribute("file", "adminService");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminService");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin service ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, AdditionalServiceHandling additionalServiceHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertService = req.getParameter("InsertService");
            if (insertService != null) {
                req.setAttribute("newService", "insert");
            }
            req.setAttribute("services", additionalServiceHandling.findAllServices());
            return "/adminService.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin service ", logger, req);
        }
    }

    private String acceptInsert(HttpServletRequest req, String acceptInsert, AdditionalServiceHandling additionalServiceHandling, String available) throws ServletException, IOException, DBException {
        if (acceptInsert != null) {
            String serviceName = req.getParameter("new_service_name");
            String serviceDesc = req.getParameter("new_service_desc");
            String servicePrice = req.getParameter("new_service_price");
            double serviceP = 0;
            if (servicePrice != null) {
                serviceP = Double.parseDouble(servicePrice);
            }
            AdditionalService additionalServiceInsert = new AdditionalService(serviceName, serviceP, serviceDesc, available);
            if (!Validator.Insert.serviceCreate(additionalServiceInsert)) {
                return generateErrorPage("can't pass create new service validation", logger, req);
            }
            additionalServiceHandling.insertService(additionalServiceInsert);
        }
        return null;
    }

    private String acceptUpdate(HttpServletRequest req, String acceptUpdate, AdditionalServiceHandling additionalServiceHandling, String available) throws ServletException, IOException, DBException {
        if (acceptUpdate != null) {
            String serviceName = req.getParameter("service_name");
            String serviceDesc = req.getParameter("service_desc");
            String servicePrice = req.getParameter("service_price");
            double serviceP = 0;
            if (servicePrice != null) {
                serviceP = Double.parseDouble(servicePrice);
            }
            AdditionalService serviceUp = additionalServiceHandling.findServiceByName(acceptUpdate);
            AdditionalService additionalServiceUpdate = new AdditionalService(serviceUp.getIdService(), serviceName, serviceP, serviceDesc, available);
            if (!Validator.Update.serviceUpdate(additionalServiceUpdate)) {
                return generateErrorPage("can't pass update service validation", logger, req);
            }
            additionalServiceHandling.updateService(additionalServiceUpdate);
        }
        return null;
    }
}
