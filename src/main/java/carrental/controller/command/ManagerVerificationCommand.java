package carrental.controller.command;

import carrental.controller.util.Validator;
import carrental.db.entity.Verification;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class ManagerVerificationCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ManagerVerificationCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, userHandling, verificationHandling);
        } else {
            return getPart(req, session, userHandling, verificationHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, UserHandling userHandling, VerificationHandling verificationHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        ManagerHandling managerHandling = new ManagerHandling();
        try {
            String accept = req.getParameter("accept");
            String acceptButton = req.getParameter("acceptButton");
            String reject = req.getParameter("reject");
            String rejectButton = req.getParameter("rejectButton");
            if (acceptButton != null) {
                String comment = req.getParameter("comment");
                verificationHandling.updateVerifications(new Verification(Integer.parseInt(accept), verificationHandling.findVerificationById(Integer.parseInt(accept)).getIdRental(), managerHandling.findManagerByName((String) session.getAttribute("manager")).getIdManager(), "accept", comment));
            }
            if (rejectButton != null) {
                String comment = req.getParameter("comment");
                Verification verificationComment = new Verification(Integer.parseInt(reject), verificationHandling.findVerificationById(Integer.parseInt(reject)).getIdRental(), managerHandling.findManagerByName((String) session.getAttribute("manager")).getIdManager(), "reject", comment);
                if (!Validator.Update.verificationComment(verificationComment)) {
                    return generateErrorPage("can't pass create message validation", logger, req);
                }
                verificationHandling.updateVerifications(verificationComment);
            }
            req.setAttribute("file", "managerVerification");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/managerVerification");
        } catch (Exception e) {
            return generateErrorPage("can't enter to manager verifications ", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, UserHandling userHandling, VerificationHandling verificationHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("verifications", verificationHandling.findAllVerifications());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("rents", rentHandling.findAllRents());
            req.setAttribute("cars", carHandling.findAllCars());
            return "/managerVerification.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't enter to all verifications ", logger, req);
        }
    }
}
