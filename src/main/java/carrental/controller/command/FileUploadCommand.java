package carrental.controller.command;

import carrental.db.DBException;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUploadCommand implements Command {
    private static final Logger logger = LogManager.getLogger(FileUploadCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AdminHandling adminHandling = new AdminHandling();
        ManagerHandling managerHandling = new ManagerHandling();
        HttpSession session = req.getSession();
        try {
            String login = session.getAttribute("login").toString();
            if (req.getParameter("Docs") != null) {
                String updateDocs = updateDocs(req, session, login, userHandling, adminHandling, managerHandling);
                if (updateDocs != null) {
                    return updateDocs;
                }
            }
            deleteDocs(req, session, login, userHandling, adminHandling, managerHandling);
            return "/app/addDocs";
        } catch (DBException e) {
            return generateErrorPage("can't upload file ", logger, req);
        }
    }

    private String updateDocs(HttpServletRequest req, HttpSession session, String login, UserHandling userHandling, AdminHandling adminHandling, ManagerHandling managerHandling) throws DBException, ServletException, IOException {
        boolean empty = true;
        for (Part part : req.getParts()) {
            if (part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                empty = false;
            }
            if (!empty) {
                part.write(session.getAttribute("directoryPath") + "docs/" + login + ".jpg");
                logger.debug("new docs for {}", login);
            }
        }
        if (empty) {
            return "/app/addDocs";
        }
        userHandling.updateUserDocs(login, login + ".jpg");
        if (session.getAttribute("admin") != null) {
            adminHandling.updateAdminDocs(login, login + ".jpg");
        }
        if (session.getAttribute("manager") != null) {
            managerHandling.updateManagerDocs(login, login + ".jpg");
        }
        session.setAttribute("docs", "active");
        return null;
    }

    private void deleteDocs(HttpServletRequest req, HttpSession session, String login, UserHandling userHandling, AdminHandling adminHandling, ManagerHandling managerHandling) throws IOException, DBException {
        if (req.getParameter("deleteDocuments") != null) {
            Files.delete(Path.of(session.getAttribute("directoryPath") + "docs/" + login + ".jpg"));
            userHandling.updateUserDocs(login, null);
            if (session.getAttribute("admin") != null) {
                adminHandling.updateAdminDocs(login, null);
            }
            if (session.getAttribute("manager") != null) {
                managerHandling.updateManagerDocs(login, null);
            }
            session.removeAttribute("docs");
        }
    }
}
