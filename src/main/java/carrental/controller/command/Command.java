package carrental.controller.command;

import carrental.controller.util.Currency;
import carrental.controller.util.Validator;
import carrental.db.DBException;
import carrental.db.entity.UserSupport;
import carrental.handling.OfficeHandling;
import carrental.handling.UserHandling;
import carrental.handling.UserSupportHandling;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
/**
 * Command is used to divide the code into classes with one main method execute
 * @author Novikov Artem
 * @version 1.1
 */
public interface Command {
    String execute(HttpServletRequest req, String method) throws ServletException, IOException;

    /**
     * @generateErrorPage used as a default method in the interface to create error pages
     */
    default String generateErrorPage(String error, Logger logger, HttpServletRequest req) throws ServletException, IOException {
        logger.error("Error {}", error);
        HttpSession session = req.getSession(false);
        if (session != null)
            session.setAttribute("error", error);
        return "/app/error";
    }
    /**
     * @navbarShell used as a default method in the interface to operate all standard navbar functions
     */
    default void navbarShell(HttpServletRequest req, HttpSession session, OfficeHandling officeHandling, String action) throws DBException, MalformedURLException {
        UserHandling userHandling = new UserHandling();
        req.setAttribute("file", action.replace("/", ""));
        req.setAttribute("offices", officeHandling.findAllOffices());
        if (!action.equals("quit")) {
            req.setAttribute("loginStart", session.getAttribute("login"));
            req.setAttribute("managerStart", session.getAttribute("manager"));
            req.setAttribute("adminStart", session.getAttribute("admin"));
        }
        String officeNameStart = req.getParameter("officeName");
        if (officeNameStart != null) {
            req.setAttribute("officeTelephone", officeHandling.findOfficeByName(officeNameStart).getTelephoneNumber());
            req.setAttribute("officeName", officeNameStart);
        }
        String language = req.getParameter("language");
        if (language == null) {
            if(session.getAttribute("lang")==null) {
                session.setAttribute("lang", "en");
            }
        } else {
            switch (language) {
                case "UK":
                    session.setAttribute("lang", "uk");
                    break;
                case "EN":
                    session.setAttribute("lang", "en");
                    break;
                default:
                    session.setAttribute("lang", "en");
                    break;
            }
        }
        int userId = userHandling.findUserByName((String) session.getAttribute("login")).getIdUser();
        File file = new File(session.getAttribute("directoryPath") + "users/" + userId + ".jpg");
        if (file.exists()) {
            req.setAttribute("imageUser", userId);
        } else {
            req.setAttribute("imageUser", "User");
        }
        Currency.currencyUpdate(req);
    }
    /**
     * @navbarShell used as a default method in the interface to create messages to administrator
     */
    default String message(HttpServletRequest req, HttpSession session, UserSupportHandling userSupportHandling, UserHandling userHandling, Logger logger) throws DBException, ServletException, IOException {
        if (req.getParameter("message") != null) {
            UserSupport userSupport = new UserSupport(userHandling.findUserByName((String) session.getAttribute("login")).getIdUser(), req.getParameter("message"), "unchecked");
            if (!Validator.Insert.userSupportCreate(userSupport)) {
                return generateErrorPage("can't pass create message validation", logger, req);
            }
            userSupportHandling.insertUserSupport(userSupport);
            logger.debug("new message [{}]", userSupport);
        }
        return null;
    }
}
