package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.*;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Objects;

public class AdminUsersCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminUsersCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AdminHandling adminHandling = new AdminHandling();
        ManagerHandling managerHandling = new ManagerHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, managerHandling, userHandling, adminHandling);
        } else {
            return getPart(req, session, managerHandling, userHandling, adminHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, ManagerHandling managerHandling, UserHandling userHandling, AdminHandling adminHandling) throws ServletException, IOException {
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        AccidentHandling accidentHandling = new AccidentHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        try {
            String userToAdmin = req.getParameter("User-to-Admin");
            String userToManager = req.getParameter("User-to-Manager");
            String userBlock = req.getParameter("User-Block");
            String unBlock = req.getParameter("User-Unblock");
            String adminRemoveAdmin = req.getParameter("Admin-Remove-Admin");
            String adminRemoveManager = req.getParameter("Admin-Remove-Manager");
            if (userToAdmin != null) {
                User userAdmin = userHandling.findUserByName(userToAdmin);
                adminHandling.insertAdmin(new Admin(userAdmin.getIdUser(),userAdmin.getLogin(), userAdmin.getPassword(), userAdmin.getFullName(), userAdmin.getTelephoneNumber(), userAdmin.getEmail(), userAdmin.getDocuments()));
            }
            if (userToManager != null) {
                User userManager = userHandling.findUserByName(userToManager);
                managerHandling.insertManager(new Manager(userManager.getIdUser(), userManager.getLogin(), userManager.getPassword(), userManager.getFullName(), userManager.getTelephoneNumber(), userManager.getEmail(), userManager.getDocuments()));
            }
            if (userBlock != null) {
                userHandling.updateUserStatus(userBlock, 1);
            }
            if (unBlock != null) {
                userHandling.updateUserStatus(unBlock, 0);
            }
            String adminRemoveAdminS = adminRemoveAdmin(req, session, adminRemoveAdmin, adminHandling, userSupportHandling);
            if (adminRemoveAdminS != null) {
                return adminRemoveAdminS;
            }
            String adminRemoveManagerS = adminRemoveManager(req, adminRemoveManager, managerHandling, accidentHandling, verificationHandling);
            if (adminRemoveManagerS != null) {
                return adminRemoveManagerS;
            }
            req.setAttribute("file", "adminUsers");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/adminUsers");
        } catch (Exception e) {
            return generateErrorPage("can't enter to admin users", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, ManagerHandling managerHandling, UserHandling userHandling, AdminHandling adminHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            req.setAttribute("managers", managerHandling.findAllManagers());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("admins", adminHandling.findAllAdmins());
        } catch (Exception e) {
            generateErrorPage("can't enter to admin users", logger, req);
        }
        return "/adminUsers.jsp";
    }

    private String adminRemoveAdmin(HttpServletRequest req, HttpSession session, String adminRemoveAdmin, AdminHandling adminHandling, UserSupportHandling userSupportHandling) throws DBException, ServletException, IOException {
        if (adminRemoveAdmin != null) {
            Admin admin = adminHandling.findAdminByName(adminRemoveAdmin);
            if (admin.getIdAdmin() == 1) {
                return generateErrorPage("you can't delete core admin", logger, req);
            }
            for (UserSupport userSupports : userSupportHandling.findAllUserSupports()) {
                if (userSupports.getIdAdmin() == admin.getIdAdmin()) {
                    userSupportHandling.updateUserSupportAdmin(userSupports.getIdUserSupport(), 1);
                }
            }
            if (adminRemoveAdmin.equals(session.getAttribute("admin").toString())) {
                session.removeAttribute("admin");
            }
            adminHandling.deleteByName(adminRemoveAdmin);
        }
        return null;
    }

    private String adminRemoveManager(HttpServletRequest req, String adminRemoveManager, ManagerHandling managerHandling, AccidentHandling accidentHandling, VerificationHandling verificationHandling) throws DBException, ServletException, IOException {
        if (adminRemoveManager != null) {
            Manager manager = managerHandling.findManagerByName(adminRemoveManager);
            if (manager.getIdManager() == 2) {
                return generateErrorPage("you can't delete core manager", logger, req);
            }
            for (Accident accidents : accidentHandling.findAllAccidents()) {
                if (accidents.getIdManager() == manager.getIdManager()) {
                    accidentHandling.updateAccidentManager(accidents.getIdAccident(), 2);
                }
            }
            for (Verification verification : verificationHandling.findAllVerifications()) {
                if (verification.getIdManager() == manager.getIdManager()) {
                    verificationHandling.updateVerificationManager(verification.getIdVerificationOfDocuments(), 2);
                }
            }
            managerHandling.deleteByName(adminRemoveManager);
        }
        return null;
    }

}

