package carrental.controller.command;

import carrental.db.DBException;
import carrental.db.entity.Accident;
import carrental.handling.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class ManagerAccidentCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ManagerAccidentCommand.class);

    @Override
    public String execute(HttpServletRequest req, String method) throws ServletException, IOException {
        UserHandling userHandling = new UserHandling();
        AccidentHandling accidentHandling = new AccidentHandling();
        HttpSession session = req.getSession();
        if (method.equals("post")) {
            return postPart(req, session, accidentHandling, userHandling);
        } else {
            return getPart(req, session, accidentHandling, userHandling);
        }
    }

    private String postPart(HttpServletRequest req, HttpSession session, AccidentHandling accidentHandling, UserHandling userHandling) throws ServletException, IOException {
        ManagerHandling managerHandling = new ManagerHandling();
        UserSupportHandling userSupportHandling = new UserSupportHandling();
        try {
            String cancel = req.getParameter("cancel");
            String newImage = req.getParameter("newImage");
            String delete = req.getParameter("delete");
            String rejectInsert = req.getParameter("new_cancel");
            String acceptInsert = req.getParameter("new_accept");
            if (cancel != null) {
                accidentHandling.deleteAccident(Integer.parseInt(cancel));
            }
            if (delete != null) {
                accidentHandling.deleteAccident(Integer.parseInt(delete));
                Files.delete(Path.of(session.getAttribute("directoryPath") + "accident/" + delete + ".jpg"));
            }
            if (rejectInsert != null) {
                req.removeAttribute("newAccident");
            }
            String newImageS = newImage(req, session, newImage, accidentHandling);
            if (newImageS != null) {
                return newImageS;
            }
            acceptInsert(req, acceptInsert, accidentHandling, managerHandling, session);
            req.setAttribute("file", "managerAccident");
            String message = message(req, session, userSupportHandling, userHandling, logger);
            return Objects.requireNonNullElse(message, "/app/managerAccident");
        } catch (Exception e) {
            return generateErrorPage("can't enter to manager accidents", logger, req);
        }
    }

    private String getPart(HttpServletRequest req, HttpSession session, AccidentHandling accidentHandling, UserHandling userHandling) throws ServletException, IOException {
        String action = req.getPathInfo();
        RentHandling rentHandling = new RentHandling();
        CarHandling carHandling = new CarHandling();
        VerificationHandling verificationHandling = new VerificationHandling();
        OfficeHandling officeHandling = new OfficeHandling();
        try {
            navbarShell(req, session, officeHandling, action);
            String insertAccident = req.getParameter("Insert-Accident");
            if (insertAccident != null) {
                req.setAttribute("newAccident", "insert");
            }
            req.setAttribute("accidents", accidentHandling.findAllAccidents());
            req.setAttribute("users", userHandling.findAllUsers());
            req.setAttribute("rents", rentHandling.findAllRents());
            req.setAttribute("cars", carHandling.findAllCars());
            req.setAttribute("verifications", verificationHandling.findAllVerifications());
            return "/managerAccident.jsp";
        } catch (Exception e) {
            return generateErrorPage("can't look to all accidents ", logger, req);
        }
    }

    private String newImage(HttpServletRequest req, HttpSession session, String newImage, AccidentHandling accidentHandling) throws ServletException, IOException, DBException {
        if (newImage != null) {
            Part filePartA = req.getPart("fileName");
            if (filePartA != null) {
                accidentHandling.updateAccidentStatus(Integer.parseInt(newImage), "not paid");
            }
            boolean empty = true;
            for (Part part : req.getParts()) {
                if (part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                    empty = false;
                }
                if (!empty) {
                    part.write(session.getAttribute("directoryPath") + "accident/" + newImage + ".jpg");
                    logger.debug("new photo for {}", newImage);
                }
            }
            if (empty) {
                return "/app/managerAccident";
            }
        }
        return null;
    }

    private void acceptInsert(HttpServletRequest req, String acceptInsert, AccidentHandling accidentHandling, ManagerHandling managerHandling, HttpSession session) throws DBException {
        if (acceptInsert != null) {
            String accidentComment = req.getParameter("new_comment");
            String accidentPriceS = req.getParameter("new_price");
            double accidentPrice = 0;
            if (accidentPriceS != null) {
                accidentPrice = Double.parseDouble(accidentPriceS);
            }
            int accidentRentId = Integer.parseInt(req.getParameter("RentId"));
            accidentHandling.insertAccident(new Accident(accidentRentId, managerHandling.findManagerByName(session.getAttribute("manager").toString()).getIdManager(), accidentPrice, accidentComment, "without photo"));
        }
    }
}
