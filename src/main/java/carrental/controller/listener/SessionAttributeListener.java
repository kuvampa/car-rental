package carrental.controller.listener;

import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionAttributeListener;
import jakarta.servlet.http.HttpSessionBindingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * SessionAttributeListener is the listener that we will use to display all of our session attributes in the console.
 * @author Novikov Artem
 * @version 1.1
 * @see WebListener
 */
@WebListener
public class SessionAttributeListener implements HttpSessionAttributeListener {
    private static final Logger logger = LogManager.getLogger(SessionAttributeListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Added attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Removed attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Replaced attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }
}
