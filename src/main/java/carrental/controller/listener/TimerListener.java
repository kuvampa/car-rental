package carrental.controller.listener;

import carrental.controller.util.Timer;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.annotation.WebServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * TimerListener the listener we will use to check orders hourly
 * @author Novikov Artem
 * @version 1.1
 * @see WebListener
 */
@WebListener
public class TimerListener implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(TimerListener.class);
    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        logger.info("TimerListener wake up and start");
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Timer(), 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        logger.debug("TimerListener goes to sleep");
        scheduler.shutdownNow();
    }
}
