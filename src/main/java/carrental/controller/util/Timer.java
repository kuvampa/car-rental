package carrental.controller.util;

import carrental.db.DBException;
import carrental.db.entity.Accident;
import carrental.db.entity.Rental;
import carrental.db.entity.Verification;
import carrental.handling.AccidentHandling;
import carrental.handling.RentHandling;
import carrental.handling.VerificationHandling;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
/**
 * Timer is used to writing a condition that will be executed when the filter calls
 * @author Novikov Artem
 * @version 1.1
 */
public class Timer implements Runnable {
    private static final Logger logger = LogManager.getLogger(Timer.class);
    @Override
    public void run() {
        logger.debug("Create new accident");
        VerificationHandling verificationHandling = new VerificationHandling();
        RentHandling rentHandling = new RentHandling();
        AccidentHandling accidentHandling = new AccidentHandling();
        try {
            for (Rental rent : rentHandling.findAllRents()) {
                Verification verification = verificationHandling.findVerificationByRentId(rent.getIdRental());
                if (rent.getReturnDate().isBefore(LocalDateTime.now()) && verification.getStatus().equals("paid")) {
                    verificationHandling.updateVerificationStatus(verification.getIdVerificationOfDocuments(), "returned");
                    accidentHandling.insertAccident(new Accident(rent.getIdRental(), 2, 200, "overdue", "not paid"));
                }
            }
        } catch (DBException e) {
            logger.error("Error while add new accident");
        }
    }
}
