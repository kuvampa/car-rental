package carrental.controller.util;

import java.io.IOException;

import jakarta.servlet.jsp.tagext.SimpleTagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * CarRatingTag is used to add star to every rating
 * @author Novikov Artem
 * @version 1.1
 */
public class CarRatingTag extends SimpleTagSupport {
    private static final Logger logger = LogManager.getLogger(CarRatingTag.class);
    private String rating;

    @Override
    public void doTag() throws IOException {
        logger.trace("car rating tag");
        getJspContext().getOut().write(rating + "★");
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
