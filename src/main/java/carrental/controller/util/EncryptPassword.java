package carrental.controller.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * EncryptPassword is used to encrypt users passwords
 * @author Novikov Artem
 * @version 1.1
 */
public class EncryptPassword {
    private static final Logger logger = LogManager.getLogger(EncryptPassword.class);

    private EncryptPassword() {
    }

    private static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    private static String toHexString(byte[] hash) {
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }
        return hexString.toString();
    }

    public static String encrypt(String password) throws NoSuchAlgorithmException {
        logger.debug("someone's password was encrypted");
        return toHexString(getSHA(password));
    }
}
