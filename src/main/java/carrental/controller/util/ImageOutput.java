package carrental.controller.util;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageOutput {
    /**
     * ImageOutput is used to processing incoming requests for images
     * @author Novikov Artem
     * @version 1.1
     */
    private ImageOutput() {
    }

    public static void output(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Path imageFile = Paths.get(session.getAttribute("directoryPath") + req.getPathInfo());
        resp.setContentType("image/jpeg");
        resp.setContentLength((int) Files.size(imageFile));
        try (OutputStream out = resp.getOutputStream()) {
            Files.copy(imageFile, out);
        }
    }
}
