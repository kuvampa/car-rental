package carrental.controller.util;

import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * Currency is used to take JSON file from p24api and parse information about currency
 * @author Novikov Artem
 * @version 1.1
 */
public class Currency {
    private static final Logger logger = LogManager.getLogger(Currency.class);

    private Currency() {
    }

    public static void currencyUpdate(HttpServletRequest req) throws MalformedURLException {
        BigDecimal bigDecimal;
        URL url = new URL("https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11");
        try (InputStream input = url.openStream()) {
            InputStreamReader isr = new InputStreamReader(input);
            BufferedReader reader = new BufferedReader(isr);
            StringBuilder json = new StringBuilder();
            int c;
            while ((c = reader.read()) != -1) {
                json.append((char) c);
            }
            String jsonString = json.toString();
            JSONArray obj = new JSONArray(jsonString);
            String currency = obj.getJSONObject(1).getString("buy");
            bigDecimal = BigDecimal.valueOf(Double.parseDouble(currency)).setScale(0, RoundingMode.HALF_EVEN);
            logger.debug("Current price of one UAH for one USD is {}", bigDecimal);
            req.setAttribute("USD", bigDecimal);
        } catch (IOException e) {
            logger.error("can't find real currency", e);
        }
    }
}
