package carrental.controller.util;

import carrental.db.entity.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The validator is used to validate the input data
 * @author Novikov Artem
 * @version 1.1
 */
public class Validator {

    public static class Insert {
        private Insert() {
        }

        public static boolean userCreate(User user) {
            return user.getLogin().matches("^\\w{3,20}$") &&
                    user.getFullName().matches("^(\\S).{1,40}(\\S)$") &&
                    user.getPassword().matches("^\\w{3,64}$") &&
                    user.getTelephoneNumber().matches("^\\d{10,12}$") &&
                    user.getEmail().matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
        }

        public static boolean carCreate(Car car) {
            return car.getCarName().matches("^(\\S).{1,45}(\\S)$") &&
                    (car.getPrice() <= 2000 && car.getPrice() > 0) &&
                    car.getType().matches("^\\w{4,20}$") &&
                    (car.getRating() > 0 && car.getRating() < 6) &&
                    (car.getCapacity() == 2 || car.getCapacity() == 4) &&
                    car.getColor().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getFuelType().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getManufacturer().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getPlateNumber().matches("^[-\\w]{10}$");
        }

        public static boolean insuranceCreate(InsurancePackage insurancePackage) {
            return insurancePackage.getDescription().matches("^(\\S).{1,500}(\\S)$") &&
                    insurancePackage.getName().matches("^(\\S).{1,45}(\\S)$");
        }

        public static boolean termsCreate(TermsOfUse termsOfUse) {
            return termsOfUse.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    termsOfUse.getFuelConditions().matches("^(\\S).{1,45}(\\S)$") &&
                    termsOfUse.getMileageLimit().matches("^(\\S).{1,45}(\\S)$");
        }

        public static boolean officeCreate(Office office) {
            return office.getTelephoneNumber().matches("^\\d{10,12}$") &&
                    office.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    office.getLocation().matches("^(\\S).{1,100}(\\S)$") &&
                    office.getLaborHours().matches("^(\\S).{1,45}(\\S)$") &&
                    office.getMap().matches("^(\\S).{1,500}(\\S)$");
        }

        public static boolean serviceCreate(AdditionalService additionalService) {
            return additionalService.getDescription().matches("^(\\S).{1,500}(\\S)$") &&
                    additionalService.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    (additionalService.getPrice() <= 50 && additionalService.getPrice() > 0);
        }

        public static boolean userSupportCreate(UserSupport userSupport) {
            return userSupport.getMessage().matches("^(\\S).{1,1000}(\\S)$");
        }
    }

    public static class Update {
        private Update() {
        }

        public static boolean userPassword(String password) {
            return password.matches("^\\w{3,20}$");
        }

        public static boolean userUpdate(User user) {
            return user.getIdUser() > 0 &&
                    user.getLogin().matches("^\\w{3,20}$") &&
                    user.getFullName().matches("^(\\S).{1,40}(\\S)$") &&
                    user.getPassword().matches("^\\w{64}$") &&
                    user.getTelephoneNumber().matches("^\\d{10,12}$") &&
                    user.getEmail().matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
        }

        public static boolean carUpdate(Car car) {
            return car.getCarName().matches("^(\\S).{1,45}(\\S)$") &&
                    (car.getPrice() <= 2000 && car.getPrice() > 0) &&
                    car.getType().matches("^\\w{4,20}$") &&
                    (car.getCapacity() == 2 || car.getCapacity() == 4) &&
                    car.getColor().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getFuelType().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getManufacturer().matches("^(\\S).{1,45}(\\S)$") &&
                    car.getPlateNumber().matches("^[-\\w]{10}$") &&
                    car.getIdCar() > 0 &&
                    car.getIdInsurance() > 0 &&
                    car.getIdTermsPackage() > 0 &&
                    car.getIdOffice() > 0 &&
                    (car.getRating() > 0 && car.getRating() < 6);
        }

        public static boolean insuranceUpdate(InsurancePackage insurancePackage) {
            return insurancePackage.getDescription().matches("^(\\S).{1,500}(\\S)$") &&
                    insurancePackage.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    insurancePackage.getIdInsurance() > 0;
        }

        public static boolean termsUpdate(TermsOfUse termsOfUse) {
            return termsOfUse.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    termsOfUse.getIdTermsPackage() > 0 &&
                    termsOfUse.getFuelConditions().matches("^(\\S).{1,45}(\\S)$") &&
                    termsOfUse.getMileageLimit().matches("^(\\S).{1,45}(\\S)$");
        }

        public static boolean officeUpdate(Office office) {
            return office.getTelephoneNumber().matches("^\\d{10,12}$") &&
                    office.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    office.getLocation().matches("^(\\S).{1,100}(\\S)$") &&
                    office.getLaborHours().matches("^(\\S).{1,45}(\\S)$") &&
                    office.getMap().matches("^(\\S).{1,500}(\\S)$") &&
                    office.getIdOffice() > 0;
        }

        public static boolean serviceUpdate(AdditionalService additionalService) {
            return additionalService.getDescription().matches("^(\\S).{1,500}(\\S)$") &&
                    additionalService.getName().matches("^(\\S).{1,45}(\\S)$") &&
                    (additionalService.getPrice() <= 50 && additionalService.getPrice() > 0) &&
                    additionalService.getIdService() > 0;
        }

        public static boolean verificationComment(Verification verification) {
            return verification.getComment().matches("^(\\S).{1,500}(\\S)$");
        }
    }

    public static class Data {
        private Data() {
        }

        public static boolean dataCheck(LocalDateTime dateTimePick, LocalDateTime dateTimeReturn, List<Rental> rentalsByRec) {
            boolean dateCheck = true;
            if (dateTimePick.isAfter(dateTimeReturn)) {
                dateCheck = false;
            }
            for (Rental rent : rentalsByRec) {
                if (dateTimePick.isAfter(rent.getDateOfReceiving()) && dateTimePick.isBefore(rent.getReturnDate())) {
                    dateCheck = false;
                }
            }
            for (Rental rent : rentalsByRec) {
                if (dateTimeReturn.isAfter(rent.getDateOfReceiving()) && dateTimeReturn.isBefore(rent.getReturnDate())) {
                    dateCheck = false;
                }
            }
            return dateCheck;
        }
    }
}
