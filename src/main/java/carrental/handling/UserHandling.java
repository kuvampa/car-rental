package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.UserDAO;
import carrental.db.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserHandling {
    private static final Logger logger = LogManager.getLogger(UserHandling.class);
    final Factory factory;
    final UserDAO userDAO;

    public UserHandling() {
        factory = Factory.getDAOFactory();
        userDAO = factory.getUserDAO();
    }

    public UserHandling(Factory factory) {
        this.factory = factory;
        userDAO = factory.getUserDAO();
    }

    public List<User> findAllUsers() throws DBException {
        logger.debug("Executing handling operation: find all users");
        return userDAO.findAll();
    }

    public void insertUser(User user) throws DBException {
        logger.debug("Executing handling operation: insert new user - [{}]", user.getLogin());
        userDAO.insert(user);
    }

    public void deleteByName(String login) throws DBException {
        logger.debug("Executing handling operation: delete admin by login - [{}]", login);
        userDAO.deleteByName(login);
    }

    public void updateUser(User user) throws DBException {
        logger.debug("Executing handling operation: update user - [{}]", user.getLogin());
        userDAO.update(user);
    }

    public User findUserByName(String login) throws DBException {
        logger.debug("Executing handling operation: find user by login - [{}]", login);
        return userDAO.findUser(login);
    }

    public void updateUserDocs(String login, String docs) throws DBException {
        logger.debug("Executing handling operation: update user documents by login - [{}]", login);
        userDAO.updateUserDocs(login, docs);
    }

    public void updateUserStatus(String login, int status) throws DBException {
        logger.debug("Executing handling operation: update user status - [{}] by login - [{}]", status, login);
        userDAO.updateUserStatus(login, status);
    }

    public void deleteUserById(int id) throws DBException {
        logger.debug("Executing handling operation: delete user by id - [{}]", id);
        userDAO.deleteById(id);
    }

    public User findUserById(int id) throws DBException {
        logger.debug("Executing handling operation: find user by id - [{}]", id);
        return userDAO.findById(id);
    }


}
