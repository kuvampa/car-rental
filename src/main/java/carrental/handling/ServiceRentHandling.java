package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.ServiceRentDAO;
import carrental.db.entity.ServiceRent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ServiceRentHandling {
    private static final Logger logger = LogManager.getLogger(ServiceRentHandling.class);
    final Factory factory;
    final ServiceRentDAO serviceRentDAO;

    public ServiceRentHandling() {
        factory = Factory.getDAOFactory();
        serviceRentDAO = factory.getServiceRentDAO();
    }

    public ServiceRentHandling(Factory factory) {
        this.factory = factory;
        serviceRentDAO = factory.getServiceRentDAO();
    }

    public List<ServiceRent> findAllServicesRents() throws DBException {
        logger.debug("Executing handling operation: find all services in rents");
        return serviceRentDAO.findAll();
    }

    public void insertServiceRent(ServiceRent serviceRent) throws DBException {
        logger.debug("Executing handling operation: insert new services in rent - [{}]", serviceRent.getIdService());
        serviceRentDAO.insert(serviceRent);
    }

    public void deleteServiceRent(int id) throws DBException {
        logger.debug("Executing handling operation: delete services in rent by id - [{}]", id);
        serviceRentDAO.deleteById(id);
    }

    public void updateServiceRent(ServiceRent serviceRent) throws DBException {
        logger.debug("Executing handling operation: update services in rent - [{}]", serviceRent.getIdService());
        serviceRentDAO.update(serviceRent);
    }

    public ServiceRent findServiceRentById(int id) throws DBException {
        logger.debug("Executing handling operation: find services in rent by id - [{}]", id);
        return serviceRentDAO.findById(id);
    }
}
