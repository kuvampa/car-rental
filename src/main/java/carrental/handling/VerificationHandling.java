package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.VerificationDAO;
import carrental.db.entity.Verification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class VerificationHandling {
    private static final Logger logger = LogManager.getLogger(VerificationHandling.class);
    final Factory factory;
    final VerificationDAO verificationDAO;

    public VerificationHandling() {
        factory = Factory.getDAOFactory();
        verificationDAO = factory.getVerificationDAO();
    }

    public VerificationHandling(Factory factory) {
        this.factory = factory;
        verificationDAO = factory.getVerificationDAO();
    }

    public List<Verification> findAllVerifications() throws DBException {
        logger.debug("Executing handling operation: find all verification");
        return verificationDAO.findAll();
    }

    public void insertVerification(Verification verification) throws DBException {
        logger.debug("Executing handling operation: insert new verification - [{}]", verification.getIdVerificationOfDocuments());
        verificationDAO.insert(verification);
    }

    public void deleteVerification(int id) throws DBException {
        logger.debug("Executing handling operation: delete verification by id - [{}]", id);
        verificationDAO.deleteById(id);
    }

    public void updateVerifications(Verification verification) throws DBException {
        logger.debug("Executing handling operation: update verification - [{}]", verification.getIdVerificationOfDocuments());
        verificationDAO.update(verification);
    }

    public void updateVerificationStatus(int id, String status) throws DBException {
        logger.debug("Executing handling operation: update verification status - [{}] by id - [{}]", status, id);
        verificationDAO.updateVerificationStatus(id, status);
    }

    public void updateVerificationManager(int id, int manager) throws DBException {
        logger.debug("Executing handling operation: update verification status - [{}] by manager - [{}]", manager, id);
        verificationDAO.updateVerificationManager(id, manager);
    }

    public Verification findVerificationById(int id) throws DBException {
        logger.debug("Executing handling operation: find verification by id - [{}]", id);
        return verificationDAO.findById(id);
    }
    public Verification findVerificationByRentId(int id) throws DBException {
        logger.debug("Executing handling operation: find verification by rent id - [{}]", id);
        return verificationDAO.findByRentId(id);
    }
}
