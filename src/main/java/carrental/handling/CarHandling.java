package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.CarDAO;
import carrental.db.dao.Factory;
import carrental.db.entity.Car;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CarHandling {
    private static final Logger logger = LogManager.getLogger(CarHandling.class);
    final Factory factory;
    final CarDAO carDAO;

    public CarHandling() {
        factory = Factory.getDAOFactory();
        carDAO = factory.getCarDAO();
    }

    public CarHandling(Factory factory) {
        this.factory = factory;
        carDAO = factory.getCarDAO();
    }

    public List<Car> findAllCars() throws DBException {
        logger.debug("Executing handling operation: find all cars");
        return carDAO.findAll();

    }

    public List<String> searchManufactures(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search manufactures");
        List<String> ourList = new ArrayList<>();
        for (String manufactures : carDAO.findUniqueManufacture()) {
            String manufacturesReal = req.getParameter(manufactures);
            if (Objects.equals(manufacturesReal, "on")) {
                ourList.add(manufactures);
            }
        }
        return ourList;
    }

    public List<String> searchColor(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search colors");
        List<String> ourList = new ArrayList<>();
        for (String colors : carDAO.findUniqueColor()) {
            String colorsReal = req.getParameter(colors);
            if (Objects.equals(colorsReal, "on")) {
                ourList.add(colors);
            }
        }
        return ourList;
    }

    public List<Integer> searchCapacity(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search capacity");
        List<Integer> ourList = new ArrayList<>();
        for (int capacity : carDAO.findUniqueCapacity()) {
            String capacityReal = req.getParameter(capacity + "C");
            if (Objects.equals(capacityReal, "on")) {
                ourList.add(capacity);
            }
        }
        return ourList;
    }

    public List<String> searchType(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search types");
        List<String> ourList = new ArrayList<>();
        for (String type : carDAO.findUniqueType()) {
            String typeReal = req.getParameter(type);
            if (Objects.equals(typeReal, "on")) {
                ourList.add(type);
            }
        }
        return ourList;
    }

    public List<String> searchFuelType(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search fuel types");
        List<String> ourList = new ArrayList<>();
        for (String fuelType : carDAO.findUniqueFuelType()) {
            String fuelTypeReal = req.getParameter(fuelType);
            if (Objects.equals(fuelTypeReal, "on")) {
                ourList.add(fuelType);
            }
        }
        return ourList;
    }

    public List<Integer> searchRating(HttpServletRequest req) throws DBException {
        logger.debug("Executing handling operation: search ratings");
        List<Integer> ourList = new ArrayList<>();
        for (int rating : carDAO.findUniqueRating()) {
            String ratingReal = req.getParameter(rating + "R");
            if (Objects.equals(ratingReal, "on")) {
                ourList.add(rating);
            }
        }
        return ourList;
    }

    public List<String> findUniqueManufacture() throws DBException {
        logger.debug("Executing handling operation: find unique manufactures");
        return carDAO.findUniqueManufacture();
    }

    public List<String> findUniqueColor() throws DBException {
        logger.debug("Executing handling operation: find unique colors");
        return carDAO.findUniqueColor();
    }

    public List<Integer> findUniqueCapacity() throws DBException {
        logger.debug("Executing handling operation: find unique capacity");
        return carDAO.findUniqueCapacity();
    }

    public List<String> findUniqueType() throws DBException {
        logger.debug("Executing handling operation: find unique type");
        return carDAO.findUniqueType();
    }

    public List<String> findUniqueFuelType() throws DBException {
        logger.debug("Executing handling operation: find unique fuel type");
        return carDAO.findUniqueFuelType();
    }

    public List<Integer> findUniqueRating() throws DBException {
        logger.debug("Executing handling operation: find unique rating");
        return carDAO.findUniqueRating();
    }

    public List<Car> searchCars(String name, List<String> types, List<String> fuelTypes, int min, int max, List<Integer> ratings, List<String> manufactures, List<String> colors, List<Integer> capacity, String sort, HttpServletRequest req, HttpSession session) throws DBException {
        logger.debug("Executing handling operation: search cars");
        int page;
        req.setAttribute("searchedMin", min);
        req.setAttribute("searchedMax", max);
        req.setAttribute("searchedName", name);
        if (!types.isEmpty()) {
            req.setAttribute("searchedTypes", types);
        } else {
            req.setAttribute("checkerT", 1);
        }
        if (!fuelTypes.isEmpty()) {
            req.setAttribute("searchedFuelTypes", fuelTypes);
        } else {
            req.setAttribute("checkerF", 1);
        }
        if (!ratings.isEmpty()) {
            req.setAttribute("searchedRatings", ratings);
        } else {
            req.setAttribute("checkerR", 1);
        }
        if (!manufactures.isEmpty()) {
            req.setAttribute("searchedManufactures", manufactures);
        } else {
            req.setAttribute("checkerM", 1);
        }
        if (!colors.isEmpty()) {
            req.setAttribute("searchedColors", colors);
        } else {
            req.setAttribute("checkerCo", 1);
        }
        if (!capacity.isEmpty()) {
            req.setAttribute("searchedCapacity", capacity);
        } else {
            req.setAttribute("checkerCa", 1);
        }
        switch (sort) {
            case "1":
                req.setAttribute("sort", 1);
                break;
            case "2":
                req.setAttribute("sort", 2);
                break;
            case "3":
                req.setAttribute("sort", 3);
                break;
            case "4":
                req.setAttribute("sort", 4);
                break;
            default:
                req.setAttribute("sort", 5);
                break;
        }
        if (session.getAttribute("page") == null) {
            session.setAttribute("page", 1);
            page = 1;
        } else {
            page = (int) session.getAttribute("page");
        }
        if (Objects.equals(req.getParameter("page"), "right")) {
            page += 1;
            session.setAttribute("page", page);
        }
        if (Objects.equals(req.getParameter("page"), "left")) {
            page -= 1;
            session.setAttribute("page", page);
        }
        int count = carDAO.countSortedCars(sort, manufactures, name, types, fuelTypes, min, max, ratings, colors, capacity);
        if ((page*9)-8>count) {
            page = 1;
            session.setAttribute("page", page);
        }
        if(page == 1){
            req.setAttribute("blockL","left");
        }
        if(((page)*9)+1>count){
            req.setAttribute("blockR","right");
        }
        return carDAO.findSortedLimitedCars(page, sort, manufactures, name, types, fuelTypes, min, max, ratings, colors, capacity);
    }

    public void insertCar(Car car) throws DBException {
        logger.debug("Executing handling operation: insert new car - [{}]", car.getCarName());
        carDAO.insert(car);
    }

    public void deleteCar(int id) throws DBException {
        logger.debug("Executing handling operation: delete car by id - [{}]", id);
        carDAO.deleteById(id);
    }

    public void updateCar(Car car) throws DBException {
        logger.debug("Executing handling operation: update car - [{}]", car.getCarName());
        carDAO.update(car);
    }

    public void updateCarStatus(String plate, String status) throws DBException {
        logger.debug("Executing handling operation: update car status by plate - [{}]", plate);
        carDAO.updateCarStatus(plate, status);
    }

    public Car findCarById(int id) throws DBException {
        logger.debug("Executing handling operation: find car by id - [{}]", id);
        return carDAO.findById(id);
    }

    public Car findCarByPlate(String plateNumber) throws DBException {
        logger.debug("Executing handling operation: find car by plate - [{}]", plateNumber);
        return carDAO.findCarByPlate(plateNumber);
    }
}
