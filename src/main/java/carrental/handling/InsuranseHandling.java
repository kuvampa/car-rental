package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.InsuranceDAO;
import carrental.db.entity.InsurancePackage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class InsuranseHandling {
    private static final Logger logger = LogManager.getLogger(InsuranseHandling.class);
    final Factory factory;
    final InsuranceDAO insuranceDAO;

    public InsuranseHandling() {
        factory = Factory.getDAOFactory();
        insuranceDAO = factory.getInsuranseDAO();
    }

    public InsuranseHandling(Factory factory) {
        this.factory = factory;
        insuranceDAO = factory.getInsuranseDAO();
    }

    public List<InsurancePackage> findAllInsurances() throws DBException {
        logger.debug("Executing handling operation: find all insurances");
        return insuranceDAO.findAll();
    }

    public void insertInsurance(InsurancePackage insurancePackage) throws DBException {
        logger.debug("Executing handling operation: insert new insurance - [{}]", insurancePackage.getName());
        insuranceDAO.insert(insurancePackage);
    }

    public void deleteInsurance(int id) throws DBException {
        logger.debug("Executing handling operation: delete insurance by id - [{}]", id);
        insuranceDAO.deleteById(id);
    }

    public void updateInsurance(InsurancePackage insurancePackage) throws DBException {
        logger.debug("Executing handling operation: update insurance - [{}]", insurancePackage.getName());
        insuranceDAO.update(insurancePackage);
    }

    public void updateInsuranceStatus(String name, String status) throws DBException {
        logger.debug("Executing handling operation: update insurance status - [{}] by name - [{}]", status, name);
        insuranceDAO.updateInsuranceStatus(name, status);
    }

    public InsurancePackage findInsuranceById(int idInsurance) throws DBException {
        logger.debug("Executing handling operation: find insurance by insurance - [{}]", idInsurance);
        return insuranceDAO.findById(idInsurance);
    }

    public InsurancePackage findInsuranceByName(String name) throws DBException {
        logger.debug("Executing handling operation: find insurance by name - [{}]", name);
        return insuranceDAO.findByName(name);
    }
}
