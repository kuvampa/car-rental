package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.OfficeDAO;
import carrental.db.entity.Office;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class OfficeHandling {
    private static final Logger logger = LogManager.getLogger(OfficeHandling.class);
    final Factory factory;
    final OfficeDAO officeDAO;

    public OfficeHandling() {
        factory = Factory.getDAOFactory();
        officeDAO = factory.getOfficeDAO();
    }

    public OfficeHandling(Factory factory) {
        this.factory = factory;
        officeDAO = factory.getOfficeDAO();
    }

    public List<Office> findAllOffices() throws DBException {
        logger.debug("Executing handling operation: find all offices");
        return officeDAO.findAll();
    }

    public void insertOffice(Office office) throws DBException {
        logger.debug("Executing handling operation: insert new office - [{}]", office.getName());
        officeDAO.insert(office);
    }

    public void deleteOffice(int id) throws DBException {
        logger.debug("Executing handling operation: delete office by id - [{}]", id);
        officeDAO.deleteById(id);
    }

    public void updateOffice(Office office) throws DBException {
        logger.debug("Executing handling operation: update office - [{}]", office.getName());
        officeDAO.update(office);
    }

    public void updateOfficeStatus(String name, String status) throws DBException {
        logger.debug("Executing handling operation: update office status - [{}] by name -[{}]", status, name);
        officeDAO.updateOfficeStatus(name, status);
    }

    public Office findOfficeByName(String name) throws DBException {
        logger.debug("Executing handling operation: find office by name - [{}]", name);
        return officeDAO.findOfficeByName(name);
    }

    public Office findOfficebyId(int officeId) throws DBException {
        logger.debug("Executing handling operation: find office by id - [{}]", officeId);
        return officeDAO.findById(officeId);
    }
}
