package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.AdminDAO;
import carrental.db.dao.Factory;
import carrental.db.entity.Admin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class AdminHandling {
    private static final Logger logger = LogManager.getLogger(AdminHandling.class);
    final Factory factory;
    final AdminDAO adminDAO;

    public AdminHandling() {
        factory = Factory.getDAOFactory();
        adminDAO = factory.getAdminDAO();
    }

    public AdminHandling(Factory factory) {
        this.factory = factory;
        adminDAO = factory.getAdminDAO();
    }

    public List<Admin> findAllAdmins() throws DBException {
        logger.debug("Executing handling operation: find all admins");
        return adminDAO.findAll();
    }

    public void insertAdmin(Admin admin) throws DBException {
        logger.debug("Executing handling operation: insert new admin - [{}]", admin.getLogin());
        adminDAO.insert(admin);
    }

    public void deleteByName(String login) throws DBException {
        logger.debug("Executing handling operation: delete admin by login - [{}]", login);
        adminDAO.deleteByName(login);
    }

    public void updateAdmin(Admin admin) throws DBException {
        logger.debug("Executing handling operation: update admin - [{}]", admin.getLogin());
        adminDAO.update(admin);
    }

    public Admin findAdminByName(String login) throws DBException {
        logger.debug("Executing handling operation: find admin by login - [{}]", login);
        return adminDAO.findAdmin(login);
    }

    public void updateAdminDocs(String login, String docs) throws DBException {
        logger.debug("Executing handling operation: update admin documents by login - [{}]", login);
        adminDAO.updateAdminDocs(login, docs);
    }

    public void deleteAdminById(int id) throws DBException {
        logger.debug("Executing handling operation: delete admin by id - [{}]", id);
        adminDAO.deleteById(id);
    }

    public Admin findAdminById(int id) throws DBException {
        logger.debug("Executing handling operation: find admin by id - [{}]", id);
        return adminDAO.findById(id);
    }
}
