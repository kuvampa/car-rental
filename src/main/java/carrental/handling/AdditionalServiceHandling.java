package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.ServiceDAO;
import carrental.db.entity.AdditionalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class AdditionalServiceHandling {
    private static final Logger logger = LogManager.getLogger(AdditionalServiceHandling.class);
    final Factory factory;
    final ServiceDAO serviceDAO;

    public AdditionalServiceHandling() {
        factory = Factory.getDAOFactory();
        serviceDAO = factory.getServiceDAO();
    }

    public AdditionalServiceHandling(Factory factory) {
        this.factory = factory;
        serviceDAO = factory.getServiceDAO();
    }

    public List<AdditionalService> findAllServices() throws DBException {
        logger.debug("Executing handling operation: find all services");
        return serviceDAO.findAll();
    }

    public void insertService(AdditionalService additionalService) throws DBException {
        logger.debug("Executing handling operation: insert new service - [{}]", additionalService.getName());
        serviceDAO.insert(additionalService);
    }

    public void deleteService(int id) throws DBException {
        logger.debug("Executing handling operation: delete new service - [{}]", id);
        serviceDAO.deleteById(id);
    }

    public void updateService(AdditionalService additionalService) throws DBException {
        logger.debug("Executing handling operation: update new service - [{}]", additionalService.getName());
        serviceDAO.update(additionalService);
    }
    public void updateServiceStatus(String name,String status) throws DBException {
        logger.debug("Executing handling operation: update new service - [{}]", name);
        serviceDAO.updateServiceStatus(name,status);
    }

    public AdditionalService findServiceById(int id) throws DBException {
        logger.debug("Executing handling operation: find service by id - [{}]", id);
        return serviceDAO.findById(id);
    }
    public AdditionalService findServiceByName(String name) throws DBException {
        logger.debug("Executing handling operation: find service by name - [{}]", name);
        return serviceDAO.findServiceByName(name);
    }

}
