package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.UserSupportDAO;
import carrental.db.entity.UserSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserSupportHandling {
    private static final Logger logger = LogManager.getLogger(UserSupportHandling.class);
    final Factory factory;
    final UserSupportDAO userSupportDAO;

    public UserSupportHandling() {
        factory = Factory.getDAOFactory();
        userSupportDAO = factory.getUserSupportDAO();
    }

    public UserSupportHandling(Factory factory) {
        this.factory = factory;
        userSupportDAO = factory.getUserSupportDAO();
    }

    public List<UserSupport> findAllUserSupports() throws DBException {
        logger.debug("Executing handling operation: find all user supports");
        return userSupportDAO.findAll();
    }

    public void insertUserSupport(UserSupport userSupport) throws DBException {
        logger.debug("Executing handling operation: insert new user support - [{}]", userSupport.getIdUserSupport());
        userSupportDAO.insert(userSupport);
    }

    public void deleteUserSupport(int id) throws DBException {
        logger.debug("Executing handling operation: delete accident by id - [{}]", id);
        userSupportDAO.deleteById(id);
    }

    public void updateUserSupport(UserSupport userSupport) throws DBException {
        logger.debug("Executing handling operation: update user support - [{}]", userSupport.getIdUserSupport());
        userSupportDAO.update(userSupport);
    }

    public void updateUserSupportStatus(int id, String status) throws DBException {
        logger.debug("Executing handling operation: update accident user support status - [{}] by id - [{}]", status, id);
        userSupportDAO.updateUserSupportStatus(id, status);
    }
    public void updateUserSupportAdmin(int id, int admin) throws DBException {
        logger.debug("Executing handling operation: update accident user support admin - [{}] by id - [{}]", admin, id);
        userSupportDAO.updateUserSupportAdmin(id, admin);
    }

    public UserSupport findUserSupportById(int id) throws DBException {
        logger.debug("Executing handling operation: find user support by id - [{}]", id);
        return userSupportDAO.findById(id);
    }
}
