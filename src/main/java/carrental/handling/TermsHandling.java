package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.TermsDAO;
import carrental.db.entity.TermsOfUse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TermsHandling {
    private static final Logger logger = LogManager.getLogger(TermsHandling.class);
    final Factory factory;
    final TermsDAO termsDAO;

    public TermsHandling() {
        factory = Factory.getDAOFactory();
        termsDAO = factory.getTermsDAO();
    }

    public TermsHandling(Factory factory) {
        this.factory = factory;
        termsDAO = factory.getTermsDAO();
    }

    public List<TermsOfUse> findAllTerms() throws DBException {
        logger.debug("Executing handling operation: find all terms");
        return termsDAO.findAll();
    }

    public void insertTerm(TermsOfUse termsOfUse) throws DBException {
        logger.debug("Executing handling operation: insert new term - [{}]", termsOfUse.getName());
        termsDAO.insert(termsOfUse);
    }

    public void deleteTerm(int id) throws DBException {
        logger.debug("Executing handling operation: delete new term - [{}]", id);
        termsDAO.deleteById(id);
    }

    public void updateTerms(TermsOfUse termsOfUse) throws DBException {
        logger.debug("Executing handling operation: update term - [{}]", termsOfUse.getName());
        termsDAO.update(termsOfUse);
    }

    public void updateTermStatus(String name, String status) throws DBException {
        logger.debug("Executing handling operation: update term status - [{}] by name - [{}]", status,name);
        termsDAO.updateTermStatus(name, status);
    }

    public TermsOfUse findTermById(int id) throws DBException {
        logger.debug("Executing handling operation: find term by id - [{}]", id);
        return termsDAO.findById(id);
    }

    public TermsOfUse findTermByName(String name) throws DBException {
        logger.debug("Executing handling operation: find term by name - [{}]", name);

        return termsDAO.findByName(name);
    }
}
