package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.ManagerDAO;
import carrental.db.entity.Manager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ManagerHandling {
    private static final Logger logger = LogManager.getLogger(ManagerHandling.class);
    final Factory factory;
    final ManagerDAO managerDAO;

    public ManagerHandling() {
        factory = Factory.getDAOFactory();
        managerDAO = factory.getManagerDAO();
    }

    public ManagerHandling(Factory factory) {
        this.factory = factory;
        managerDAO = factory.getManagerDAO();
    }

    public List<Manager> findAllManagers() throws DBException {
        logger.debug("Executing handling operation: find all managers");
        return managerDAO.findAll();
    }

    public void insertManager(Manager manager) throws DBException {
        logger.debug("Executing handling operation: insert new manager - [{}]", manager.getLogin());
        managerDAO.insert(manager);
    }

    public void deleteByName(String login) throws DBException {
        logger.debug("Executing handling operation: delete manager by login - [{}]", login);
        managerDAO.deleteByName(login);
    }

    public void updateManager(Manager manager) throws DBException {
        logger.debug("Executing handling operation: update manager - [{}]", manager.getLogin());
        managerDAO.update(manager);
    }

    public Manager findManagerByName(String login) throws DBException {
        logger.debug("Executing handling operation: find manager by login - [{}]", login);
        return managerDAO.findManager(login);
    }

    public void updateManagerDocs(String login, String docs) throws DBException {
        logger.debug("Executing handling operation: update manager documents by login - [{}]", login);
        managerDAO.updateManagerDocs(login, docs);
    }

    public void deleteManagerById(int id) throws DBException {
        logger.debug("Executing handling operation: find manager by id - [{}]", id);
        managerDAO.deleteById(id);
    }

    public Manager findManagerById(int id) throws DBException {
        logger.debug("Executing handling operation: find manager by id - [{}]", id);
        return managerDAO.findById(id);
    }
}
