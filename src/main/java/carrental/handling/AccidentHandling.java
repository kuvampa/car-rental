package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.AccidentDAO;
import carrental.db.dao.Factory;
import carrental.db.entity.Accident;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class AccidentHandling {
    private static final Logger logger = LogManager.getLogger(AccidentHandling.class);
    final Factory factory;
    final AccidentDAO accidentDAO;

    public AccidentHandling() {
        factory = Factory.getDAOFactory();
        accidentDAO = factory.getAccidentDAO();
    }

    public AccidentHandling(Factory factory) {
        this.factory = factory;
        accidentDAO = factory.getAccidentDAO();
    }

    public List<Accident> findAllAccidents() throws DBException {
        logger.debug("Executing handling operation: find all accidents");
        return accidentDAO.findAll();
    }

    public void insertAccident(Accident accident) throws DBException {
        logger.debug("Executing handling operation: insert new accident - [{}]", accident.getIdAccident());
        accidentDAO.insert(accident);
    }

    public void deleteAccident(int id) throws DBException {
        logger.debug("Executing handling operation: delete accident by id - [{}]", id);
        accidentDAO.deleteById(id);
    }

    public void updateAccident(Accident accident) throws DBException {
        logger.debug("Executing handling operation: update accident - [{}]", accident.getIdAccident());
        accidentDAO.update(accident);
    }

    public void updateAccidentStatus(int id, String status) throws DBException {
        logger.debug("Executing handling operation: update accident status - [{}] by id - [{}]", status, id);
        accidentDAO.updateAccidentStatus(id, status);
    }

    public void updateAccidentManager(int id, int manager) throws DBException {
        logger.debug("Executing handling operation: update accident manager id - [{}] by id - [{}]", manager, id);
        accidentDAO.updateAccidentManager(id, manager);
    }

    public Accident findAccidentById(int id) throws DBException {
        logger.debug("Executing service operation: find accident by id - [{}]", id);
        return accidentDAO.findById(id);
    }
}
