package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.RentDAO;
import carrental.db.entity.Rental;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;

public class RentHandling {
    private static final Logger logger = LogManager.getLogger(RentHandling.class);
    final Factory factory;
    final RentDAO rentDAO;

    public RentHandling() {
        factory = Factory.getDAOFactory();
        rentDAO = factory.getRentDAO();
    }

    public RentHandling(Factory factory) {
        this.factory = factory;
        rentDAO = factory.getRentDAO();
    }

    public List<Rental> findAllRents() throws DBException {
        logger.debug("Executing handling operation: find all rents");
        return rentDAO.findAll();
    }
    public List<Rental> findRentsByCarIdSortByReceivingDate(int carId) throws DBException {
        logger.debug("Executing handling operation: find rents by car id sort by receiving date");
        return rentDAO.findByIdSortByReceivingDate(carId);
    }
    public List<Rental> findRentsByCarIdSortByReturnDate(int carId) throws DBException {
        logger.debug("Executing handling operation: find rents by car id sort by return date");
        return rentDAO.findByIdSortByReturnDate(carId);
    }

    public void insertRent(Rental rental) throws DBException {
        logger.debug("Executing handling operation: insert new rent - [{}]", rental.getIdRental());
        rentDAO.insert(rental);
    }

    public void deleteRent(int id) throws DBException {
        logger.debug("Executing handling operation: delete rent by id - [{}]", id);
        rentDAO.deleteById(id);
    }

    public void updateRent(Rental rental) throws DBException {
        logger.debug("Executing handling operation: update rent - [{}]", rental.getIdRental());
        rentDAO.update(rental);
    }

    public Rental findRentById(int id) throws DBException {
        logger.debug("Executing handling operation: find rent by id - [{}]", id);
        return rentDAO.findById(id);
    }
    public Rental findRentByDate(LocalDateTime rec, LocalDateTime ret) throws DBException {
        logger.debug("Executing handling operation: find rent by dates - [{}] and - [{}]", rec , ret);
        return rentDAO.findByDate(rec,ret);
    }
}
