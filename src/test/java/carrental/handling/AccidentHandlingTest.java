package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.AccidentDAOImpl;
import carrental.db.entity.Accident;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AccidentHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    AccidentDAOImpl mockAccidentDAOImpl;

    AccidentHandling accidentHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getAccidentDAO()).thenReturn(mockAccidentDAOImpl);
        accidentHandling = new AccidentHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] accidentId = {1, 2, 3};
        int[] rentId = {1, 2, 3};
        int[] managerId = {1, 2, 3};
        double[] price = {30.1, 30.2, 30.3};
        String[] comment = {"Boom", "Bam", "Badaboom"};
        String[] status = {"unchecked", "unchecked", "unchecked"};
        List<Accident> testAccidentList = new ArrayList<>();
        for (int i = 0; i < managerId.length; i++) {
            Accident accident = new Accident(accidentId[i], rentId[i], managerId[i], price[i], comment[i], status[i]);
            testAccidentList.add(accident);
        }
        Mockito.when(mockAccidentDAOImpl.findAll()).thenReturn(testAccidentList);
        Assertions.assertEquals(testAccidentList, accidentHandling.findAllAccidents());
        Mockito.verify(mockAccidentDAOImpl, Mockito.times(1)).findAll();
    }
}
