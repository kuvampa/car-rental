package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.RentDAOImpl;
import carrental.db.entity.Rental;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class RentHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    RentDAOImpl mockRentDAOImpl;

    RentHandling rentHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getRentDAO()).thenReturn(mockRentDAOImpl);
        rentHandling = new RentHandling(mockFactory);
    }
    @Test
    void findAll() throws DBException {
        int[] rentalId = {1, 2, 3};
        int[] userId = {1, 2, 3};
        int[] carId = {1, 2, 3};
        int[] rightOfCancellation = {1, 1, 1};
        double[] price = {10, 15, 20};
        LocalDateTime[] dateOfReceiving = {LocalDateTime.of(2022, 1, 13, 15, 56), LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 3, 13, 15, 56)};
        LocalDateTime[] returnDate = {LocalDateTime.of(2023, 1, 13, 15, 56), LocalDateTime.of(2023, 2, 13, 15, 56), LocalDateTime.of(2023, 3, 13, 15, 56)};
        String[] returnPlace = {"Office", "Office", "Office"};
        List<Rental> testRentList = new ArrayList<>();
        for (int i = 0; i < rentalId.length; i++) {
            Rental rental = new Rental(rentalId[i], userId[i], carId[i], rightOfCancellation[i], price[i], returnPlace[i], dateOfReceiving[i], returnDate[i]);
            testRentList.add(rental);
        }
        Mockito.when(mockRentDAOImpl.findAll()).thenReturn(testRentList);
        Assertions.assertEquals(testRentList, rentHandling.findAllRents());
        Mockito.verify(mockRentDAOImpl, Mockito.times(1)).findAll();
    }
}
