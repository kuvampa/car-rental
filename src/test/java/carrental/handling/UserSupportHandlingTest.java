package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.UserSupportDAOImpl;
import carrental.db.entity.UserSupport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserSupportHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    UserSupportDAOImpl mockUserSupportDAOImpl;

    UserSupportHandling userSupportHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getUserSupportDAO()).thenReturn(mockUserSupportDAOImpl);
        userSupportHandling = new UserSupportHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] userSupportId = {1, 2, 3};
        int[] userId = {1, 2, 3};
        int[] adminId = {1, 2, 3};
        String[] message = {"Cool navbar", "Cool footer", "Cool background"};
        String[] status = {"available", "available", "available"};
        List<UserSupport> testUserSupportList = new ArrayList<>();
        for (int i = 0; i < userSupportId.length; i++) {
            UserSupport userSupport = new UserSupport(userSupportId[i], userId[i], adminId[i], message[i], status[i]);
            testUserSupportList.add(userSupport);
        }
        Mockito.when(mockUserSupportDAOImpl.findAll()).thenReturn(testUserSupportList);
        Assertions.assertEquals(testUserSupportList, userSupportHandling.findAllUserSupports());
        Mockito.verify(mockUserSupportDAOImpl, Mockito.times(1)).findAll();
    }
}
