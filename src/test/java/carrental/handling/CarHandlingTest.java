package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.CarDAOImpl;
import carrental.db.entity.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class CarHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    CarDAOImpl mockCarDAOImpl;

    CarHandling carHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getCarDAO()).thenReturn(mockCarDAOImpl);
        carHandling = new CarHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] idCar = {1, 2, 3};
        int[] idOffice = {1, 2, 3};
        int[] idInsurance = {1, 2, 3};
        int[] idTermsPackage = {1, 2, 3};
        int[] capacity = {2, 2, 4};
        int[] rating = {1, 2, 3};
        double[] price = {15, 20, 30};
        String[] status = {"ok", "ok", "ok"};
        String[] color = {"white", "white", "white"};
        String[] carName = {"Ford Fiesta", "Nissan GTR", "Porshe 911"};
        String[] plateNumber = {"AA-1313-AA", "AA-1414-AA", "AA-1414-AA"};
        String[] type = {"sport", "super sport", "super duper sport"};
        String[] manufacturer = {"Ford", "Nissan", "Porshe"};
        String[] fuelType = {"petrol", "petrol", "petrol"};
        List<Car> testCarList = new ArrayList<>();
        for (int i = 0; i < idCar.length; i++) {
            Car car = new Car(idCar[i], idOffice[i], idInsurance[i], idTermsPackage[i], capacity[i], rating[i], price[i], status[i], color[i], carName[i], plateNumber[i], type[i], manufacturer[i], fuelType[i]);
            testCarList.add(car);
        }
        Mockito.when(mockCarDAOImpl.findAll()).thenReturn(testCarList);
        Assertions.assertEquals(testCarList, carHandling.findAllCars());
        Mockito.verify(mockCarDAOImpl, Mockito.times(1)).findAll();
    }
}
