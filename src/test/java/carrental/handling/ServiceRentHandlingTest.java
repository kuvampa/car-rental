package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.ServiceRentDAOImpl;
import carrental.db.entity.ServiceRent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class ServiceRentHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    ServiceRentDAOImpl mockServiceRentDAOImpl;

    ServiceRentHandling serviceRentHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getServiceRentDAO()).thenReturn(mockServiceRentDAOImpl);
        serviceRentHandling = new ServiceRentHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] idRentService = {1, 2, 3};
        int[] idRent = {1, 2, 3};
        int[] idService = {1, 2, 3};
        List<ServiceRent> testServiceRentList = new ArrayList<>();
        for (int i = 0; i < idRentService.length; i++) {
            ServiceRent serviceRent = new ServiceRent(idRentService[i], idRent[i], idService[i]);
            testServiceRentList.add(serviceRent);
        }
        Mockito.when(mockServiceRentDAOImpl.findAll()).thenReturn(testServiceRentList);
        Assertions.assertEquals(testServiceRentList, serviceRentHandling.findAllServicesRents());
        Mockito.verify(mockServiceRentDAOImpl, Mockito.times(1)).findAll();
    }
}
