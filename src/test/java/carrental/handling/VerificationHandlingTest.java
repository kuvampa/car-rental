package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.VerificationDAOImpl;
import carrental.db.entity.Verification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class VerificationHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    VerificationDAOImpl mockVerificationDAOImpl;

    VerificationHandling verificationHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getVerificationDAO()).thenReturn(mockVerificationDAOImpl);
        verificationHandling = new VerificationHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] verificationId = {1, 2, 3};
        int[] rentalId = {1, 2, 3};
        int[] managerId = {1, 2, 3};
        String[] comment = {"reject", "reject", "reject"};
        String[] status = {"reject", "reject", "reject"};
        List<Verification> testVerificationList = new ArrayList<>();
        for (int i = 0; i < managerId.length; i++) {
            Verification verification = new Verification(verificationId[i], rentalId[i], managerId[i], comment[i], status[i]);
            testVerificationList.add(verification);
        }
        Mockito.when(mockVerificationDAOImpl.findAll()).thenReturn(testVerificationList);
        Assertions.assertEquals(testVerificationList, verificationHandling.findAllVerifications());
        Mockito.verify(mockVerificationDAOImpl, Mockito.times(1)).findAll();
    }
}
