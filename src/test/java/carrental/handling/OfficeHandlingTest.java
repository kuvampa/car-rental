package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.OfficeDAOImpl;
import carrental.db.entity.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class OfficeHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    OfficeDAOImpl mockOfficeDAOImpl;

    OfficeHandling officeHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getOfficeDAO()).thenReturn(mockOfficeDAOImpl);
        officeHandling = new OfficeHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] officeId = {1, 2, 3};
        String[] name = {"Відділ 1", "Відділ 2", "Відділ 3"};
        String[] location = {"Нова Почта1", "Нова Почта2", "Нова Почта3"};
        String[] laborHours = {"Без вихідних", "Без вихідних", "Без вихідних"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] status = {"avaible", "avaible", "avaible"};
        String[] map = {"1", "2", "3"};
        List<Office> testOfficeList = new ArrayList<>();
        for (int i = 0; i < officeId.length; i++) {
            Office office = new Office(officeId[i], name[i], location[i], laborHours[i], status[i], telephoneNumber[i], map[i]);
            testOfficeList.add(office);
        }
        Mockito.when(mockOfficeDAOImpl.findAll()).thenReturn(testOfficeList);
        Assertions.assertEquals(testOfficeList, officeHandling.findAllOffices());
        Mockito.verify(mockOfficeDAOImpl, Mockito.times(1)).findAll();
    }
}
