package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.ServiceDAOImpl;
import carrental.db.entity.AdditionalService;
import carrental.db.entity.ServiceRent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AdditionalServiceHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    ServiceDAOImpl mockServiceDAOImpl;

    AdditionalServiceHandling additionalServiceHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getServiceDAO()).thenReturn(mockServiceDAOImpl);
        additionalServiceHandling = new AdditionalServiceHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] serviceId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        double[] price = {30, 35, 40};
        String[] description = {"So low", "So mid", "So high"};
        String[] status = {"available", "available", "available"};
        List<AdditionalService> testServiceList = new ArrayList<>();
        for (int i = 0; i < serviceId.length; i++) {
            AdditionalService additionalService = new AdditionalService(serviceId[i], name[i], price[i], description[i], status[i]);
            testServiceList.add(additionalService);
        }
        Mockito.when(mockServiceDAOImpl.findAll()).thenReturn(testServiceList);
        Assertions.assertEquals(testServiceList, additionalServiceHandling.findAllServices());
        Mockito.verify(mockServiceDAOImpl, Mockito.times(1)).findAll();
    }
}
