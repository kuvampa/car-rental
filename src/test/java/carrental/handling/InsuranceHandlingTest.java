package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.AdminDAOImpl;
import carrental.db.dao.impl.InsuranceDAOImpl;
import carrental.db.entity.InsurancePackage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class InsuranceHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    InsuranceDAOImpl mockInsuranceDAOImpl;

    InsuranseHandling insuranseHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getInsuranseDAO()).thenReturn(mockInsuranceDAOImpl);
        insuranseHandling = new InsuranseHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] insuranceId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        String[] description = {"Low package", "Mid package", "High package"};
        String[] status = {"available", "available", "available"};
        List<InsurancePackage> testInsuranceList = new ArrayList<>();
        for (int i = 0; i < insuranceId.length; i++) {
            InsurancePackage insurancePackage = new InsurancePackage(insuranceId[i], name[i], description[i], status[i]);
            testInsuranceList.add(insurancePackage);
        }
        Mockito.when(mockInsuranceDAOImpl.findAll()).thenReturn(testInsuranceList);
        Assertions.assertEquals(testInsuranceList, insuranseHandling.findAllInsurances());
        Mockito.verify(mockInsuranceDAOImpl, Mockito.times(1)).findAll();
    }
}
