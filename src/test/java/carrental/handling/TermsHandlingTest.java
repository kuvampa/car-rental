package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.TermsDAOImpl;
import carrental.db.entity.TermsOfUse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class TermsHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    TermsDAOImpl mockTermsDAOImpl;

    TermsHandling termsHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getTermsDAO()).thenReturn(mockTermsDAOImpl);
        termsHandling = new TermsHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] termsPackageId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        String[] fuelConditions = {"small", "mid", "high"};
        String[] mileageLimit = {"small", "mid", "high"};
        String[] status = {"available", "available", "available"};
        List<TermsOfUse> testTermsList = new ArrayList<>();
        for (int i = 0; i < termsPackageId.length; i++) {
            TermsOfUse termsOfUse = new TermsOfUse(termsPackageId[i], name[i], fuelConditions[i], mileageLimit[i], status[i]);
            testTermsList.add(termsOfUse);
        }
        Mockito.when(mockTermsDAOImpl.findAll()).thenReturn(testTermsList);
        Assertions.assertEquals(testTermsList, termsHandling.findAllTerms());
        Mockito.verify(mockTermsDAOImpl, Mockito.times(1)).findAll();
    }
}
