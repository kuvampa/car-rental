package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.ManagerDAOImpl;
import carrental.db.entity.Manager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class ManagerHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    ManagerDAOImpl mockManagerDAOImpl;

    ManagerHandling managerHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getManagerDAO()).thenReturn(mockManagerDAOImpl);
        managerHandling = new ManagerHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] userId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        String[] documents = {"1", "2", "3"};
        List<Manager> testManagerList = new ArrayList<>();
        for (int i = 0; i < userId.length; i++) {
            Manager manager = new Manager(userId[i], fullName[i], telephoneNumber[i], email[i], login[i], password[i], documents[i]);
            testManagerList.add(manager);
        }
        Mockito.when(mockManagerDAOImpl.findAll()).thenReturn(testManagerList);
        Assertions.assertEquals(testManagerList, managerHandling.findAllManagers());
        Mockito.verify(mockManagerDAOImpl, Mockito.times(1)).findAll();
    }
}
