package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.AdminDAOImpl;
import carrental.db.entity.Admin;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AdminHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    AdminDAOImpl mockAdminDAOImpl;

    AdminHandling adminHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getAdminDAO()).thenReturn(mockAdminDAOImpl);
        adminHandling = new AdminHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] userId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        String[] documents = {"1", "2", "3"};
        List<Admin> testAdminList = new ArrayList<>();
        for (int i = 0; i < userId.length; i++) {
            Admin admin = new Admin(userId[i], fullName[i], telephoneNumber[i], email[i], login[i], password[i], documents[i]);
            testAdminList.add(admin);
        }
        Mockito.when(mockAdminDAOImpl.findAll()).thenReturn(testAdminList);
        Assertions.assertEquals(testAdminList, adminHandling.findAllAdmins());
        Mockito.verify(mockAdminDAOImpl, Mockito.times(1)).findAll();
    }
}
