package carrental.handling;

import carrental.db.DBException;
import carrental.db.dao.Factory;
import carrental.db.dao.impl.UserDAOImpl;
import carrental.db.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserHandlingTest {
    @Mock
    Factory mockFactory;

    @Mock
    UserDAOImpl mockUserDAOImpl;

    UserHandling userHandling;

    @BeforeEach
    void setUp() {
        Mockito.when(mockFactory.getUserDAO()).thenReturn(mockUserDAOImpl);
        userHandling = new UserHandling(mockFactory);
    }

    @Test
    void findAll() throws DBException {
        int[] userId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        int[] status = {0, 0, 0};
        String[] documents = {"1", "2", "3"};
        List<User> testUserList = new ArrayList<>();
        for (int i = 0; i < userId.length; i++) {
            User user = new User(userId[i], status[i], fullName[i], telephoneNumber[i], email[i], login[i], password[i], documents[i]);
            testUserList.add(user);
        }
        Mockito.when(mockUserDAOImpl.findAll()).thenReturn(testUserList);
        Assertions.assertEquals(testUserList, userHandling.findAllUsers());
        Mockito.verify(mockUserDAOImpl, Mockito.times(1)).findAll();
    }
}
