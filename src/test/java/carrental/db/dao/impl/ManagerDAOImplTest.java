package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Manager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class ManagerDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        managerDAOImpl.insert(manager);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        managerDAOImpl.update(manager);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(manager.getIdManager());
        Mockito.when(mockResultSet.getString("email")).thenReturn(manager.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(manager.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(manager.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(manager.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(manager.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(manager.getDocuments());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Assertions.assertEquals(manager, managerDAOImpl.findById(manager.getIdManager()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] managerId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        String[] documents = {"1", "2", "3"};
        List<Manager> testManagerList = new ArrayList<>();
        for (int i = 0; i < managerId.length; i++) {
            Manager manager = new Manager(managerId[i], login[i], password[i], fullName[i], telephoneNumber[i], email[i], documents[i]);
            testManagerList.add(manager);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(managerId[0]).thenReturn(managerId[1]).thenReturn(managerId[2]);
        Mockito.when(mockResultSet.getString("email")).thenReturn(email[0]).thenReturn(email[1]).thenReturn(email[2]);
        Mockito.when(mockResultSet.getString("password")).thenReturn(password[0]).thenReturn(password[1]).thenReturn(password[2]);
        Mockito.when(mockResultSet.getString("login")).thenReturn(login[0]).thenReturn(login[1]).thenReturn(login[2]);
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(fullName[0]).thenReturn(fullName[1]).thenReturn(fullName[2]);
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(telephoneNumber[0]).thenReturn(telephoneNumber[1]).thenReturn(telephoneNumber[2]);
        Mockito.when(mockResultSet.getString("documents")).thenReturn(documents[0]).thenReturn(documents[1]).thenReturn(documents[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Assertions.assertEquals(testManagerList, managerDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        managerDAOImpl.deleteById(manager.getIdManager());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByLogin() throws SQLException, DBException {
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        managerDAOImpl.deleteByName(manager.getLogin());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void findByLogin() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(manager.getIdManager());
        Mockito.when(mockResultSet.getString("email")).thenReturn(manager.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(manager.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(manager.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(manager.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(manager.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(manager.getDocuments());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Assertions.assertEquals(manager, managerDAOImpl.findManager(manager.getLogin()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void UpdateDocs() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        ManagerDAOImpl managerDAOImpl = new ManagerDAOImpl(mockConn);
        Manager manager = new Manager(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        managerDAOImpl.updateManagerDocs(manager.getLogin(), manager.getDocuments());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

}
