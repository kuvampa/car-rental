package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.AdditionalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class ServiceDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        serviceDAOImpl.insert(additionalService);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        serviceDAOImpl.update(additionalService);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void UpdateServiceStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        serviceDAOImpl.updateServiceStatus(additionalService.getName(), additionalService.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        Mockito.when(mockResultSet.getInt("id_service")).thenReturn(additionalService.getIdService());
        Mockito.when(mockResultSet.getString("name")).thenReturn(additionalService.getName());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(additionalService.getPrice());
        Mockito.when(mockResultSet.getString("description")).thenReturn(additionalService.getDescription());
        Mockito.when(mockResultSet.getString("status")).thenReturn(additionalService.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        Assertions.assertEquals(additionalService, serviceDAOImpl.findById(additionalService.getIdService()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByName() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        Mockito.when(mockResultSet.getInt("id_service")).thenReturn(additionalService.getIdService());
        Mockito.when(mockResultSet.getString("name")).thenReturn(additionalService.getName());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(additionalService.getPrice());
        Mockito.when(mockResultSet.getString("description")).thenReturn(additionalService.getDescription());
        Mockito.when(mockResultSet.getString("status")).thenReturn(additionalService.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        Assertions.assertEquals(additionalService, serviceDAOImpl.findServiceByName(additionalService.getName()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] serviceId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        double[] price = {30, 35, 40};
        String[] description = {"So low", "So mid", "So high"};
        String[] status = {"available", "available", "available"};
        List<AdditionalService> testServiceList = new ArrayList<>();
        for (int i = 0; i < serviceId.length; i++) {
            AdditionalService additionalService = new AdditionalService(serviceId[i], name[i], price[i], description[i], status[i]);
            testServiceList.add(additionalService);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_service")).thenReturn(serviceId[0]).thenReturn(serviceId[1]).thenReturn(serviceId[2]);
        Mockito.when(mockResultSet.getString("name")).thenReturn(name[0]).thenReturn(name[1]).thenReturn(name[2]);
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getString("description")).thenReturn(description[0]).thenReturn(description[1]).thenReturn(description[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        Assertions.assertEquals(testServiceList, serviceDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        AdditionalService additionalService = new AdditionalService(1, "Low", 30, "So low", "available");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        ServiceDAOImpl serviceDAOImpl = new ServiceDAOImpl(mockConn);
        serviceDAOImpl.deleteById(additionalService.getIdService());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
