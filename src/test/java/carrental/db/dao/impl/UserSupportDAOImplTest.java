package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Manager;
import carrental.db.entity.UserSupport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserSupportDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        UserSupport userSupport = new UserSupport(1, 1, 1, "Cool navbar", "available");
        userSupportDAOImpl.insert(userSupport);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        UserSupport userSupport = new UserSupport(1, 1, 1, "Cool navbar", "available");
        userSupportDAOImpl.update(userSupport);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }
    @Test
    void UpdateUserSupportStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        UserSupport userSupport = new UserSupport(1, 1, 1, "Cool navbar", "available");
        userSupportDAOImpl.updateUserSupportStatus(userSupport.getIdUserSupport(),userSupport.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        UserSupport userSupport = new UserSupport(1, 1, 1, "Cool navbar", "available");
        Mockito.when(mockResultSet.getInt("id_user_support")).thenReturn(userSupport.getIdUserSupport());
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(userSupport.getIdUser());
        Mockito.when(mockResultSet.getInt("id_admin")).thenReturn(userSupport.getIdAdmin());
        Mockito.when(mockResultSet.getString("message")).thenReturn(userSupport.getMessage());
        Mockito.when(mockResultSet.getString("status")).thenReturn(userSupport.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        Assertions.assertEquals(userSupport, userSupportDAOImpl.findById(userSupport.getIdUserSupport()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] userSupportId = {1, 2, 3};
        int[] userId = {1, 2, 3};
        int[] adminId = {1, 2, 3};
        String[] message = {"Cool navbar", "Cool footer", "Cool background"};
        String[] status = {"available", "available", "available"};
        List<UserSupport> testUserSupportList = new ArrayList<>();
        for (int i = 0; i < userSupportId.length; i++) {
            UserSupport userSupport = new UserSupport(userSupportId[i], userId[i], adminId[i], message[i], status[i]);
            testUserSupportList.add(userSupport);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_user_support")).thenReturn(userSupportId[0]).thenReturn(userSupportId[1]).thenReturn(userSupportId[2]);
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getInt("id_admin")).thenReturn(adminId[0]).thenReturn(adminId[1]).thenReturn(adminId[2]);
        Mockito.when(mockResultSet.getString("message")).thenReturn(message[0]).thenReturn(message[1]).thenReturn(message[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        Assertions.assertEquals(testUserSupportList, userSupportDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        UserSupport userSupport = new UserSupport(1, 1, 1, "Cool navbar", "available");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        UserSupportDAOImpl userSupportDAOImpl = new UserSupportDAOImpl(mockConn);
        userSupportDAOImpl.deleteById(userSupport.getIdUserSupport());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
