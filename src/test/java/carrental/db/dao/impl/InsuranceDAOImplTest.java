package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.InsurancePackage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class InsuranceDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        insuranceDAOImpl.insert(insurancePackage);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        insuranceDAOImpl.update(insurancePackage);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void UpdateInsuranceStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        insuranceDAOImpl.updateInsuranceStatus(insurancePackage.getName(), insurancePackage.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(insurancePackage.getIdInsurance());
        Mockito.when(mockResultSet.getString("name")).thenReturn(insurancePackage.getName());
        Mockito.when(mockResultSet.getString("description")).thenReturn(insurancePackage.getDescription());
        Mockito.when(mockResultSet.getString("status")).thenReturn(insurancePackage.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        Assertions.assertEquals(insurancePackage, insuranceDAOImpl.findById(insurancePackage.getIdInsurance()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByName() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(insurancePackage.getIdInsurance());
        Mockito.when(mockResultSet.getString("name")).thenReturn(insurancePackage.getName());
        Mockito.when(mockResultSet.getString("description")).thenReturn(insurancePackage.getDescription());
        Mockito.when(mockResultSet.getString("status")).thenReturn(insurancePackage.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        Assertions.assertEquals(insurancePackage, insuranceDAOImpl.findByName(insurancePackage.getName()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] insuranceId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        String[] description = {"Low package", "Mid package", "High package"};
        String[] status = {"available", "available", "available"};
        List<InsurancePackage> testInsuranceList = new ArrayList<>();
        for (int i = 0; i < insuranceId.length; i++) {
            InsurancePackage insurancePackage = new InsurancePackage(insuranceId[i], name[i], description[i], status[i]);
            testInsuranceList.add(insurancePackage);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(insuranceId[0]).thenReturn(insuranceId[1]).thenReturn(insuranceId[2]);
        Mockito.when(mockResultSet.getString("name")).thenReturn(name[0]).thenReturn(name[1]).thenReturn(name[2]);
        Mockito.when(mockResultSet.getString("description")).thenReturn(description[0]).thenReturn(description[1]).thenReturn(description[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        Assertions.assertEquals(testInsuranceList, insuranceDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        InsurancePackage insurancePackage = new InsurancePackage(1, "Low", "Low package", "available");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        InsuranceDAOImpl insuranceDAOImpl = new InsuranceDAOImpl(mockConn);
        insuranceDAOImpl.deleteById(insurancePackage.getIdInsurance());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
