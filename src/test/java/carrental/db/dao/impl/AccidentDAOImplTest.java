package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Accident;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AccidentDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        Accident accident = new Accident(1, 1, 1, 30.1, "Boom", "unchecked");
        accidentDAOImpl.insert(accident);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        Accident accident = new Accident(1, 1, 1, 30.1, "Boom", "unchecked");
        accidentDAOImpl.update(accident);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
    }

    @Test
    void UpdateStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        Accident accident = new Accident(1, 1, 1, 30.1, "Boom", "unchecked");
        accidentDAOImpl.updateAccidentStatus(accident.getIdAccident(), accident.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Accident accident = new Accident(1, 1, 1, 30.1, "Boom", "unchecked");
        Mockito.when(mockResultSet.getInt("id_accident")).thenReturn(accident.getIdAccident());
        Mockito.when(mockResultSet.getInt("id_rent")).thenReturn(accident.getIdRental());
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(accident.getIdManager());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(accident.getPrice());
        Mockito.when(mockResultSet.getString("comment")).thenReturn(accident.getComment());
        Mockito.when(mockResultSet.getString("status")).thenReturn(accident.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        Assertions.assertEquals(accident, accidentDAOImpl.findById(accident.getIdAccident()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] accidentId = {1, 2, 3};
        int[] rentId = {1, 2, 3};
        int[] managerId = {1, 2, 3};
        double[] price = {30.1, 30.2, 30.3};
        String[] comment = {"Boom", "Bam", "Badaboom"};
        String[] status = {"unchecked", "unchecked", "unchecked"};
        List<Accident> testAccidentList = new ArrayList<>();
        for (int i = 0; i < managerId.length; i++) {
            Accident accident = new Accident(accidentId[i], rentId[i], managerId[i], price[i], comment[i], status[i]);
            testAccidentList.add(accident);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_accident")).thenReturn(accidentId[0]).thenReturn(accidentId[1]).thenReturn(accidentId[2]);
        Mockito.when(mockResultSet.getInt("id_rent")).thenReturn(rentId[0]).thenReturn(rentId[1]).thenReturn(rentId[2]);
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(managerId[0]).thenReturn(managerId[1]).thenReturn(managerId[2]);
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getString("comment")).thenReturn(comment[0]).thenReturn(comment[1]).thenReturn(comment[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        Assertions.assertEquals(testAccidentList, accidentDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Accident accident = new Accident(1, 1, 1, 30.1, "Boom", "unchecked");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        AccidentDAOImpl accidentDAOImpl = new AccidentDAOImpl(mockConn);
        accidentDAOImpl.deleteById(accident.getIdAccident());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
