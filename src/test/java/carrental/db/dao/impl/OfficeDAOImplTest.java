package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class OfficeDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        officeDAOImpl.insert(office);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        officeDAOImpl.update(office);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void UpdateOfficeStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        officeDAOImpl.updateOfficeStatus(office.getName(), office.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(office.getIdOffice());
        Mockito.when(mockResultSet.getString("name")).thenReturn(office.getName());
        Mockito.when(mockResultSet.getString("location")).thenReturn(office.getLocation());
        Mockito.when(mockResultSet.getString("labor_hours")).thenReturn(office.getLaborHours());
        Mockito.when(mockResultSet.getString("status")).thenReturn(office.getStatus());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(office.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("map")).thenReturn(office.getMap());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Assertions.assertEquals(office, officeDAOImpl.findById(office.getIdOffice()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByName() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(office.getIdOffice());
        Mockito.when(mockResultSet.getString("name")).thenReturn(office.getName());
        Mockito.when(mockResultSet.getString("location")).thenReturn(office.getLocation());
        Mockito.when(mockResultSet.getString("labor_hours")).thenReturn(office.getLaborHours());
        Mockito.when(mockResultSet.getString("status")).thenReturn(office.getStatus());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(office.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("map")).thenReturn(office.getMap());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Assertions.assertEquals(office, officeDAOImpl.findOfficeByName(office.getName()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] officeId = {1, 2, 3};
        String[] name = {"Відділ 1", "Відділ 2", "Відділ 3"};
        String[] location = {"Нова Почта1", "Нова Почта2", "Нова Почта3"};
        String[] laborHours = {"Без вихідних", "Без вихідних", "Без вихідних"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] status = {"avaible", "avaible", "avaible"};
        String[] map = {"1", "2", "3"};
        List<Office> testOfficeList = new ArrayList<>();
        for (int i = 0; i < officeId.length; i++) {
            Office office = new Office(officeId[i], name[i], location[i], laborHours[i], status[i], telephoneNumber[i], map[i]);
            testOfficeList.add(office);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(officeId[0]).thenReturn(officeId[1]).thenReturn(officeId[2]);
        Mockito.when(mockResultSet.getString("name")).thenReturn(name[0]).thenReturn(name[1]).thenReturn(name[2]);
        Mockito.when(mockResultSet.getString("location")).thenReturn(location[0]).thenReturn(location[1]).thenReturn(location[2]);
        Mockito.when(mockResultSet.getString("labor_hours")).thenReturn(laborHours[0]).thenReturn(laborHours[1]).thenReturn(laborHours[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(telephoneNumber[0]).thenReturn(telephoneNumber[1]).thenReturn(telephoneNumber[2]);
        Mockito.when(mockResultSet.getString("map")).thenReturn(map[0]).thenReturn(map[1]).thenReturn(map[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        Assertions.assertEquals(testOfficeList, officeDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Office office = new Office(1, "Відділ 3", "Нова Почта", "Без вихідних", "open", "0956294043", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        OfficeDAOImpl officeDAOImpl = new OfficeDAOImpl(mockConn);
        officeDAOImpl.deleteById(office.getIdOffice());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
