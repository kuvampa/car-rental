package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Manager;
import carrental.db.entity.ServiceRent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
 class ServiceRentDAOImplTest {
 @Mock
 Connection mockConn;

 @Mock
 PreparedStatement mockPrepStmt;

 @Mock
 Statement mockStmt;

 @Mock
 ResultSet mockResultSet;

 @Test
 void insert() throws SQLException, DBException {
  Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
  Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
  Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
  ServiceRentDAOImpl serviceRentDAOImpl = new ServiceRentDAOImpl(mockConn);
  ServiceRent serviceRent = new ServiceRent(1,1,1);
  serviceRentDAOImpl.insert(serviceRent);
  Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
  Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
 }

 @Test
 void Update() throws SQLException, DBException {
  Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
  Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
  Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
  ServiceRentDAOImpl serviceRentDAOImpl = new ServiceRentDAOImpl(mockConn);
  ServiceRent serviceRent = new ServiceRent(1,1,1);
  serviceRentDAOImpl.update(serviceRent);
  Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
  Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
 }

 @Test
 void findById() throws SQLException, DBException {
  Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
  Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
  ServiceRent serviceRent = new ServiceRent(1,1,1);
  Mockito.when(mockResultSet.getInt("id_rent_service")).thenReturn(serviceRent.getIdRentService());
  Mockito.when(mockResultSet.getInt("id_rent")).thenReturn(serviceRent.getIdRentService());
  Mockito.when(mockResultSet.getInt("id_service")).thenReturn(serviceRent.getIdService());
  Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
  ServiceRentDAOImpl serviceRentDAOImpl = new ServiceRentDAOImpl(mockConn);
  Assertions.assertEquals(serviceRent, serviceRentDAOImpl.findById(serviceRent.getIdService()));
  Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
  Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
 }

 @Test
 void findAll() throws SQLException, DBException {
  int[] idRentService = {1, 2, 3};
  int[] idRent = {1, 2, 3};
  int[] idService = {1, 2, 3};
  List<ServiceRent> testServiceRentList = new ArrayList<>();
  for (int i = 0; i < idRentService.length; i++) {
   ServiceRent serviceRent = new ServiceRent(idRentService[i], idRent[i], idService[i]);
   testServiceRentList.add(serviceRent);
  }
  Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
  Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
  Mockito.when(mockResultSet.getInt("id_rent_service")).thenReturn(idRentService[0]).thenReturn(idRentService[1]).thenReturn(idRentService[2]);
  Mockito.when(mockResultSet.getInt("id_rent")).thenReturn(idRent[0]).thenReturn(idRent[1]).thenReturn(idRent[2]);
  Mockito.when(mockResultSet.getInt("id_service")).thenReturn(idService[0]).thenReturn(idService[1]).thenReturn(idService[2]);
  Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
  ServiceRentDAOImpl serviceRentDAOImpl = new ServiceRentDAOImpl(mockConn);
  Assertions.assertEquals(testServiceRentList, serviceRentDAOImpl.findAll());
  Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
 }

 @Test
 void deleteById() throws SQLException, DBException {
  ServiceRent serviceRent = new ServiceRent(1,1,1);
  Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
  Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
  ServiceRentDAOImpl serviceRentDAOImpl = new ServiceRentDAOImpl(mockConn);
  serviceRentDAOImpl.deleteById(serviceRent.getIdRentService());
  Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
  Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
 }
}
