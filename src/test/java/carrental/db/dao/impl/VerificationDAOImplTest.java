package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Manager;
import carrental.db.entity.Verification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class VerificationDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        verificationDAOImpl.insert(verification);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        verificationDAOImpl.update(verification);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void updateVerificationStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        verificationDAOImpl.updateVerificationStatus(verification.getIdVerificationOfDocuments(), verification.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void updateVerificationManager() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        verificationDAOImpl.updateVerificationManager(verification.getIdVerificationOfDocuments(), verification.getIdManager());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        Mockito.when(mockResultSet.getInt("id_verification_of_documents")).thenReturn(verification.getIdVerificationOfDocuments());
        Mockito.when(mockResultSet.getInt("id_rental")).thenReturn(verification.getIdRental());
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(verification.getIdManager());
        Mockito.when(mockResultSet.getString("comment")).thenReturn(verification.getComment());
        Mockito.when(mockResultSet.getString("status")).thenReturn(verification.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Assertions.assertEquals(verification, verificationDAOImpl.findById(verification.getIdManager()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] verificationId = {1, 2, 3};
        int[] rentalId = {1, 2, 3};
        int[] managerId = {1, 2, 3};
        String[] comment = {"reject", "reject", "reject"};
        String[] status = {"reject", "reject", "reject"};
        List<Verification> testVerificationList = new ArrayList<>();
        for (int i = 0; i < managerId.length; i++) {
            Verification verification = new Verification(verificationId[i], rentalId[i], managerId[i], comment[i], status[i]);
            testVerificationList.add(verification);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_verification_of_documents")).thenReturn(verificationId[0]).thenReturn(verificationId[1]).thenReturn(verificationId[2]);
        Mockito.when(mockResultSet.getInt("id_rental")).thenReturn(rentalId[0]).thenReturn(rentalId[1]).thenReturn(rentalId[2]);
        Mockito.when(mockResultSet.getInt("id_manager")).thenReturn(managerId[0]).thenReturn(managerId[1]).thenReturn(managerId[2]);
        Mockito.when(mockResultSet.getString("comment")).thenReturn(comment[0]).thenReturn(comment[1]).thenReturn(comment[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        Assertions.assertEquals(testVerificationList, verificationDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Verification verification = new Verification(1, 1, 1, "available", "reject");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        VerificationDAOImpl verificationDAOImpl = new VerificationDAOImpl(mockConn);
        verificationDAOImpl.deleteById(verification.getIdVerificationOfDocuments());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
