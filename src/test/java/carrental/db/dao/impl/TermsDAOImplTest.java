package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Manager;
import carrental.db.entity.TermsOfUse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class TermsDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        termsDAOImpl.insert(termsOfUse);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(4)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        termsDAOImpl.update(termsOfUse);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(4)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void UpdateTermsStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        termsDAOImpl.updateTermStatus(termsOfUse.getName(), termsOfUse.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(termsOfUse.getIdTermsPackage());
        Mockito.when(mockResultSet.getString("name")).thenReturn(termsOfUse.getName());
        Mockito.when(mockResultSet.getString("fuel_conditions")).thenReturn(termsOfUse.getFuelConditions());
        Mockito.when(mockResultSet.getString("mileage_limit")).thenReturn(termsOfUse.getMileageLimit());
        Mockito.when(mockResultSet.getString("status")).thenReturn(termsOfUse.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        Assertions.assertEquals(termsOfUse, termsDAOImpl.findById(termsOfUse.getIdTermsPackage()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByName() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(termsOfUse.getIdTermsPackage());
        Mockito.when(mockResultSet.getString("name")).thenReturn(termsOfUse.getName());
        Mockito.when(mockResultSet.getString("fuel_conditions")).thenReturn(termsOfUse.getFuelConditions());
        Mockito.when(mockResultSet.getString("mileage_limit")).thenReturn(termsOfUse.getMileageLimit());
        Mockito.when(mockResultSet.getString("status")).thenReturn(termsOfUse.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        Assertions.assertEquals(termsOfUse, termsDAOImpl.findByName(termsOfUse.getName()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] termsPackageId = {1, 2, 3};
        String[] name = {"Low", "Mid", "High"};
        String[] fuelConditions = {"small", "mid", "high"};
        String[] mileageLimit = {"small", "mid", "high"};
        String[] status = {"available", "available", "available"};
        List<TermsOfUse> testTermsList = new ArrayList<>();
        for (int i = 0; i < termsPackageId.length; i++) {
            TermsOfUse termsOfUse = new TermsOfUse(termsPackageId[i], name[i], fuelConditions[i], mileageLimit[i], status[i]);
            testTermsList.add(termsOfUse);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(termsPackageId[0]).thenReturn(termsPackageId[1]).thenReturn(termsPackageId[2]);
        Mockito.when(mockResultSet.getString("name")).thenReturn(name[0]).thenReturn(name[1]).thenReturn(name[2]);
        Mockito.when(mockResultSet.getString("fuel_conditions")).thenReturn(fuelConditions[0]).thenReturn(fuelConditions[1]).thenReturn(fuelConditions[2]);
        Mockito.when(mockResultSet.getString("mileage_limit")).thenReturn(mileageLimit[0]).thenReturn(mileageLimit[1]).thenReturn(mileageLimit[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        Assertions.assertEquals(testTermsList, termsDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        TermsOfUse termsOfUse = new TermsOfUse(1, "Low", "small", "small", "available");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        TermsDAOImpl termsDAOImpl = new TermsDAOImpl(mockConn);
        termsDAOImpl.deleteById(termsOfUse.getIdTermsPackage());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
