package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        User user = new User(0, 0, "Артём Новиков", "0506294043", "kuvampa@gmai.com", "Rivender", "tobby654", null);
        userDAOImpl.insert(user);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        userDAOImpl.update(user);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }
    @Test
    void UpdateDocs() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        userDAOImpl.updateUserDocs(user.getLogin(),user.getDocuments());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }
    @Test
    void UpdateStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        userDAOImpl.updateUserStatus(user.getLogin(),user.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(user.getIdUser());
        Mockito.when(mockResultSet.getString("email")).thenReturn(user.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(user.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(user.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(user.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(user.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(user.getDocuments());
        Mockito.when(mockResultSet.getInt("status")).thenReturn(user.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        Assertions.assertEquals(user, userDAOImpl.findById(user.getIdUser()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] userId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        int[] status = {0, 0, 0};
        String[] documents = {"1", "2", "3"};
        List<User> testUserList = new ArrayList<>();
        for (int i = 0; i < userId.length; i++) {
            User user = new User(userId[i], status[i], fullName[i], telephoneNumber[i], email[i], login[i], password[i], documents[i]);
            testUserList.add(user);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getString("email")).thenReturn(email[0]).thenReturn(email[1]).thenReturn(email[2]);
        Mockito.when(mockResultSet.getString("password")).thenReturn(password[0]).thenReturn(password[1]).thenReturn(password[2]);
        Mockito.when(mockResultSet.getString("login")).thenReturn(login[0]).thenReturn(login[1]).thenReturn(login[2]);
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(fullName[0]).thenReturn(fullName[1]).thenReturn(fullName[2]);
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(telephoneNumber[0]).thenReturn(telephoneNumber[1]).thenReturn(telephoneNumber[2]);
        Mockito.when(mockResultSet.getString("documents")).thenReturn(documents[0]).thenReturn(documents[1]).thenReturn(documents[2]);
        Mockito.when(mockResultSet.getInt("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        Assertions.assertEquals(testUserList, userDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }
    @Test
    void deleteById() throws SQLException, DBException {
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        userDAOImpl.deleteById(user.getIdUser());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
    @Test
    void deleteByLogin() throws SQLException, DBException {
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        userDAOImpl.deleteByName(user.getLogin());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
    @Test
    void findByLogin() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        User user = new User(1, 0, "admin", "0506294043", "administrator@gmail.com", "admin", "tobby6543", null);
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(user.getIdUser());
        Mockito.when(mockResultSet.getString("email")).thenReturn(user.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(user.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(user.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(user.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(user.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(user.getDocuments());
        Mockito.when(mockResultSet.getInt("status")).thenReturn(user.getStatus());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        UserDAOImpl userDAOImpl = new UserDAOImpl(mockConn);
        Assertions.assertEquals(user, userDAOImpl.findUser(user.getLogin()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

}
