package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Rental;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class RentDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        Rental rental = new Rental(1, 1, 1, 1, 30, "Office", LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 2, 23, 15, 56));
        rentDAOImpl.insert(rental);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        Rental rental = new Rental(1, 1, 1, 1, 30, "Office", LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 2, 23, 15, 56));
        rentDAOImpl.update(rental);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(4)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Rental rental = new Rental(1, 1, 1, 1, 30, "Office", LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 2, 23, 15, 56));
        Mockito.when(mockResultSet.getInt("id_rental")).thenReturn(rental.getIdRental());
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(rental.getIdUser());
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(rental.getIdCar());
        Mockito.when(mockResultSet.getObject("date_of_receiving")).thenReturn(rental.getDateOfReceiving());
        Mockito.when(mockResultSet.getObject("return_date")).thenReturn(rental.getReturnDate());
        Mockito.when(mockResultSet.getString("return_place")).thenReturn(rental.getReturnPlace());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(rental.getPrice());
        Mockito.when(mockResultSet.getInt("right_of_cancellation")).thenReturn(rental.getRightOfCancellation());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        Assertions.assertEquals(rental, rentDAOImpl.findById(rental.getIdRental()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByDate() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Rental rental = new Rental(1, 1, 1, 1, 30, "Office", LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 2, 23, 15, 56));
        Mockito.when(mockResultSet.getInt("id_rental")).thenReturn(rental.getIdRental());
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(rental.getIdUser());
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(rental.getIdCar());
        Mockito.when(mockResultSet.getObject("date_of_receiving")).thenReturn(rental.getDateOfReceiving());
        Mockito.when(mockResultSet.getObject("return_date")).thenReturn(rental.getReturnDate());
        Mockito.when(mockResultSet.getString("return_place")).thenReturn(rental.getReturnPlace());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(rental.getPrice());
        Mockito.when(mockResultSet.getInt("right_of_cancellation")).thenReturn(rental.getRightOfCancellation());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        Assertions.assertEquals(rental, rentDAOImpl.findByDate(rental.getDateOfReceiving(), rental.getReturnDate()));
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setObject(Mockito.anyInt(),Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] rentalId = {1, 2, 3};
        int[] userId = {1, 2, 3};
        int[] carId = {1, 2, 3};
        int[] rightOfCancellation = {1, 1, 1};
        double[] price = {10, 15, 20};
        LocalDateTime[] dateOfReceiving = {LocalDateTime.of(2022, 1, 13, 15, 56), LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 3, 13, 15, 56)};
        LocalDateTime[] returnDate = {LocalDateTime.of(2023, 1, 13, 15, 56), LocalDateTime.of(2023, 2, 13, 15, 56), LocalDateTime.of(2023, 3, 13, 15, 56)};
        String[] returnPlace = {"Office", "Office", "Office"};
        List<Rental> testRentList = new ArrayList<>();
        for (int i = 0; i < rentalId.length; i++) {
            Rental rental = new Rental(rentalId[i], userId[i], carId[i], rightOfCancellation[i], price[i], returnPlace[i], dateOfReceiving[i], returnDate[i]);
            testRentList.add(rental);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_rental")).thenReturn(rentalId[0]).thenReturn(rentalId[1]).thenReturn(rentalId[2]);
        Mockito.when(mockResultSet.getInt("id_user")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(carId[0]).thenReturn(carId[1]).thenReturn(carId[2]);
        Mockito.when(mockResultSet.getObject("date_of_receiving")).thenReturn(dateOfReceiving[0]).thenReturn(dateOfReceiving[1]).thenReturn(dateOfReceiving[2]);
        Mockito.when(mockResultSet.getObject("return_date")).thenReturn(returnDate[0]).thenReturn(returnDate[1]).thenReturn(returnDate[2]);
        Mockito.when(mockResultSet.getString("return_place")).thenReturn(returnPlace[0]).thenReturn(returnPlace[1]).thenReturn(returnPlace[2]);
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getInt("right_of_cancellation")).thenReturn(rightOfCancellation[0]).thenReturn(rightOfCancellation[1]).thenReturn(rightOfCancellation[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        Assertions.assertEquals(testRentList, rentDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Rental rental = new Rental(1, 1, 1, 1, 30, "Office", LocalDateTime.of(2022, 2, 13, 15, 56), LocalDateTime.of(2022, 2, 23, 15, 56));
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        RentDAOImpl rentDAOImpl = new RentDAOImpl(mockConn);
        rentDAOImpl.deleteById(rental.getIdRental());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}
