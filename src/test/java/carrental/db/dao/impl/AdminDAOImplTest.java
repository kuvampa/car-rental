package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Admin;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AdminDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        adminDAOImpl.insert(admin);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        adminDAOImpl.update(admin);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void UpdateDocs() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        adminDAOImpl.updateAdminDocs(admin.getLogin(), admin.getDocuments());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockResultSet.getInt("id_admin")).thenReturn(admin.getIdAdmin());
        Mockito.when(mockResultSet.getString("email")).thenReturn(admin.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(admin.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(admin.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(admin.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(admin.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(admin.getDocuments());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Assertions.assertEquals(admin, adminDAOImpl.findById(admin.getIdAdmin()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] adminId = {1, 2, 3};
        String[] login = {"Rivender", "Lesh", "Kaiserus"};
        String[] password = {"tobby654", "winner123", "znatok987"};
        String[] fullName = {"Артем Новиков", "Dmitriy Novykov", "Andrey Bereziuk"};
        String[] telephoneNumber = {"0506294043", "0506294042", "0506294041"};
        String[] email = {"Kuvampa@gmail.com", "Tarakan@gmail.com", "Duche@gmail.com"};
        String[] documents = {"1", "2", "3"};
        List<Admin> testAdminList = new ArrayList<>();
        for (int i = 0; i < adminId.length; i++) {
            Admin admin = new Admin(adminId[i], login[i], password[i], fullName[i], telephoneNumber[i], email[i], documents[i]);
            testAdminList.add(admin);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_admin")).thenReturn(adminId[0]).thenReturn(adminId[1]).thenReturn(adminId[2]);
        Mockito.when(mockResultSet.getString("email")).thenReturn(email[0]).thenReturn(email[1]).thenReturn(email[2]);
        Mockito.when(mockResultSet.getString("password")).thenReturn(password[0]).thenReturn(password[1]).thenReturn(password[2]);
        Mockito.when(mockResultSet.getString("login")).thenReturn(login[0]).thenReturn(login[1]).thenReturn(login[2]);
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(fullName[0]).thenReturn(fullName[1]).thenReturn(fullName[2]);
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(telephoneNumber[0]).thenReturn(telephoneNumber[1]).thenReturn(telephoneNumber[2]);
        Mockito.when(mockResultSet.getString("documents")).thenReturn(documents[0]).thenReturn(documents[1]).thenReturn(documents[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Assertions.assertEquals(testAdminList, adminDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        adminDAOImpl.deleteById(admin.getIdAdmin());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByLogin() throws SQLException, DBException {
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        adminDAOImpl.deleteByName(admin.getLogin());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void findByLogin() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Admin admin = new Admin(1, "Rivender", "tobby654", "Артём Новиков", "0506294043", "kuvampa@gmai.com", null);
        Mockito.when(mockResultSet.getInt("id_admin")).thenReturn(admin.getIdAdmin());
        Mockito.when(mockResultSet.getString("email")).thenReturn(admin.getEmail());
        Mockito.when(mockResultSet.getString("password")).thenReturn(admin.getPassword());
        Mockito.when(mockResultSet.getString("login")).thenReturn(admin.getLogin());
        Mockito.when(mockResultSet.getString("full_name")).thenReturn(admin.getFullName());
        Mockito.when(mockResultSet.getString("telephone_number")).thenReturn(admin.getTelephoneNumber());
        Mockito.when(mockResultSet.getString("documents")).thenReturn(admin.getDocuments());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        AdminDAOImpl adminDAOImpl = new AdminDAOImpl(mockConn);
        Assertions.assertEquals(admin, adminDAOImpl.findAdmin(admin.getLogin()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }
}
