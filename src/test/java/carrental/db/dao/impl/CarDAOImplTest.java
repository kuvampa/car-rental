package carrental.db.dao.impl;

import carrental.db.DBException;
import carrental.db.entity.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class CarDAOImplTest {
    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insert() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "available", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        carDAOImpl.insert(car);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(7)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void Update() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "available", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        carDAOImpl.update(car);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(6)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setDouble(Mockito.anyInt(), Mockito.anyDouble());
        Mockito.verify(mockPrepStmt, Mockito.times(7)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void UpdateCarStatus() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "available", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        carDAOImpl.updateCarStatus(car.getPlateNumber(), car.getStatus());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findById() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "avaible", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(car.getIdCar());
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(car.getIdOffice());
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(car.getIdInsurance());
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(car.getIdTermsPackage());
        Mockito.when(mockResultSet.getInt("capacity")).thenReturn(car.getCapacity());
        Mockito.when(mockResultSet.getInt("rating")).thenReturn(car.getRating());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(car.getPrice());
        Mockito.when(mockResultSet.getString("status")).thenReturn(car.getStatus());
        Mockito.when(mockResultSet.getString("color")).thenReturn(car.getColor());
        Mockito.when(mockResultSet.getString("car_name")).thenReturn(car.getCarName());
        Mockito.when(mockResultSet.getString("plate_number")).thenReturn(car.getPlateNumber());
        Mockito.when(mockResultSet.getString("type")).thenReturn(car.getType());
        Mockito.when(mockResultSet.getString("manufacturer")).thenReturn(car.getManufacturer());
        Mockito.when(mockResultSet.getString("fuel_type")).thenReturn(car.getFuelType());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(car, carDAOImpl.findById(car.getIdCar()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByPlate() throws SQLException, DBException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "available", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(car.getIdCar());
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(car.getIdOffice());
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(car.getIdInsurance());
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(car.getIdTermsPackage());
        Mockito.when(mockResultSet.getInt("capacity")).thenReturn(car.getCapacity());
        Mockito.when(mockResultSet.getInt("rating")).thenReturn(car.getRating());
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(car.getPrice());
        Mockito.when(mockResultSet.getString("status")).thenReturn(car.getStatus());
        Mockito.when(mockResultSet.getString("color")).thenReturn(car.getColor());
        Mockito.when(mockResultSet.getString("car_name")).thenReturn(car.getCarName());
        Mockito.when(mockResultSet.getString("plate_number")).thenReturn(car.getPlateNumber());
        Mockito.when(mockResultSet.getString("type")).thenReturn(car.getType());
        Mockito.when(mockResultSet.getString("manufacturer")).thenReturn(car.getManufacturer());
        Mockito.when(mockResultSet.getString("fuel_type")).thenReturn(car.getFuelType());
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(car, carDAOImpl.findCarByPlate(car.getPlateNumber()));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAll() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        int[] idOffice = {1, 2, 3};
        int[] idInsurance = {1, 2, 3};
        int[] idTermsPackage = {1, 2, 3};
        int[] capacity = {2, 2, 4};
        int[] rating = {1, 2, 3};
        double[] price = {15, 20, 30};
        String[] status = {"ok", "ok", "ok"};
        String[] color = {"white", "white", "white"};
        String[] carName = {"Ford Fiesta", "Nissan GTR", "Porshe 911"};
        String[] plateNumber = {"AA-1313-AA", "AA-1414-AA", "AA-1414-AA"};
        String[] type = {"sport", "super sport", "super duper sport"};
        String[] manufacturer = {"Ford", "Nissan", "Porshe"};
        String[] fuelType = {"petrol", "petrol", "petrol"};
        List<Car> testCarList = new ArrayList<>();
        for (int i = 0; i < idCar.length; i++) {
            Car car = new Car(idCar[i], idOffice[i], idInsurance[i], idTermsPackage[i], capacity[i], rating[i], price[i], status[i], color[i], carName[i], plateNumber[i], type[i], manufacturer[i], fuelType[i]);
            testCarList.add(car);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("id_car")).thenReturn(idCar[0]).thenReturn(idCar[1]).thenReturn(idCar[2]);
        Mockito.when(mockResultSet.getInt("id_office")).thenReturn(idOffice[0]).thenReturn(idOffice[1]).thenReturn(idOffice[2]);
        Mockito.when(mockResultSet.getInt("id_insurance")).thenReturn(idInsurance[0]).thenReturn(idInsurance[1]).thenReturn(idInsurance[2]);
        Mockito.when(mockResultSet.getInt("id_terms_package")).thenReturn(idTermsPackage[0]).thenReturn(idTermsPackage[1]).thenReturn(idTermsPackage[2]);
        Mockito.when(mockResultSet.getInt("capacity")).thenReturn(capacity[0]).thenReturn(capacity[1]).thenReturn(capacity[2]);
        Mockito.when(mockResultSet.getInt("rating")).thenReturn(rating[0]).thenReturn(rating[1]).thenReturn(rating[2]);
        Mockito.when(mockResultSet.getDouble("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getString("status")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.getString("color")).thenReturn(color[0]).thenReturn(color[1]).thenReturn(color[2]);
        Mockito.when(mockResultSet.getString("car_name")).thenReturn(carName[0]).thenReturn(carName[1]).thenReturn(carName[2]);
        Mockito.when(mockResultSet.getString("plate_number")).thenReturn(plateNumber[0]).thenReturn(plateNumber[1]).thenReturn(plateNumber[2]);
        Mockito.when(mockResultSet.getString("type")).thenReturn(type[0]).thenReturn(type[1]).thenReturn(type[2]);
        Mockito.when(mockResultSet.getString("manufacturer")).thenReturn(manufacturer[0]).thenReturn(manufacturer[1]).thenReturn(manufacturer[2]);
        Mockito.when(mockResultSet.getString("fuel_type")).thenReturn(fuelType[0]).thenReturn(fuelType[1]).thenReturn(fuelType[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testCarList, carDAOImpl.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteById() throws SQLException, DBException {
        Car car = new Car(1, 1, 1, 1, 4, 1, 15, "available", "white", "Ford Fiesta", "AA-1212-AA", "sport", "Ford", "petrol");
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        carDAOImpl.deleteById(car.getIdCar());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void findUniqueManufacture() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        String[] manufacturer = {"Ford", "Nissan", "Porshe"};
        List<String> testList = new ArrayList<>(Arrays.asList(manufacturer).subList(0, idCar.length));
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getString("manufacturer")).thenReturn(manufacturer[0]).thenReturn(manufacturer[1]).thenReturn(manufacturer[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueManufacture());


    }

    @Test
    void findUniqueColor() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        String[] color = {"white", "black", "grey"};
        List<String> testList = new ArrayList<>(Arrays.asList(color).subList(0, idCar.length));
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getString("color")).thenReturn(color[0]).thenReturn(color[1]).thenReturn(color[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueColor());


    }

    @Test
    void findUniqueRating() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        int[] rating = {1, 2, 3};
        List<Integer> testList = new ArrayList<>();
        for (int i = 0; i < idCar.length; i++) {
            testList.add(rating[i]);
        }
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("rating")).thenReturn(rating[0]).thenReturn(rating[1]).thenReturn(rating[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueRating());
    }

    @Test
    void findUniqueType() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        String[] type = {"sport", "super sport", "super duper sport"};
        List<String> testList = new ArrayList<>(Arrays.asList(type).subList(0, idCar.length));
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getString("type")).thenReturn(type[0]).thenReturn(type[1]).thenReturn(type[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueType());


    }

    @Test
    void findUniqueFuelType() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        String[] fuelType = {"petrol", "diesel", "gas"};
        List<String> testList = new ArrayList<>(Arrays.asList(fuelType).subList(0, idCar.length));
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getString("fuel_type")).thenReturn(fuelType[0]).thenReturn(fuelType[1]).thenReturn(fuelType[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueFuelType());


    }

    @Test
    void findUniqueCapacity() throws SQLException, DBException {
        int[] idCar = {1, 2, 3};
        int[] capacity = {2, 2, 4};
        List<Integer> testList = new ArrayList<>();
        for (int i = 0; i < idCar.length; i++) {
            testList.add(capacity[i]);
        }
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.getInt("capacity")).thenReturn(capacity[0]).thenReturn(capacity[1]).thenReturn(capacity[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        CarDAOImpl carDAOImpl = new CarDAOImpl(mockConn);
        Assertions.assertEquals(testList, carDAOImpl.findUniqueCapacity());


    }
}
